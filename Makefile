%:
	nix build .#$@

build:
	nix build .#`hostname`

switch:
	nix run .#`hostname`
