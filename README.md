home
====

My `home-manager`ed dot-files the nix flake way

# my-hm

```
my-hm switch  # build and switch to next generation
my-hm build   # build current home
my-hm pull    # synchronize repositories
my-hm deploy  # deploy to other box
```

# dev

## build

```
nix build .#`hostname`
```

## switch

```
nix run .#`hostname`
```
