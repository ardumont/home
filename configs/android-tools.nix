{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.my.android-tools;
in {
  options.my.android-tools = {
    enable = mkEnableOption "Android tools dependencies";
  };

  config = mkIf cfg.enable {
    home = {
      packages = [
        pkgs.androidenv.androidPkgs_9_0.platform-tools
      ];
    };
  };
}
