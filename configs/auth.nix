{ config, lib, pkgs, creds, ... }:

let cfg = config.my.auth;
in {
  options.my.auth = {
    enable = lib.mkOption {
      type = lib.types.bool;
      description = "Manage auth tools and configuration";
      default = false;
    };
  };

  config = lib.mkIf cfg.enable {
    # FIXME: Find a way to encrypt it from here
    home.file.".authinfo".text = creds.authinfo;
  };
}
