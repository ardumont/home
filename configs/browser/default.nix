{ config, lib, pkgs, ...}:

with lib;
let
  cfg = config.my.xsession;
  folder-config = ".config/qutebrowser";
  folder-userscripts = ".local/share/qutebrowser/userscripts";
in {
  options.my.browser = {
    program = mkOption {
      type = types.str;
      default = "";
      description = "Default program path";
    };
  };
  config = mkIf cfg.enable {
    my.browser.program = "~/bin/qutebrowser";

    home = {
       file = {
       # qutebrowser userscripts
        "${folder-userscripts}/to-tsm" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

~/bin/transmission-remote --add "$QUTE_URL"
'';
        };
        "${folder-userscripts}/dl" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

# This appends the hinted link target to the $TRANSMISSION_CACHE_FILE
# M-x hint links userscript dl

pushd $VIDEO_FOLDER

${pkgs.yt-dlp}/bin/yt-dlp --format 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' \
	   $QUTE_URL
'';
        };

        "${folder-userscripts}/dl-audio" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

# This appends the hinted link target to the $TRANSMISSION_CACHE_FILE
# M-x hint links userscript dl-audio

pushd $AUDIO_FOLDER

${pkgs.yt-dlp}/bin/yt-dlp --extract-audio \
	   --audio-format vorbis \
	   $QUTE_URL
'';
        };

        "${folder-userscripts}/to-sample" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

# This appends the hinted link target to the $TRANSMISSION_CACHE_FILE
# M-x hint links userscript to-sample

echo $QUTE_URL >> $TRANSMISSION_CACHE_FILE
'';
        };
        "${folder-userscripts}/parse-id" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

"""
This computes the id from an url (imdb, swh forge, ...) and copy it into the clipboard
M-x hint links userscript parse-id
"""

URL_ID=$($HOME/bin/parse-id-from-url $QUTE_URL)
echo -n $URL_ID | ${pkgs.xclip}/bin/xclip -i -selection clipboard

'';
        };

        "${folder-userscripts}/parse-gitlab-ref" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

"""
This computes the internal gitlab reference id from an url and copy it into the
clipboard.
M-x hint links userscript parse-gitlab-ref
"""

REF_ID=$($HOME/work/inria/bin/swh-gitlab-reference.py $QUTE_URL)
echo -n $REF_ID | ${pkgs.xclip}/bin/xclip -i -selection clipboard

'';
        };

        # configuration
        "${folder-config}/config.py".source = ./config.py;

        "bin/qutebrowser" = with pkgs; {
          executable = true;
          text = ''
#!${stdenv.shell}

export QT_XCB_GL_INTEGRATION=none
if [ -f /usr/bin/qutebrowser ]; then
  # export QUTE_QT_WRAPPER=PyQt6
  export QUTE_QT_WRAPPER=PyQt5
  browser=/usr/bin/qutebrowser
else
  browser=${qutebrowser}/bin/qutebrowser
fi
exec $browser $@
'';
       };
      };
    };
  };
}
