{ lib, config, pkgs, creds, ... }:

with lib;
let
  cfg = config.my.element-desktop;
  # cfg-xsession = config.my.xsession;
in {
  options.my.element-desktop = {
    enable = mkEnableOption "Install element-desktop tools deps";
  };

  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        element-desktop
      ];

    };
  };
}
