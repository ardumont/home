{ lib, config, pkgs, creds, ... }:

with lib;
let
  cfg = config.my.gomuks;
in {
  options.my.gomuks = {
    enable = mkEnableOption "Install gomuks tools deps";
  };

  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        gomuks
      ];

    };
  };
}
