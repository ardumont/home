{ lib, config, pkgs, creds, ... }:

with lib;
let
  cfg = config.my.iamb;
in {
  options.my.iamb = {
    enable = mkEnableOption "Install iamb tools deps";
  };

  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        iamb
      ];

      file.".config/iamb/config.json".text = ''
{
    "profiles": {
        "matrix.org": {
           "url": "https://matrix-client.matrix.org/",
            "user_id": "@ardumont:matrix.org"
        }
    }
}
'';
    };
  };
}
