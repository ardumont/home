{ lib, config, pkgs, creds, ... }:

with lib;
let cfg = config.my.weechat;

in {
  options.my.weechat = {
    enable = mkEnableOption "Weechat specific configuration";
  };

  config = mkIf cfg.enable {
    home = {
      packages = [
        pkgs.weechat-matrix
      ];

      file.".config/weechat/wee_matter.conf".text = ''
[server]
inria.host = "${creds.mattermost.inria.host}"
inria.password = "${creds.mattermost.inria.password}"
inria.path = ""
inria.personal_access_token = "${creds.mattermost.inria.token}"
inria.port = "443"
inria.protocol = "https"
inria.username = "${creds.mattermost.inria.user}"
      '';
    };
  };
}
