{ config, lib, pkgs, ... }:

let cfg = config.my.common-lisp;
in {
  options.my.common-lisp = {
    enable = lib.mkOption {
      type = lib.types.bool;
      description = "Manage common-lisp tools and configuration";
      default = false;
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [
      sbcl
    ];

    home.file.".sbclrc".text = ''
;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

'';
  };
}
