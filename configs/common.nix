{ config, pkgs, ... }:

{
  # Don't let home-manager install/ manage itself because it relies on nix-channel
  # (impure regarding reproducibility)
  programs.home-manager.enable = false;
  home = {
    sessionVariables = {
      PATH = "$HOME/bin:$HOME/.config/guix/current/bin:/usr/local/bin:$HOME/.local/bin:$HOME/.nix-profile/bin:$PATH";
      MANPATH="/usr/share/man:/usr/local/man:$MANPATH";
      EDITOR = "$HOME/bin/emacs";
      BROWSER="${config.my.browser.program}";
      LC_ALL = "en_US.utf8";
      LANGUAGE = "en_US.utf8";
      # locale issue
      LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
      VIDEO_FOLDER = "$HOME/Video";
      AUDIO_FOLDER = "$HOME/Music";
      TERM = "screen-256color";
      REPO_PUBLIC = "$HOME/repo/public";
      ORGTRELLO_HOME = "$REPO_PUBLIC/org-trello";
      PAGER = "${pkgs.less}/bin/less";
      MANPAGER = "${pkgs.most}/bin/most";
      HISTSIZE = 500000;
    };
    language = {
      base = "en_US.utf8";
      address = "en_US.utf8";
      monetary = "fr_FR.utf8";
      paper = "fr_FR.utf8";
      time = "fr_FR.utf8";
    };
    keyboard = {
      layout = "us";
      model = "pc105";
      options = ["eurosign:e" "ctrl:nocaps" "terminate=ctrl_alt_backspace"
                 "altwin:meta_alt" "compose:lwin"];
    };
  };
}
