{ config, lib, pkgs, mypkgs, system, creds, ... }:

with lib;
let
  cfg = config.my.container-tools;
in {
  options.my.container-tools = {
    enable = mkEnableOption "Install container-tools dependencies";
  };

  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        kubectx
        kubernetes
        kubernetes-helm
        # skaffold  # too old version
        argo
        k9s
        kail
        stern
        vcluster
        kind
      ];

      file = {
        ".docker/config.json".text = ''
{
  "detachKeys": "ctrl-@",
  "auths": {
    "https://index.docker.io/v1/": {
       "auth": "${creds.dockerhub.token}"
    },
    "container-registry.softwareheritage.org": {
       "auth": "${creds.forge-tokens.gitlab.swh-container-registry}"
    }
  }
}'';
        ".config/kubectl/completion-zsh".source = ./.kubectl-completion-zsh;
        ".kube/zsh".text = ''
function set-kubeconfig {
  # Sets the KUBECONFIG environment variable to a dynamic concatenation of everything
  # under ~/.kube/config.d/*
  # Does NOT overwrite KUBECONFIG if the KUBECONFIG_MANUAL env var is set

  if [ -d ~/.kube/config.d -a -z "$KUBECONFIG_MANUAL" ]; then
    export KUBECONFIG=~/.kube/config$(find ~/.kube/config.d/ -type f 2>/dev/null | xargs -I % echo -n ":%")
  fi
}

add-zsh-hook precmd set-kubeconfig

alias kcg='kubectl config get-contexts'
alias kcu='kubectl config use-context'

        '';
        "bin/skaffold" = {
          executable = true;
          text = with pkgs; ''
            #!${stdenv.shell} -xe

            LOCAL_CMD=/usr/local/bin/skaffold
            if [ -f $LOCAL_CMD ]; then
                cmd=$LOCAL_CMD
            else
                cmd=skaffold
            fi

            $cmd $@
          '';
        };
        # Wrapper to move minikube home in another partition
        "bin/minikube" = {
          executable = true;
          text = ''
          #!${pkgs.stdenv.shell} -xe

          CMD=${pkgs.minikube}/bin/minikube
          export MINIKUBE_HOME=/srv2/minikube
          KUBEDIR=$MINIKUBE_HOME/.kube
          mkdir -p $KUBEDIR
          export KUBECONFIG=$KUBEDIR/config

          # To keep kubectl happy when called outside minikube, we link
          # the $MINIKUBE_HOME to ~/.minikube
          [ ! -h ~/.minikube ] && rm -rf ~/.minikube && \
            ln -s $MINIKUBE_HOME/.minikube ~/.minikube

          $CMD config set driver docker

          $CMD $@
          '';
        };
      };
    };
  };
}
