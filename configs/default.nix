{ config, lib, nixpkgs, pkgs, mypkgs, home-manager, pytools, system, creds, identity, ...}:

{
  imports = map (m: import m {
    inherit config lib nixpkgs pkgs mypkgs home-manager pytools system creds identity;
  } ) [
    ./browser/default.nix
    ./common.nix
    ./dev/default.nix
    ./android-tools.nix
    ./emacs/default.nix
    ./email/default.nix
    ./git.nix
    ./gpg/default.nix
    ./monitoring.nix
    ./network.nix
    ./nano.nix
    ./nix/default.nix
    ./nixpkgs/default.nix
    ./shell/default.nix
    ./ssh/default.nix
    ./ssh/clustershell.nix
    ./scripts/default.nix
    ./tmux.nix
    ./transmission/default.nix
    ./urxvt/default.nix
    ./xkeysnail/default.nix
    ./xsession/default.nix
    ./work/default.nix
    ./zathura.nix
    ./fstools.nix
    ./qmk
    ./container-tools
    ./guix.nix
    ./common-lisp.nix
    ./auth.nix
    ./chat/matterhorn.nix
    ./chat/weechat.nix
    ./chat/element-desktop.nix
    ./chat/gomuks.nix
    ./chat/iamb.nix
    ./media/default.nix
  ];
}
