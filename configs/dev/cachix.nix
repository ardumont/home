{ lib, creds, ... }:

{
  home.sessionVariables.CACHIX_AUTH_TOKEN = creds.cachix.tokens.nix-emacs-ci;
}
