{ config, lib, pkgs, pytools, mypkgs, system, creds, ... }:

with lib;
let
  cfg = config.my.development;
  cfg-xsession = config.my.xsession;
in {
  options.my.development = {
    enable = mkEnableOption "Install development dependencies";
  };

  imports = map (m: import m {
    inherit config lib pkgs pytools mypkgs system creds;
  } ) [
    ./direnv/default.nix
    ./cachix.nix
    ./python.nix
  ];

  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        # recsel
        recutils
        thefuck pipenv
        # texLiveFull
        # haskell tools
        cabal-install cabal2nix
        stack
        pkg-config
        gnumake
        tig gitAndTools.hub # gitAndTools.git-annex
        # ui tools
        gitui
        gpg-tui
        gping
        #
        mosh
        postgresql_12
        # odroid board, uart
        picocom minicom
        usbutils
        bc
        go
        # pair programming tool
        tmate
        # compile tools
        automake
        # personal tools
        pytools.defaultPackage."${system}"
        mypkgs.packages."${system}".mnamer
        # clojure
        boot
        whois
        wireguard-tools
        mr
        jq
        w3m
        file-rename
        nixos-container
        python310Packages.hachoir
        dd_rescue
      ] ++ lib.lists.optionals cfg-xsession.enable [ pkgs.inkscape ];

      file = {
        ".mrtrust".text = let h = config.home.homeDirectory; in ''
${h}/.mrconfig
${h}/work/inria/repo/swh/puppet-environment/.mrconfig
${h}/work/inria/repo/swh/swh-environment/.mrconfig
${h}/work/inria/repo/swh/kube-swh-env/.mrconfig
${h}/work/inria/repo/swh/ci-environment/.mrconfig
${h}/work/inria/repo/swh/sysadm-environment/.mrconfig
${h}/.emacs.d/.mrconfig
'';
        ".gbp.conf".source = ./gbp.conf;
        ".rabbitmqadmin.conf".source = ./rabbitmqadmin.conf;
        ".psqlrc".source = ./psqlrc.conf;
      };
    };
  };

}
