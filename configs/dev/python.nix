{ config, ... }:

{
  config = {
    home.file = {
      "bin/pyenv-init" = {
        executable = true;
        text = ''
# To configure pyenv a-la-demande (when bootstraping a venv for example)
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
'';
      };
      ".config/direnv/lib/python.sh" = {
        executable = true;
        text = ''
# Load pyenv in the direnv context only
export PYENV_ROOT="$HOME/.pyenv"

if [ -e $PYENV_ROOT ]; then
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"

    # Restart your shell for the changes to take effect.

    # Load pyenv-virtualenv automatically by adding
    # the following to ~/.bashrc:

    eval "$(pyenv virtualenv-init -)"
fi

layout_virtualenv() {
  local venv_path="$1"
  source $venv_path/bin/activate
}

layout_virtualenvwrapper() {
  local venv_path="$WORKON_HOME/$1"
  layout_virtualenv $venv_path
}
'';
      };
    };
  };
}
