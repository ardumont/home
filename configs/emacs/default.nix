{ config, lib, pkgs, mypkgs, system, creds, ... }:

with lib;
let
  xsession-enable = config.my.xsession.enable;
  email-enable = config.my.email.enable;
  optional-deps = config.my.emacs.optional-dependencies.enable;
  emacs-powerline = mypkgs.packages."${system}".emacs-powerline;
  consumer-key = creds.org-trello.consumer-key;
  access-token = creds.org-trello.access-token;
  noop = epkgs: [];
  internal-configs = [
    {
      # init.el (default packages)
      pkgs-fn = epkgs: with epkgs; [ use-package dash s f deferred ];
    }
    {
      # utils-pack.el
      pkgs-fn = epkgs: [ ];
    }
    {
      # haskell-pack
      pkgs-fn = epkgs: with epkgs; [ haskell-mode ghci-completion ];
    }
    {
      # java-pack
      pkgs-fn = epkgs: with epkgs; [ lsp-mode lsp-java groovy-mode ];
    }
    {
      # yaml-pack
      pkgs-fn = epkgs: with epkgs; [ highlight-indent-guides ];
    }
    {
      # theme-pack
      pkgs-fn = epkgs:
        with epkgs; [
          deferred
          smart-mode-line
          spacemacs-theme
          doom-themes
          doom-modeline
          emacs-powerline
          all-the-icons
        ];
    }
    {
      # prelude-pack
      pkgs-fn = epkgs: with epkgs; [ crux projectile helm ];
    }
    {
      # buffer-pack
      pkgs-fn = epkgs:
        with epkgs; [
          move-text
          dockerfile-mode
          projectile
          dash
          markdown-mode
          iedit
          markdown-toc
          yaml-mode
          multiple-cursors
          git-gutter
          buffer-move
          ace-window
          s
          git-gutter
          popwin
          ht
          ctrlf
        ];
      deps = [ pkgs.hunspell pkgs.hunspellDicts.en_US ];
    }
    {
      # git-pack
      pkgs-fn = epkgs: with epkgs; [ fullframe magit git-gutter forge ];
      deps = with pkgs; [ git arcanist ];
      deps-optional = if xsession-enable then [ pkgs.gitg ] else [];
    }
    {
      # pass-pack
      pkgs-fn = epkgs: [ epkgs.helm-pass ];
    }
    {
      # prog-mode-pack
      pkgs-fn = epkgs: with epkgs; [ highlight-indent-guides ];
    }
    {
      # helm-pack
      pkgs-fn = epkgs: [ epkgs.helm ];
    }
    {
      # help-pack
      pkgs-fn = epkgs: with epkgs; [ which-key helpful ];
    }
    {
      # popup-pack
      pkgs-fn = noop;
    }
    {
      # modeline-pack
      pkgs-fn = epkgs: with epkgs; [ dash s diminish ];
    }
    {
      # shell-pack
      pkgs-fn = epkgs: with epkgs; [ popwin exec-path-from-shell ];
    }
    {
      # orgmode-pack
      pkgs-fn = epkgs:
        with epkgs; [
          org
          ac-math
          dash
          auto-complete
          toc-org
          org-trello
          helm-org-rifle
        ];
      deps-optional = [ pkgs.lilypond ];
    }
    {
      # python-pack
      pkgs-fn = epkgs: with epkgs; [
        elpy
        smartparens
        blacken
      ];
      deps-optional = [ pkgs.python310Packages.python-lsp-server ];
      deps-internal = with pkgs; let
        python-with-tools = python310.withPackages (pps: [ python310Packages.matplotlib ]);
        in [ python-with-tools ];
    }
    {
      # twitter-pack
      pkgs-fn = epkgs: [ epkgs.twittering-mode ];
    }
    {
      # lisp-pack
      pkgs-fn = epkgs:
        with epkgs; [ paredit fold-dwim eval-sexp-fu rainbow-delimiters ];
      deps = with pkgs; [ boot ];
    }
    {
      # guix-pack
      pkgs-fn = epkgs:
        with epkgs; [ paredit fold-dwim eval-sexp-fu rainbow-delimiters
                      geiser geiser-guile guix ];
      deps = with pkgs; [ guile ];
    }
    {
      # calendar-pack
      pkgs-fn = noop;
    }
    {
      # common-lisp-pack
      pkgs-fn = epkgs:
        with epkgs; [ paredit fold-dwim rainbow-delimiters aggressive-indent
                      sly sly-quicklisp helm-company # helm-sly
                    ];
      deps = with pkgs; [ libmysqlclient ];
    }
    {
      # clojure-pack
      pkgs-fn = epkgs:
        with epkgs; [ paredit fold-dwim rainbow-delimiters  aggressive-indent
                      cider ];
    }
    {
      # elisp-pack
      pkgs-fn = epkgs:
        with epkgs; [ paredit fold-dwim rainbow-delimiters aggressive-indent
                      page-break-lines
                      bug-hunter goto-last-change dash request-deferred el-mock
                      undercover ];
      deps = [ pkgs.cask ];
    }
    {
      # puppet-pack
      pkgs-fn = epkgs: [ epkgs.puppet-mode ];
    }
    {
      # blog-pack
      pkgs-fn = epkgs: with epkgs; [ org2jekyll kv ];
    }
    {
      # wiki-pack
      pkgs-fn = epkgs: with epkgs; [ mediawiki ox-mediawiki ];
    }
    {
      # mercurial-pack
      pkgs-fn = epkgs: [ epkgs.monky ];
    }
    {
      # pres-pack
      pkgs-fn = epkgs: [ epkgs.ox-reveal ];
    }
    {
      # browser-pack
      pkgs-fn = epkgs: [ epkgs.restclient ];
    }
    {
      # scratch-pack
      pkgs-fn = epkgs: with epkgs; [ htmlize google-this hydra ];
    }
    {
      # viewer-pack
      pkgs-fn = noop;
    }
    {
      # marmalade-pack
      pkgs-fn = epkgs: [ epkgs.marmalade-client ];
    }
    {
      # nix-pack
      pkgs-fn = epkgs:
        with epkgs; [
          nix-mode
        ];
      deps = [
        # pkgs.rnix-lsp  # removed from nix 'cause unmaintained
      ];
    }
    {
      # macro-pack
      pkgs-fn = noop;
    }
    {
      # devops-pack
      pkgs-fn = epkgs: with epkgs; [ terraform-mode groovy-mode ];
    }
    {
      # employeur
      pkgs-fn = noop;
    }
    {
      # rust-pack
      pkgs-fn = epkgs: with epkgs; [ rust-mode rustic ];
      deps = with pkgs; [ rustup ];
    }
    {
      # kill-pack
      pkgs-fn = noop;
      deps-optional = [ pkgs.xsel ];
    }
    {
      # ereader-pack
      pkgs-fn = epkgs: [ epkgs.nov ];
    }
    {
      # screencast-pack
      pkgs-fn = epkgs: [ epkgs.gif-screencast ];
      deps-optional = [
        pkgs.scrot
        pkgs.imagemagick # mogrify
        pkgs.gifsicle
      ];
    }
    {
      # js-pack
      pkgs-fn = noop;
    }
    {
      # graph-pack
      pkgs-fn = epkgs: with epkgs; [ plantuml-mode mermaid-mode ];
      deps-optional = [
        pkgs.mermaid-cli
      ];
    }
    {
      # coverage
      pkgs-fn = epkgs: [ epkgs.cov ];
    }
    {
      # global-pack
      pkgs-fn = epkgs: with epkgs; [
        company
        lsp-mode
        helm-lsp
        lsp-ui
        fill-column-indicator
      ];

    }
    {
      # env
      pkgs-fn = epkgs: [ epkgs.envrc ];
    }

  ] ++ lib.optionals email-enable [
    {
      # mail-pack
      pkgs-fn = epkgs: [
        epkgs.creds # override because of the imported "creds"
        epkgs.dash
        epkgs.s
        epkgs.async
        epkgs.notmuch
      ];
      deps = [ pkgs.notmuch ];
    }
  ];

  # system packages emacs' config depends upon
  # On some platform, compiling those are too heavy
  optional-runtime-dependencies = if optional-deps then
    concatMap (config: config.deps-optional or  []) internal-configs
  else
    [];
  optional-internal-dependencies = if optional-deps then
    concatMap (config: config.deps-internal or []) internal-configs
  else
    [];
  # Add here extra runtimes
  extra-runtime-dependencies = concatMap (config: config.deps or []) internal-configs;

  cfg = config.my.emacs;
in
{
  options.my.emacs = {
    enable = mkOption {
      type = types.bool;
      description = "Emacs tool configuration";
      default = true;
    };

    optional-dependencies.enable = mkOption {
      type = types.bool;
      description = "Install emacs with optional dependencies";
      default = true;
    };
  };

  config = lib.mkIf cfg.enable {
    programs.emacs = with pkgs; {
      enable = true;
      # emacs no x if no x session
      package = if xsession-enable then emacs else emacs-nox;
      # nevertheless, install the deps
      extraPackages = epkgs:
        concatMap (config: config.pkgs-fn epkgs) internal-configs
        ++ extra-runtime-dependencies ++ optional-runtime-dependencies
        ++ optional-internal-dependencies;
    };

    home.packages = extra-runtime-dependencies ++ optional-runtime-dependencies;

    services.emacs.enable = true;

    # Inline the packs into the init.el
    home.file = {
      "bin/emacs" = {
        executable = true;
        text = with pkgs; ''
          #!${stdenv.shell} -x

          ${config.programs.emacs.package}/bin/emacsclient --create-frame $@
        '';
      };

      "bin/extract-coverage" = {
        executable = true;
        source = ./extract-coverage.py;
      };
      "bin/coverage-report" = {
        executable = true;
        text = ''
          # -*- sh -*-

          coverage json
          for env in py3 py3-dev py3-django2 py3-django2-dev; do
              sed -e s"/.tox\/$env\/lib\/python3.10\/site-packages\///g" -i coverage.json
          done
        '';
      };

      ".emacs.d/init.el".source = ./init.el;
      ".emacs.d/init.d".source = ./init.d;
      # # org-trello credentials configuration
      # ".emacs.d/extra-init.d/ardumont.el".text = ''
      #   (use-package org-trello
      #    :config
      #      (setq org-trello-consumer-key "${consumer-key}"
      #            org-trello-access-token "${access-token}"))
      # '';
    };
  };
}
