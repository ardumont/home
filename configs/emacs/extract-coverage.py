#!/usr/bin/env python3

# Universal code coverage transformer
import sys
import os
import json
from typing import List, Optional, Dict, Any


class Coverage:
    """Represents code coverage for one file.

       Use 2 lists: covered and uncovered lines"""

    def __init__(self, covered: List[str], uncovered: List[str]) -> None:
        self.covered = covered
        self.uncovered = uncovered

    def __str__(self) -> str:
        return f"{'|'.join(self.covered)}\n{'|'.join(self.uncovered)}"


def extract_python_coverage(fname: str) -> Optional[Coverage]:
    """Extract python coverage from current directory. Returns None if no python coverage
       exist

    The coverage file expected is the .coverage outputed as coverage.json (as per
    `coverage json` output).

    Args:
        fname: code source filename to retrieve coverage information from

    Returns:
        Coverage instance

    """
    # output from: coverage json
    python_coverage_file = "coverage.json"
    if not os.path.exists(python_coverage_file):
        return None
    # Get the coverage info
    with open(python_coverage_file) as f:
        coverage_info = json.loads(f.read())

    coverage_fname = coverage_info.get("files", {}).get(fname, {})
    if not coverage_fname:
        return None

    covered: List[str] = map(str, coverage_fname["executed_lines"])
    uncovered: List[str] = map(str, coverage_fname["missing_lines"])
    return Coverage(covered, uncovered)


def extract_emacs_lisp_coverage(fname: str) -> Optional[Coverage]:
    """Extract python coverage from current directory. Returns None if no python coverage
       exist

    The coverage file expected is the .coverage outputed as coverage.json (as per
    `coverage json` output).

    Args:
        fname: code source filename to retrieve coverage information from

    Returns:
        Coverage instance

    """
    # output from: coverage json
    python_coverage_file = "undercover.json"
    if not os.path.exists(python_coverage_file):
        return None
    # Get the coverage info
    with open(python_coverage_file) as f:
        coverage_info = json.loads(f.read())

    coverage_fnames = {}
    for source_file in coverage_info["source_files"]:
        name = source_file["name"]
        coverage_fnames[name] = source_file["coverage"]

    coverage_fname = coverage_fnames.get(fname, {})
    if not coverage_fname:
        return None

    covered: List[str] = []
    uncovered: List[str] = []
    for linenum, line in enumerate(coverage_fname):
        lst = uncovered if line is None else covered
        lst.append(str(linenum))

    return Coverage(covered, uncovered)


if __name__ == '__main__':
    fname = sys.argv[1]
    # Try to parse the different kind of coverage that we know about
    extract_fn = [
        # extract_go_coverage,
        # extract_ruby_coverage,
        extract_python_coverage,
        extract_emacs_lisp_coverage
    ]

    for f in extract_fn:
        res = f(fname)
        # If we find one that makes sense, print it and exit
        if res is not None:
            print(res)
            sys.exit(0)
    print("Coverage not found!")
    sys.exit(1)
