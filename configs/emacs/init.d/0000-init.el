;;; 0000-init.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont (ardumont)

;; Author: Antoine R. Dumont (ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; customized development languages to use with lsp
(defvar language-mode-hook-pack '()
  "List of language modes used internally to configure common configurations.")

(defvar python-path-setup 'lsp "Determine whether to configure elpy or lsp")

(defvar global-wrap-line-number 88 "Global number of lines to wrap text.")

(defvar theme-pack-theme-name 'doom "Determine theme-pack to use (e.g spacemacs)")
(defvar theme-pack-theme-nature 'dark "Default dark (or light) theme.")

;; default indentation setup
(setq-default indent-tabs-mode nil  ;; only insert space on tab completion
              fill-column global-wrap-line-number
              tab-width 4)

(defun trailing-whitespace-hook ()
  (setq show-trailing-whitespace t))

(add-hook 'prog-mode-hook 'trailing-whitespace-hook)
(add-hook 'text-mode-hook 'trailing-whitespace-hook)

(defun init-lisp-hooks (lisp-hooks)
  "Initialize common functions to various lisp modes in LISP-HOOKS."
  (require 'paredit)
  (require 'fold-dwim)
  ;; external
  (require 'eval-sexp-fu)
  (require 'page-break-lines)

  (add-hook 'ielm-mode-hook 'paredit-mode)
  ;; to display beautiful lines instead of ugly ^L
  (when (boundp global-page-break-lines-mode)
    (global-page-break-lines-mode t))

  (when-let (map paredit-mode-map)
    (define-key map (kbd "C-w") 'kill-region)
    (define-key map (kbd "C-M-h") 'backward-kill-sexp)
    (define-key map (kbd "M-s") 'paredit-splice-sexp)
    (define-key map (kbd "M-S") 'paredit-split-sexp)
    (define-key map (kbd "C-h") 'paredit-backward-delete)

    (define-key map (kbd "M-(") nil)
    (define-key map (kbd "M-(") 'paredit-backward-slurp-sexp)
    (define-key map (kbd "M-)") nil)
    (define-key map (kbd "M-)") 'paredit-forward-slurp-sexp)

    (define-key map (kbd "M-?") nil)) ;; unset the help key

  ;; Add multiple modes to lispy modes
  (dolist (fn '(enable-paredit-mode
                show-paren-mode
                rainbow-delimiters-mode
                ;; aggressive-indent-mode
                (lambda ()
                  (local-set-key (kbd "C-c s t") 'fold-dwim-toggle)
                  (local-set-key (kbd "C-c s h") 'fold-dwim-hide-all)
                  (local-set-key (kbd "C-c s s") 'fold-dwim-show-all))))
    (dolist (hook lisp-hooks)
      (add-hook hook fn)))

  ;; Customize Rainbow Delimiters.

  (require 'rainbow-delimiters)
  (set-face-foreground 'rainbow-delimiters-depth-1-face "#c66")  ; red
  (set-face-foreground 'rainbow-delimiters-depth-2-face "#6c6")  ; green
  (set-face-foreground 'rainbow-delimiters-depth-3-face "#69f")  ; blue
  (set-face-foreground 'rainbow-delimiters-depth-4-face "#cc6")  ; yellow
  (set-face-foreground 'rainbow-delimiters-depth-5-face "#6cc")  ; cyan
  (set-face-foreground 'rainbow-delimiters-depth-6-face "#c6c")  ; magenta
  (set-face-foreground 'rainbow-delimiters-depth-7-face "#ccc")  ; light gray
  (set-face-foreground 'rainbow-delimiters-depth-8-face "#999")  ; medium gray
  (set-face-foreground 'rainbow-delimiters-depth-9-face "#666")  ; dark gray

  ;; checking parenthesis at save time
  (add-hook 'before-save-hook 'check-parens nil t))

(custom-set-variables
 ;; Sets the *scratch* to org mode as default
 '(initial-major-mode 'org-mode)
 '(initial-buffer-choice (let ((scratch-filepath (expand-file-name "~/scratch.org")))
                           (when (file-exists-p scratch-filepath)
                             scratch-filepath)))
 '(initial-scratch-message
   "#+title: This buffer is for text that is not saved\n\n")
 ;; Prevents the Emacs Startup menu
 '(inhibit-splash-screen t)
 '(confirm-nonexistent-file-or-buffer t)
 ;; Save pastebin to kill ring
 '(save-interprogram-paste-before-kill t)
 ;; Hides uninteresting files
 '(dired-omit-mode t)
 ;; Removing the beep when error
 '(ring-bell-function 'ignore)
 ;; Prevent Read-only warnings in prompt
 '(minibuffer-prompt-properties
   '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt))
 ;; Prevent emacs from showing GUI-dialogs
 '(use-dialog-box nil)
 ;; Indentation
 '(standard-indent 4)
 '(tab-width 4)
 '(css-indent-offset 4)
 '(c-basic-offset 4)
 '(history-length 1000)
 ;; Always end files with a newline character.
 '(require-final-newline t)
 ;; Don't want to move based on visual line.
 '(line-move-visual nil)
 ;; Disable backups
 '(auto-save-default nil)
 '(create-lockfiles nil)
 '(make-backup-files nil)
 '(backup-inhibited t)
 ;; Sets the window title to more useful
 '(frame-title-format '("emacs@" system-name ": %b %+%+ %f"))
 ;; Only one space after sentence
 '(sentence-end-double-space nil)
 ;; Show keystrokes at once in the bottom
 '(echo-keystrokes 0.1)
 ;; Shell mode character fix
 '(system-uses-terminfo nil)
 ;; Time before asking for su pass again
 '(password-cache-expiry (* 60 15))
 '(fringes-outside-margins t)
 ;; for desktop-mode
 '(desktop-restore-frames nil)
 ;; Scrolling settings
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount '(1 ((shift) . 1)))
 '(mouse-wheel-follow-mouse t)
 '(show-paren-delay 0))

;; No blinking cursor
(blink-cursor-mode -1)
;; Moves the cursor if in the way
(mouse-avoidance-mode 'animate)
;; Delete region on typing, like other editors
(delete-selection-mode 1)
;; y for yes and n for no
; (defalias 'yes-or-no-p 'y-or-n-p)
(winner-mode 1)
(savehist-mode 1)
(global-auto-revert-mode t)
;; Move in CamelCasing
(global-subword-mode 1)
;; Highlight mathing parentese
(show-paren-mode)
;; show linenumber in modebar
(line-number-mode 1)
;; show linenumber in modebar
(column-number-mode 1)
(transient-mark-mode 1)
(electric-indent-mode +1)

;; Unbind annoying bindings
(global-unset-key [(control z)])
(global-unset-key [(control x)(control z)])

(provide '0000-init)
;;; 0000-init.el ends here
