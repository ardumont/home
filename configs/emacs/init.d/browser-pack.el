;;; browser-pack.el --- Browser manipulation

;;; Commentary:

;;; Code:

(use-package browse-url
  :config
  (custom-set-variables
   '(browse-url-browser-function 'browse-url-generic)
   '(browse-url-generic-program (or (getenv "BROWSER") (executable-find "qutebrowser")))))

(use-package restclient)

(provide 'browser-pack)
;;; browser-pack.el ends here
