;;; common-lisp-pack.el ---                          -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont

;; Author: Antoine R. Dumont <tony@yavin4>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq inferior-lisp-program (executable-find "sbcl"))

;; (use-package slime
;;   :config
;;   (init-lisp-hooks '(slime-repl-mode-hook))

;;   (defun override-slime-del-key ()
;;     (define-key slime-repl-mode-map
;;       (read-kbd-macro paredit-backward-delete-key) nil))

;;   (add-hook 'slime-repl-mode-hook 'override-slime-del-key)

;;   (use-package slime-company
;;     :config
;;     (slime-setup '(slime-company))
;;     (add-hook 'slime-mode-hook 'slime-company)

;;     (define-key company-active-map (kbd "C-n") 'company-select-next)
;;     (define-key company-active-map (kbd "C-p") 'company-select-previous)
;;     (define-key company-active-map (kbd "C-d") 'company-show-doc-buffer)
;;     (define-key company-active-map (kbd "M-.") 'company-show-location)

;;     (custom-set-variables
;;      '(slime-company-completion 'fuzzy)
;;      '(slime-company-after-completion 'slime-company-just-one-space))))

(let ((quicklisp-home (format "%s/.config/quicklisp" (getenv "HOME")))
      (quicklisp-cmd (executable-find "quicklisp")))
  (unless (and quicklisp-cmd (file-exists-p quicklisp-home))
    (shell-command (format "quicklisp init --quicklisp-dir %s" quicklisp-home))))

(use-package sly
  :config
  (use-package sly-mrepl
    :config
    (init-lisp-hooks '(sly-mrepl-hook))
    (add-hook 'sly-mrepl-hook #'company-mode)

    (use-package helm-company
      :config
      (define-key sly-mrepl-mode-map (kbd "<tab>") 'helm-company))))

(provide 'common-lisp-pack)
;;; common-lisp-pack.el ends here
