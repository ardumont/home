;;; coverage.el --- Display coverage in buffer linum  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Antoine R. Dumont

;; Author: Antoine R. Dumont <tony@yavin4>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cov)

(custom-set-faces
 '(cov-none-face  ((((class color)) :foreground "red")))
 '(cov-light-face ((((class color)) :foreground "orange")))
 '(cov-med-face   ((((class color)) :foreground "yellow")))
 '(cov-heavy-face ((((class color)) :foreground "green"))))

(require 'dash)

(defvar coverage-extract-cmd (expand-file-name "~/bin/extract-coverage"))
(defvar-local coverage-data-uncovered nil)
(defvar-local coverage-data-covered nil)

(defun coverage-line-fringe (linenum)
  "Given LINENUM a line number return a propertized fringe for it"
  (let ((color (cond
                ((member linenum coverage-data-uncovered) "red")
                ((and (not (member linenum coverage-data-uncovered))
                      (member linenum coverage-data-covered)) "green")
                ((and (not (member linenum coverage-data-uncovered))
                      (not (member linenum coverage-data-covered))) ""))))
    (propertize " " 'face `(:background ,color :foreground " "))))

(defun coverage-compute ()
  "Compure coverage data for current buffer"
  (interactive)
  (coverage-set-data-for-project-file (buffer-file-name))
  (linum-mode -1)
  (linum-mode t))

(defun coverage-parse-result (coverage-line)
  "Parse result out of a COVERAGE-LINE (format: x|y|z... ) -> '(\"x\", ...)"
  (->> (split-string coverage-line "|")
       (-map #'string-to-number)))

(defun coverage-extract (root fname)
  "Extract coverage information from ROOT directory for the filename FNAME."
  (let* (;; Relative fname of the requested file
         (relative-fname (replace-regexp-in-string root "" fname))
         ;; Command to run to get coverage, using external script
         (cmd (format "cd '%s' && %s '%s'" root coverage-extract-cmd relative-fname)))
    ;; Raw coverage information returned by external script
    (split-string (shell-command-to-string cmd) "\n")))

(defun coverage-set-data-for-project-file (fname)
  "Process coverage data for FNAME and set the globals use to display coverage"
  (let* ((root (projectile-project-root))
         ;; Raw coverage information returned by external script
         (coverage-all-lines (coverage-extract root fname))
         (covered-lines (car coverage-all-lines))
         (uncovered-lines (cadr coverage-all-lines)))
    (setq coverage-data-covered (coverage-parse-result covered-lines)
          coverage-data-uncovered (coverage-parse-result uncovered-lines))))

(define-minor-mode coverage-mode
  "Allow annotating the file with coverage information"
  :lighter coverage-mode-text
  (if coverage-mode
      (progn
        (linum-mode t)
        (setf linum-format 'coverage-line-fringe)
        (coverage-compute))
    (setf linum-format 'dynamic)
    (linum-delete-overlays)
    (linum-mode -1)))

(provide 'coverage)
;;; coverage.el ends here
