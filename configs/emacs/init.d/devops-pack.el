;;; devops-pack.el --- Devops tools           -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022  Antoine R. Dumont (@ardumont)

;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package terraform-mode
  :config
  (custom-set-variables '(terraform-indent-level 2)))

(use-package groovy-mode
  :config
  (add-to-list 'auto-mode-alist '("j2\\'" . groovy-mode))
  (custom-set-variables '(groovy-indent-offset 2)))

(provide 'devops-pack)
;;; devops-pack.el ends here
