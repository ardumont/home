;;; emacs-pack.el --- Utility commands to (re)load my local tweaks

;; Copyright (C) 2022  Antoine R. Dumont

;; Author: Antoine R. Dumont <tony@dagobah>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun emacs-pack-load-file (non-expanded-filepath)
  "Load NON-EXPANDED-FILEPATH if it exists."
  (let ((filepath (expand-file-name non-expanded-filepath)))
    (when (file-exists-p filepath)
      (load-file filepath))))

(defun emacs-pack-load-configuration ()
  "Load installed emacs configuration (out of guix or nix)."
  (interactive)
  (emacs-pack-load-file "~/.emacs.d/init.el"))

(defun emacs-pack-load-dev-configuration ()
  "Load emacs (dev) configuration (out of the private repository declaration)"
  (interactive)
  (emacs-pack-load-file "~/repo/private/guix-home/emacs/init.el"))

(defvar emacs-pack-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c c l") 'emacs-pack-load-configuration)
    (define-key map (kbd "C-c c d") 'emacs-pack-load-dev-configuration)
    map)
  "Keymap for emacs-pack mode.")

(define-minor-mode emacs-pack-mode
  "Minor mode to consolidate emacs-pack extensions.

\\{emacs-pack-mode-map}"
  :lighter " ɛ"
  :keymap emacs-pack-mode-map
  :global t)

(defun emacs-pack-on ()
  "Turn on `emacs-pack-mode'."
  (emacs-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode global-emacs-pack-mode emacs-pack-mode emacs-pack-on)

(provide 'emacs-pack)
;;; emacs-pack.el ends here
