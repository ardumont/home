;;; employeur.el --- Calcule paie employe.
;;; Commentary: Seulement valable en France.
;;; Code;

(defun charge-non-imposable (charge-coeff salaire-brut)
  "Calcule la charge non imposable CHARGE-COEFF sur le SALAIRE-BRUT."
  (/ (* charge-coeff salaire-brut) 100))

;; (charge-non-imposable 0.95 522.72) ;; 4.97
;; (charge-non-imposable 6.9 522.72)  ;; 36.07

(defun charge-imposable (charge-coeff salaire-brut)
  "Calcule la charge imposable CHARGE-COEFF sur le SALAIRE-BRUT."
  (let ((salaire (* 0.9825 salaire-brut)))
    (/ (* charge-coeff salaire) 100)))

;; (charge-imposable 2.4 522.72)  ;; 12.33

(defun charges-salariales (salaire-brut)
  "Calcule les charges salariales a partir du SALAIRE-BRUT."
  (let ((assedic-assurance-chomage     0)
        (ss-maladie-invalidite-deces   0)
        (ss-veuvage                    0.4)
        (ss-vieillesse                 6.9)
        (ircem-retraite-complementaire 3.1)
        (ircem-prevoyance              1.15)
        (agff                          0.8)
        (crds-imposable                0.5)
        (csg-imposable                 2.4)
        (csg-non-imposable             6.8))
    (+ (charge-non-imposable salaire-brut assedic-assurance-chomage)
       (charge-non-imposable salaire-brut ss-maladie-invalidite-deces)
       (charge-non-imposable salaire-brut ss-veuvage)
       (charge-non-imposable salaire-brut ss-vieillesse)
       (charge-non-imposable salaire-brut ircem-retraite-complementaire)
       (charge-non-imposable salaire-brut ircem-prevoyance)
       (charge-non-imposable salaire-brut agff)
       (charge-imposable salaire-brut crds-imposable)
       (charge-imposable salaire-brut csg-imposable)
       (charge-imposable salaire-brut csg-non-imposable))))

(defun nombre-heures-normales ()
  "Calcule le nombre d'heures du travail mensualise sur une annee."
  (let* ((nb-semaines-travail  36)
         (nb-heure-par-semaine 36)) ;; 4 jours 8h-17h ~> 36h
    (/ (* nb-semaines-travail nb-heure-par-semaine) 12)))

;; (nombre-heures-normales)  ;; 108

(defun salaire-net ()
  "Calcule le salaire net."
  (let* ((salaire-brut-horaire           4.4)
         (nb-semaines-travail            36)
         (nb-heure-par-semaine           36) ;; 4 jours 8h-17h ~> 36h
         (salaire-brut-base-mensuel      (* salaire-brut-horaire (nombre-heures-normales)))
         (conges-payes                   (* 0.1 salaire-brut-base-mensuel))
         (salaire-brut                   (+ salaire-brut-base-mensuel conges-payes))
         (charges-salariales             (charges-salariales salaire-brut)))
    (- salaire-brut charges-salariales)))

;; (salaire-net) ;; 403.38

(defun indemnite-journalier-entretien (nb-jours)
  "Calcule les indemnites entretien pour NB-JOURS."
  (let ((indemnite-journalier-entretien 3.5))
    (* indemnite-journalier-entretien nb-jours)))

;; (indemnite-journalier-entretien 12) ;; 42.0

(defun indemnite-journalier-repas (nb-jours)
  "Calcule les indemnites repas pour NB-JOURS."
  (let ((indemnite-journalier-repas 3.5))
    (* indemnite-journalier-repas nb-jours)))

;; (indemnite-journalier-repas 12) ;; 42.0

(defun paie-avec-indemnites (nb-jours)
  "Calcule la paie en fonction de NB-JOURS de travail effectif du mois."
  (+ (salaire-net)
     (indemnite-journalier-entretien nb-jours)
     (indemnite-journalier-repas     nb-jours)))

;;; employeur.el ends here
