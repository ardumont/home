;;; ereader-pack.el ---                              -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022  Antoine R. Dumont (ardumont)

;; Author: Antoine R. Dumont (ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; epub-reader from emacs

;;; Code:

(use-package nov
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

  (custom-set-variables
   '(nov-text-width global-wrap-line-number))

  (add-hook 'nov-mode-hook
            (lambda () (setq show-trailing-whitespace nil))))

(provide 'ereader-pack)
;;; ereader-pack.el ends here
