;;; git-pack.el --- Git configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont

;; Author: Antoine R. Dumont <tony@dagobah>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;; Work around recent magit issues regarding the introduction of seq-keep
;; and the current emacs 28 packaged by nix which comes with seq 2.23
;; This fixes at least the magit ui status summary view
;; https://github.com/magit/magit/issues/5011#issuecomment-1732622958
(defun seq-keep (function sequence)
  "Apply FUNCTION to SEQUENCE and return the list of all the non-nil results."
  (delq nil (seq-map function sequence)))

(use-package magit
  :config
  (when-let ((git (executable-find "git")))
    (custom-set-variables
     `(magit-git-executable ,git)))

  (define-key magit-mode-map (kbd "C-n") 'next-line)
  (define-key magit-mode-map (kbd "C-p") 'previous-line))

;; Disable internal emacs' version control interface
(custom-set-variables '(vc-handled-backends (delq 'Git vc-handled-backends)))

(use-package forge
  :config

  (custom-set-variables
   '(forge-topic-list-limit '(60 . 5)))

  (defun git-pack--add-gitlab-forge (forge-host)
    (let ((gitlab-entry (list forge-host (format "%s/api/v4" forge-host) forge-host 'forge-gitlab-repository)))
      (add-to-list 'forge-alist gitlab-entry)))

  ;; Add custom forges
  (mapc #'git-pack--add-gitlab-forge
      '("gitlab-staging.swh.network" "gitlab.softwareheritage.org")))


;; github
;; (ghub-request "GET" "/users" nil
;;               :forge 'gitlab
;;               :host "gitlab.softwareheritage.org/api/v4"
;;               :username "ardumont"
;;               :auth 'forge)

(use-package git-gutter
  :config
  (add-hook 'prog-mode-hook 'git-gutter-mode)
  (custom-set-variables
   '(git-gutter:modified-sign " ") ;; two space
   '(git-gutter:added-sign "+") ;; multiple character is OK
   '(git-gutter:deleted-sign "-")
   '(git-gutter:lighter " GG")
   '(git-gutter:disabled-modes '(org-mode)))

  (set-face-background 'git-gutter:modified "blue") ;; background color
  (set-face-foreground 'git-gutter:added "green")
  (set-face-foreground 'git-gutter:deleted "red"))

(defvar git-pack-mode-map nil "Keymap for git-pack mode.")
;; Initialized empty to ease update later on
(setq git-pack-mode-map
      (let ((map (make-sparse-keymap)))
        (define-key map (kbd "C-c d s") 'git-gutter:stage-hunk)
        (define-key map (kbd "C-c d t") 'git-gutter:toggle)
        (define-key map (kbd "C-c d p") 'git-gutter:previous-hunk)
        (define-key map (kbd "C-c d n") 'git-gutter:next-hunk)
        (define-key map (kbd "C-c d d") 'git-gutter:popup-hunk)
        (define-key map (kbd "C-c d r") 'git-gutter:revert-hunk)
        (define-key map (kbd "C-c d g") 'magit-status)
        (define-key map (kbd "C-c g")   'magit-status)
        map))

(define-minor-mode git-pack-mode
  "Minor mode to consolidate git-pack extensions.

\\{git-pack-mode-map}"
  :lighter " γ"
  :keymap git-pack-mode-map
  :global t)

(defun git-pack-on ()
  "Turn on `git-pack-mode'."
  (git-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode global-git-pack-mode git-pack-mode git-pack-on)

(provide 'git-pack)
;;; git-pack ends here
