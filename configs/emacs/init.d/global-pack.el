;;; global-pack.el --- lsp-mode related setup

;; Copyright (C) 2021  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:
(require 'lsp)
(require 'lsp-ui)
(require 'lsp-ui-flycheck)

(defun global-pack-reload-supported-lsp-modes ()
  "Allow (re)loading of newly supported lsp modes."
  (interactive)
  (dolist (hook-fn language-mode-hook-pack)
    (add-hook hook-fn #'lsp)))

;; Force to use the define-key stanza as the next command does not work
(define-key lsp-mode-map (kbd "C-c l") lsp-command-map)

(custom-set-variables
 ;; not working
 ;; '(lsp-keymap-prefix "C-c l")
 ;; Prefer using lsp-ui (flycheck) over flymake.
 '(lsp-prefer-flymake nil))

(define-key lsp-mode-map (kbd "M-.") 'lsp-find-definition)
(define-key lsp-mode-map [remap xref-find-apropos] #'helm-lsp-workspace-symbol)
(define-key lsp-mode-map (kbd "C-c C-d") #'lsp-describe-thing-at-point)
(define-key lsp-mode-map (kbd "C-c <deletechar>") #'lsp-describe-thing-at-point)

(define-key lsp-ui-mode-map (kbd "C-c l f") #'lsp-ui-peek-find-definitions)
(define-key lsp-ui-mode-map (kbd "C-c l r") #'lsp-ui-peek-find-references)

(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; Extend the default company mode mapping
(add-hook 'company-mode-hook
          (lambda ()
            (interactive)
            (define-key company-active-map (kbd "C-h") 'delete-backward-char)
            (define-key company-active-map (kbd "M-?") 'company-show-doc-buffer)
            (define-key company-active-map (kbd "C-n") 'company-select-next)
            (define-key company-active-map (kbd "C-p") 'company-select-previous)
            (define-key company-active-map (kbd "M-/") 'company-complete)))

(push 'company-lsp company-backends)
(custom-set-variables
 '(company-idle-delay 0.1)
 ;; Disable client-side cache because the LSP server does a better job.
 '(company-transformers nil)
 '(company-lsp-async t)
 '(company-lsp-cache-candidates nil))

(custom-set-variables
 '(lsp-ui-doc-enable t)
 '(lsp-ui-doc-use-childframe t)
 '(lsp-ui-doc-position 'at-point)
 '(lsp-ui-doc-include-signature t)
 '(lsp-ui-doc-show-with-cursor t)
 '(lsp-ui-doc-show-with-mouse nil)
 '(lsp-ui-sideline-update-mode 'point)
 '(lsp-ui-flycheck-list-position 'bottom)
 '(lsp-ui-peek-enable t)
 '(lsp-ui-peek-list-width 100)
 '(lsp-ui-peek-peek-height 50))

(add-hook 'lsp-mode-hook 'lsp-ui-mode)

(custom-set-variables
 '(display-fill-column-indicator-mode "|"))

(global-display-fill-column-indicator-mode)

(provide 'global-pack)
;;; global-pack.el ends here
