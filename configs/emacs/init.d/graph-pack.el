;;; graph-pack.el ---                             -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2024  Antoine R. Dumont

;; Author: Antoine R. Dumont <tony@yavin4>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package plantuml-mode
  :config
  (custom-set-variables
   '(plantuml-executable-path (executable-find "plantuml"))
   '(plantuml-default-exec-mode 'executable)
   '(plantuml-output-type "png") ;; svg is unreadable in dark theme
   '(plantuml-indent-level 2))

  ;; Enable plantuml-mode for PlantUML files
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
  (add-to-list 'auto-mode-alist '("\\.uml\\'" . plantuml-mode)))

(use-package mermaid-mode
  :config
  (custom-set-variables
   '(mermaid-mmdc-location (executable-find "mmdc"))))

(provide 'graph-pack)
;;; graph.el ends here
