;;; guix-pack.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont (ardumont)

;; Author: Antoine R. Dumont (ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package scheme
  :config

  ;; guix related setup
  (let ((guix-home (format "%s/repo/public/guix/guix" (getenv "HOME"))))
    (when (file-exists-p guix-home)
      (require 'yasnippet)
      (with-eval-after-load 'yasnippet
        (add-to-list 'yas-snippet-dirs (format "%s/etc/snippets" guix-home)))

      ;; Assuming the Guix checkout is in ~/src/guix.
      (load-file (format "%s/etc/copyright.el" guix-home))

      (setq copyright-names-regexp
            (format "%s <%s>" user-full-name user-mail-address))

      (use-package tramp
        :config
        (add-to-list 'tramp-remote-path "/run/current-system/profile/bin"))

      (custom-set-variables '(scheme-mit-dialect nil))
      (init-lisp-hooks '(scheme-mode-hook)))))

(use-package geiser-impl
  :config
  (custom-set-variables
   '(geiser-default-implementation 'guile)))

(use-package geiser-repl
  :config
  (init-lisp-hooks '(geiser-repl-mode-hook)))

(defun guix-pack-connect-repl ()
  "Connect to guix repl using default host and port."
  ;; The guix repl should be spawned by ~/bin/remote-guix-repl
  ;; loading the local clone of the guix source tree (running with the necessary deps)
  (interactive)
  (require 'geiser)
  (geiser-connect 'guile geiser-repl-default-host geiser-repl-default-port))

;; not ready yet
(defun guix-bug-report-at-point ()
  "Insert guix bug report at point."
  (interactive)
  (let ((text (shell-command-to-string "guix bug-report -h")))
    (with-current-buffer (current-buffer)
      (insert text))))

;; (setenv "GUIX_EXTENSIONS_PATH" "/home/tony/repo/public/guix/guix/guix/extensions")
;; (getenv "GUIX_EXTENSIONS_PATH")

(provide 'guix-pack)
;;; guix-pack.el ends here
