;;; helm-pack.el --- Helm default bindings

;; Copyright (C) 2015-2022  Antoine R. Dumont (@ardumont)

;; Author: ardumont <eniotna.t@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(use-package helm
  :config
  (setq helm-input-idle-delay                     0.01
        helm-reuse-last-window-split-state        t
        helm-always-two-windows                   nil
        helm-split-window-inside-p                nil
        helm-commands-using-frame                 '(completion-at-point
                                                    ;; helm-apropos
                                                    helm-eshell-prompts helm-imenu
                                                    helm-imenu-in-all-buffers)
        helm-actions-inherit-frame-settings       t
        helm-use-frame-when-more-than-two-windows t
        helm-use-frame-when-dedicated-window      t
        helm-show-action-window-other-window      'left
        helm-allow-mouse                          nil
        helm-move-to-line-cycle-in-source         t
        helm-autoresize-max-height                80 ; it is %.
        helm-autoresize-min-height                10 ; it is %.
        helm-follow-mode-persistent               t
        helm-candidate-number-limit               100
        helm-visible-mark-prefix                  "✓"
        helm-display-header-line                  nil)

  (add-to-list 'helm-sources-using-default-as-input 'helm-source-info-bash)

  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "C-x b") 'helm-buffers-list)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)

  (define-key helm-map (kbd "C-h") 'delete-backward-char)
  (define-key helm-map (kbd "C-d") 'delete-forward-char)
  (define-key helm-map (kbd "M-? C-d") 'helm-debug-output)
  (define-key helm-map (kbd "M-? h") 'helm-help)
  (global-set-key (kbd "M-y") 'helm-show-kill-ring)

  (add-to-list 'display-buffer-alist
             `(,(rx bos "*helm" (* not-newline) "*" eos)
               (display-buffer-in-side-window)
               (inhibit-same-window . t)
               (window-height . 0.3)))

  (add-hook 'helm-find-files-after-init-hook
            (lambda ()
              (define-key helm-find-files-map (kbd "~")
                (lambda ()
                (interactive)
                (if (looking-back "/")
                    (insert "~/")
                  (call-interactively 'self-insert-command)))))))

(provide 'helm-pack)
;;; helm-pack.el ends here
