;;; help-pack.el --- A pack to reference the help modes in mastering emacs

;;; Commentary:

;;; Code:
(use-package which-key
  :config
  (which-key-mode +1)
  (custom-set-variables '(which-key-popup-type 'minibuffer)))

(use-package helpful
  :config (global-set-key (kbd "C-c ? .") #'helpful-at-point))

(provide 'help-pack)
;;; help-pack.el ends here
