;;; kill-pack.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022  Antoine R. Dumont

;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defgroup kill-pack nil " Kill-pack group"
  :tag "Kill-pack"
  :version "0.0.1")

(defcustom kill-pack-command (executable-find "xsel")
  "Base command of the pack (default to xsel)"
  :group 'kill-pack)

(defcustom kill-pack-kill (format "%s --input --clipboard" kill-pack-command)
  "Kill command"
  :group 'kill-pack)

(defcustom kill-pack-yank (format "%s --output --clipboard" kill-pack-command)
  "Yank command"
  :group 'kill-pack)

;;;###autoload
(defun kill-pack-save-to-x-clipboard ()
  "Permit kill-saving text to and from X11 clipboard.

  This beats the heck out of manually copying with the cursor."
  (interactive)
  (progn
    (shell-command-on-region (region-beginning) (region-end) kill-pack-kill)
    (message "Kill-saved region to clipboard!")
    (deactivate-mark)))

;;;###autoload
(defun kill-pack-yank-from-x-clipboard ()
  (interactive)
  (progn
    (insert (shell-command-to-string kill-pack-yank)))
  (message "Yanked region from clipboard!"))

(define-minor-mode kill-pack
  "Toggle `kill-pack-mode'.  Allows Converts words in DOuble CApitals to
Single Capitals as you type."
  :init-value nil
  :group 'kill-pack)

(global-set-key (kbd "C-c k") 'kill-pack-save-to-x-clipboard)
(global-set-key (kbd "C-c y") 'kill-pack-yank-from-x-clipboard)

(provide 'kill-pack)
;;; kill-pack.el ends here
