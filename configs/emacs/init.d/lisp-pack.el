;;; lisp-pack.el --- Some common setup between the multiple lisp runtime

;; Copyright (C) 2018-2020  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

;; internal libs
(require 'lisp-mode)
(require 'files)
(require 'paren)

;; clojure

(init-lisp-hooks
 '(lisp-mode-hook
   inferior-lisp-mode-hook
   lisp-mode-hook
   lisp-interaction-mode-hook
   ielm-mode-hook
   eval-expression-minibuffer-setup-hook))

(provide 'lisp-pack)
;;; lisp-pack.el ends here
