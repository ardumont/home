;;; mail-pack.el --- A `pack` to setup your email accounts through a ~/.authinfo(.gpg) credentials file

;; Copyright (C) 2014-2022 Antoine R. Dumont <eniotna.t AT gmail.com>

;; Author: Antoine R. Dumont <eniotna.t AT gmail.com>
;; Maintainer: Antoine R. Dumont <eniotna.t AT gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((dash "2.11.0") (dash-functional "2.11.0") (creds "0.0.6.1") (async "1.3") (s "1.9.0"))
;; Keywords: emails notmuch configuration
;; URL: https://github.com/ardumont/mail-pack

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING. If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; A `pack` to setup your email accounts through a ~/.authinfo(.gpg) credentials file
;;
;; Multiple accounts can be configured through the ~/.authinfo(.gpg) routine.
;; While the format is not that good to parse, existing modules uses it to
;; access credentials information (smtp). So might as well centralize email
;; information there as well.
;;
;; This modules aims to declare emails accounts information in that file in
;; order to be able to browse one's emails through the same notmuch interface.
;; While still being able to send or reply to email with the right account.
;;
;; It relies on:
;; - sync mailboxes (the sync technology is out of scope here, e.g offlineimap, ...)
;; - notmuch-emacs: to read emails through emacs
;; - notmuch
;; - internal emacs libs to send email through smtp
;; - google-contacts for address completion
;;
;; You need to setup your credentials file "~/.authinfo" for this to work. (The
;; credentials file can be secured with gpg or not).
;;
;; A single account configuration file ’~/.authinfo.gpg’ would be:
;; machine email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server> email-alias <other-email>
;; machine <smtp-server> login <your-email> port <smtp-port> password <your-mail-password-or-dedicated-token>
;;
;; A multiple account configuration file ’~/.authinfo.gpg’ would be:
;; machine email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>
;;
;; machine <smtp-server> login <login> port <smtp-port> password <your-mail-password-or-dedicated-token>
;; machine 2-email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>
;;
;; machine <smtp-server> login <2nd-email> port <smtp-port> password <your-mail-password-or-dedicated-token>
;; machine 3-email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>
;;
;; ...
;;
;; As a pre-requisite, you need to install notmuch packages. For example, on
;; debian-based system, ‘sudo aptitude install -y notmuch‘...

;;; Code:


(defgroup mail-pack nil " Mail-pack group"
  :tag "Mail-pack"
  :version "0.0.1")

;; Internal libs
(require 'gnus)
(require 'epa-file)
(require 'smtpmail)

;; External libs
(require 'creds)
(require 'smtpmail-async)
(require 'notmuch)
(require 'helm)

;; ===================== User setup (user can touch this, the preferred approach it to define a hook to override those values)

;; create your .authinfo file and and encrypt it in ~/.authinfo.gpg with M-x epa-encrypt-file
(defcustom mail-pack-mail-root-folder (expand-file-name "~/.mails")
  "The root folder where you store your maildirs folder."
  :group 'mail-pack)

(defcustom mail-pack-credentials-file (expand-file-name "~/.authinfo.gpg")
  "The credentials file where you store your email information.
This can be plain text too."
  :group 'mail-pack)


;; ===================== Static setup (user must not touch this)

(defvar mail-pack-accounts nil "User's accounts (information parsed out of authinfo).")
(defvar mail-pack-account-emails nil "User's emails list.")

(defvar mail-pack-setup-hooks nil "Use hooks for user to set their setup override.")
(setq mail-pack-setup-hooks nil) ;; reset hooks

;; ===================== functions

(defun mail-pack-quote-code ()
  "Insert delimitation code snippet at point."
  (interactive)
  (utils-pack-insert-text-at-point
   "--8<---------------cut here---------------end--------------->8---

--8<---------------cut here---------------end--------------->8---"))

(defun mail-pack-log (&rest args)
  "Log ARGS with specific pack prefix."
  (apply #'message (format "Mail Pack - %s" (car args)) (cdr args)))

(defun mail-pack-pre-requisites-ok-p ()
  "Ensure that the needed installation pre-requisites are met.
Returns nil if problem."
  (if (executable-find "notmuch")
      (require 'notmuch)
    nil))

(defun mail-pack-setup-possible-p (creds-file)
  "Check if CREDS-FILE exists and contain at least one account.
If all is ok, return the creds-file's content, nil otherwise."
  (when (file-exists-p creds-file)
    (let* ((creds-file-content (creds/read-lines creds-file))
           (email-description  (creds/get creds-file-content "email-description"))
           (account-server     (creds/get-entry email-description "smtp-server"))
           (account-email      (creds/get-entry email-description "mail")))
      (when (creds/get-with creds-file-content `(("machine" . ,account-server) ("login" . ,account-email)))
        creds-file-content))))

(defun mail-pack--nb-accounts (creds-file-content)
  "In CREDS-FILE-CONTENT, compute how many accounts exist?"
  (--reduce-from (let ((machine (creds/get-entry it "machine")))
                   (if (and machine (string-match-p "email-description" machine))
                       (+ 1 acc)
                     acc))
                 0
                 creds-file-content))

(defun mail-pack--email-account (account)
  "Given an ACCOUNT, return its user-mail-address entry."
  (car (assoc-default 'user-mail-address account)))

(defun mail-pack--email-accounts (accounts)
  "Given the ACCOUNTS list, return list of emails."
  (mapcar #'mail-pack--email-account accounts))

(defun mail-pack-choose-main-account (possible-accounts)
  "Allow user to choose an account from the POSSIBLE-ACCOUNTS list as main one.
Return the chosen account."
  (if (= 1 (length possible-accounts))
      (car possible-accounts)
    (helm-comp-read "Compose with account: " possible-accounts)))

(defun mail-pack--email-to-account (email accounts)
  "Given an EMAIL, return the associated account from the ACCOUNTS list."
  (->> accounts
       (-filter (lambda (account) (string= email (mail-pack--email-account account))))
       car))

(defun mail-pack-set-main-account ()
  "Switch the current account.
If only one, keep it.
If 2, switch to the other one.
Otherwise, let the user decide which account (s)he wants."
  (interactive)
  (let* ((accounts             mail-pack-accounts)
         (possible-emails      (mail-pack--email-accounts accounts))
         (chosen-email-account (mail-pack-choose-main-account possible-emails)))
    (-> chosen-email-account
        (mail-pack--email-to-account accounts)
        mail-pack--setup-as-main-account)
    (mail-pack-log "Switch to account: %s" chosen-email-account)))

(defun mail-pack-current-account ()
  "Compute the current account."
  user-mail-address)

(defun mail-pack-display-current-account ()
  "Display the current enabled account."
  (interactive)
  (message "Account: %s\nFull name: %s <%s>"
           (mail-pack-current-account) user-full-name user-mail-address))

(defun mail-pack--label (entry-number label)
  "Given an ENTRY-NUMBER, and a LABEL, compute the full label."
  (if (or (null entry-number) (string= "" entry-number))
      label
    (format "%s-%s" entry-number label)))

(defvar mail-pack--saved-searches
  '((:name "andumont"
           :query "tag:lists/andumont"
           :sort-order 'newest-first
           :key "a")
    (:name "college"
           :query "tag:lists/college"
           :sort-order 'newest-first
           :key "C")
    (:name "commandes"
           :query "tag:lists/commandes"
           :sort-order 'newest-first
           :key "c")
    (:name "debian"
           :query "tag:lists/debian"
           :sort-order 'newest-first
           :key "d")
    (:name "emacs"
           :query "tag:lists/emacs"
           :sort-order 'newest-first
           :key "e")
    (:name "guix-devel"
           :query "tag:lists/guix-devel"
           :sort-order 'newest-first
           :key "g")
    (:name "github"
           :query "tag:lists/github"
           :sort-order 'newest-first
           :key "G")
    (:name "inbox"
           :query "tag:inbox"
           :sort-order 'newest-first
           :key "i")
    (:name "famille"
           :query "tag:lists/famille"
           :sort-order 'newest-first
           :key "f")
    (:name "judo"
           :query "tag:judo"
           :sort-order 'newest-first
           :key "j")
    (:name "jobs"
           :query "tag:lists/jobs"
           :sort-order 'newest-first
           :key "h")
    (:name "nixpkgs"
           :query "tag:lists/nix-dev or tag:lists/nixpkgs"
           :sort-order 'newest-first
           :key "n")
    (:name "notmuch"
           :query "tag:lists/notmuch"
           :sort-order 'newest-first
           :key "N")
    (:name "org"
           :query "tag:lists/emacs-orgmode"
           :sort-order 'newest-first
           :key "o")
    (:name "pass"
           :query "tag:lists/password-store"
           :sort-order 'newest-first
           :key "p")
    (:name "quora"
           :query "tag:lists/quora"
           :sort-order 'newest-first
           :key "q")
    (:name "sent"
           :query "tag:sent"
           :sort-order 'newest-first
           :key "t")
    (:name "swh"
           :query "tag:lists/swh"
           :sort-order 'newest-first
           :key "s")
    (:name "swh-notifs"
           :query "tag:lists/swh-notifs"
           :sort-order 'newest-first
           :key "w")
    (:name "twitter"
           :query "tag:lists/twitter"
           :sort-order 'newest-first
           :key "T")
    (:name "xmonad"
           :query "tag:lists/xmonad"
           :sort-order 'newest-first
           :key "x")
    (:name "youtube"
           :query "tag:lists/youtube"
           :sort-order 'newest-first
           :key "y")
    ;; not really interesting
    (:name "unread"
           :query "tag:unread"
           :sort-order 'newest-first
           :key "u")
    (:name "important"
           :query "tag:flagged"
           :sort-order 'newest-first
           :key "I")
    (:name "drafts"
           :query "tag:draft"
           :sort-order 'newest-first
           :key "d")
    (:name "deleted"
           :query "tag:deleted"
           :sort-order 'newest-first
           :key "D")
    (:name "junk"
           :query "tag:spam or tag:junk"
           :sort-order 'newest-first
           :key "J")))

(defconst mail-pack--flag-spam '("+spam" "-inbox" "-unread"))
(defconst mail-pack--flag-deleted '("+deleted" "-inbox" "-unread"))
(defconst mail-pack--flag-unread '("-unread"))

(defun mail-pack--common-configuration ()
  "Install the common configuration between all accounts."
  (custom-set-variables
   '(notmuch-command (executable-find "notmuch"))
   '(notmuch-search-oldest-first nil)
   '(notmuch-show-indent-content nil)
   '(notmuch-crypto-process-mime 'do-verify-signature-and-decrypt-mail-if-need-be)
   '(message-kill-buffer-on-exit t)
   ;; SMTP setup ; pre-requisite: gnutls-bin package installed
   '(message-send-mail-function 'async-smtpmail-send-it)
   ;; no message signature as we use signature-file instead
   '(message-signature nil)
   '(smtpmail-stream-type 'starttls)
   '(starttls-use-gnutls t)
   '(smtpmail-debug-info t)
   '(smtpmail-debug-verb t)
   '(notmuch-saved-searches mail-pack--saved-searches)
   ;; emacs-27 && notmuch-0.31 require both customs below to continue signing
   '(mml-secure-openpgp-sign-with-sender 'please-do-sign)
   '(mml-secure-smime-sign-with-sender 'please-do-sign))

  ;; always sign the entire message
  (add-hook 'message-setup-hook 'mml-secure-message-sign)

  (setq gnus-invalid-group-regexp "[:`'\"]\\|^$")

  ;; show-mode
  (define-key notmuch-show-mode-map "r" 'notmuch-show-reply)
  (define-key notmuch-show-mode-map "R" 'notmuch-show-reply-sender)
  (define-key notmuch-show-mode-map "/" 'notmuch-search)
  (define-key notmuch-show-mode-map "b" (lambda (&optional address)
                                          "Bounce the current message."
                                          (interactive "sBounce To: ")
                                          (notmuch-show-view-raw-message)
                                          (message-resend address)))
  (define-key notmuch-show-mode-map "s" (lambda () (interactive)
                                          (notmuch-show-tag mail-pack--flag-spam)))
  (define-key notmuch-show-mode-map "d" (lambda () (interactive)
                                          (notmuch-show-tag mail-pack--flag-deleted)))

  ;; search-mode
  (define-key notmuch-hello-mode-map "/" 'notmuch-search)
  (define-key notmuch-hello-mode-map "g" 'notmuch-refresh-this-buffer)

  (define-key notmuch-search-mode-map "r" 'notmuch-search-reply-to-thread)
  (define-key notmuch-search-mode-map "R" 'notmuch-search-reply-to-thread-sender)
  (define-key notmuch-search-mode-map "/" 'notmuch-search)
  (define-key notmuch-search-mode-map "s" (lambda () (interactive)
                                            (notmuch-search-tag mail-pack--flag-spam)
                                            (next-line)))
  (define-key notmuch-search-mode-map "d" (lambda () (interactive)
                                            (notmuch-search-tag mail-pack--flag-deleted)
                                            (next-line)))
  (define-key notmuch-search-mode-map "x" (lambda () (interactive)
                                            (notmuch-search-tag mail-pack--flag-unread)
                                            (next-line))))

(defun mail-pack--compute-fullname (firstname surname name &optional pseudo)
  "Given FIRSTNAME, SURNAME, NAME, and PSEUDO, compute the user's fullname."
  (cl-flet ((null-then-empty (v) (if v v "")))
    (s-trim (format "%s %s %s%s"
                    (null-then-empty firstname)
                    (null-then-empty surname)
                    (null-then-empty name)
                    (if pseudo (format " (%s)" pseudo) "")))))

(defun mail-pack--maildir-from-email (mail-address)
  "Compute the maildir (without its root folder) from the MAIL-ADDRESS."
  (car (s-split "@" mail-address)))

(defun mail-pack--setup-as-main-account (account-setup-vars)
  "Given the entry ACCOUNT-SETUP-VARS, set the main account vars up."
  (mapc #'(lambda (var) (set (car var) (cadr var))) (cdr account-setup-vars)))

(defun mail-pack-refile-msg (default-folder)
  "Compute the refiling folder with default DEFAULT-FOLDER.
ARCHIVE-FOLDER is the catch-all folder."
  (lexical-let ((archive-folder default-folder))
    (lambda (msg)
      (mail-pack-rules-filter-msg msg archive-folder))))

(defun mail-pack-new-mail (&optional prompt-for-sender)
  "Compose new mail with the current selected account.

If PROMPT-FOR-SENDER is non-nil, the user will be prompted for
the From: address first."
  (interactive "P")
  (let* ((sender (if prompt-for-sender
                     (mail-pack-choose-main-account mail-pack-account-emails)
                   (mail-pack-current-account)))
         (other-headers (list (cons 'From sender))))
    (notmuch-mua-mail nil nil other-headers nil (notmuch-mua-get-switch-function))))

(defun mail-pack--setup-account (creds-file creds-file-content &optional entry-number)
  "Setup account, return tuple of account setup structure (account + optional alias)
CREDS-FILE represents the credentials file.
CREDS-FILE-CONTENT is the content of that same file.
ENTRY-NUMBER is the optional account number (multiple accounts setup possible)."
  (let* ((description-entry        (creds/get creds-file-content (mail-pack--label entry-number "email-description")))
         (full-name                (mail-pack--compute-fullname (creds/get-entry description-entry "firstname")
                                                                (creds/get-entry description-entry "surname")
                                                                (creds/get-entry description-entry "name")
                                                                (creds/get-entry description-entry "pseudo")))
         (x-url                    (creds/get-entry description-entry "x-url"))
         (mail-host                (creds/get-entry description-entry "mail-host"))
         (signature                (creds/get-entry description-entry "signature-file"))
         (smtp-server              (creds/get-entry description-entry "smtp-server"))
         (mail-address             (creds/get-entry description-entry "mail"))
         (draft-folder             (creds/get-entry description-entry "draft-folder"))
         (sent-folder              (creds/get-entry description-entry "sent-folder"))
         (trash-folder             (creds/get-entry description-entry "trash-folder"))
         (archive-folder           (creds/get-entry description-entry "archive-folder"))
         (attachment-folder        (creds/get-entry description-entry "attachment-folder"))
         (smtp-server-entry        (creds/get-with creds-file-content `(("machine" . ,smtp-server) ("login" . ,mail-address))))
         (email-alias              (creds/get-entry description-entry "email-alias"))
         (smtp-port                (creds/get-entry smtp-server-entry "port"))
         (folder-mail-address      (mail-pack--maildir-from-email mail-address))
         (folder-root-mail-address (format "%s/%s" mail-pack-mail-root-folder folder-mail-address))
         (refile-archive-fn        (mail-pack-refile-msg archive-folder))
         ;; setup the account
         (account-email-setup      `(,folder-mail-address
                                     ;; Global setup
                                     (user-mail-address      ,mail-address)
                                     (user-full-name         ,full-name)
                                     (message-signature-file ,signature)
                                     ;; GNUs setup
                                     (gnus-posting-styles ((".*"
                                                            (name ,full-name)
                                                            ("X-URL" ,x-url)
                                                            (mail-host-address ,mail-host))))
                                     (smtpmail-smtp-user ,mail-address)
                                     (smtpmail-starttls-credentials ((,smtp-server ,smtp-port nil nil)))
                                     (smtpmail-smtp-service         ,smtp-port)
                                     (smtpmail-default-smtp-server  ,smtp-server)
                                     (smtpmail-smtp-server          ,smtp-server)
                                     (smtpmail-auth-credentials     ,creds-file)))
         (account-email-alias      (when email-alias
                                     `(,folder-mail-address
                                       ;; Global setup
                                       (user-mail-address      ,email-alias)
                                       (user-full-name         ,full-name)
                                       (message-signature-file ,signature)
                                       ;; GNUs setup
                                       (gnus-posting-styles ((".*"
                                                              (name ,full-name)
                                                              ("X-URL" ,x-url)
                                                              (mail-host-address ,mail-host))))
                                       (smtpmail-smtp-user ,mail-address)
                                       (smtpmail-starttls-credentials ((,smtp-server ,smtp-port nil nil)))
                                       (smtpmail-smtp-service         ,smtp-port)
                                       (smtpmail-default-smtp-server  ,smtp-server)
                                       (smtpmail-smtp-server          ,smtp-server)
                                       (smtpmail-auth-credentials     ,creds-file)))))
    ;; In any case, return the account setup vars
    ;; create a subsidiary account aliases the first one
    (list account-email-setup account-email-alias)))

(add-hook 'message-mode-hook
          (lambda ()
            "Settings for message composition."
            (require 'flyspell)
            (set-fill-column 72)
            (turn-on-flyspell)))

(defun mail-pack-setup (creds-file creds-file-content)
  "Mail pack setup with the CREDS-FILE path and the CREDS-FILE-CONTENT."
  ;; common setup
  (mail-pack--common-configuration)

  ;; reinit the accounts list
  (setq mail-pack-accounts nil
        mail-pack-account-emails nil)

  ;; secondary accounts setup
  (-when-let (nb-accounts (mail-pack--nb-accounts creds-file-content))
    (when (< 1 nb-accounts)
      (->> (number-sequence 2 nb-accounts)
           (mapc (lambda (account-entry-number)
                   (let* ((setup-accounts (->> account-entry-number
                                               (format "%s")
                                               (mail-pack--setup-account creds-file creds-file-content)))
                          (account (car setup-accounts))
                          (alias-account (cadr setup-accounts)))
                     (add-to-list 'mail-pack-accounts account)
                     (when alias-account
                       (add-to-list 'mail-pack-accounts alias-accounts))))))))

  ;; main account setup
  (let* ((main-accounts (mail-pack--setup-account creds-file creds-file-content))
         (main-account (car main-accounts))
         (alias-account (cadr main-accounts)))
    (mail-pack--setup-as-main-account main-account)
    (add-to-list 'mail-pack-accounts main-account)
    (when alias-account
      (add-to-list 'mail-pack-accounts alias-account)))

  (setq mail-pack-account-emails (mail-pack--email-accounts mail-pack-accounts)))


;; ===================== Starting the mode

;;;###autoload
(defun mail-pack-load-pack ()
  "Mail pack loading routine.
This will check if the pre-requisite are met.
If ok, then checks if an account file exists the minimum required (1 account).
If ok then do the actual loading.
Otherwise, will log an error message with what's wrong to help the user fix it."
  (interactive)
  ;; run user defined hooks
  (run-hooks 'mail-pack-setup-hooks)
  ;; at last the checks and load pack routine
  (if (mail-pack-pre-requisites-ok-p)
      (-if-let (creds-file-content (mail-pack-setup-possible-p mail-pack-credentials-file))
          (progn
            (mail-pack-log "'%s' found! Running Setup..." mail-pack-credentials-file)
            (mail-pack-setup mail-pack-credentials-file creds-file-content)
            (mail-pack-log "Setup done!"))
        (-> (s-join
             "\n"
             '("You need to setup your credentials file '%s' for this to work. (The credentials file can be secured with gpg or not)."
               ""
               "A single account configuration file '%s' would look like this:"
               "machine email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>"
               "machine <smtp-server> login <your-email> port <smtp-port> password <your-mail-password-or-dedicated-token>"
               ""
               "A multiple account configuration file '%s' would look like this:"
               "machine email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>\n"
               "machine <smtp-server> login <login> port <smtp-port> password <your-mail-password-or-dedicated-token>"
               "machine 2-email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>\n"
               "machine <smtp-server> login <2nd-email> port <smtp-port> password <your-mail-password-or-dedicated-token>"
               "machine 3-email-description firstname <firstname> surname <surname> name <name> pseudo <pseudo> x-url <url> mail-host <mail-host> signature <signature> smtp-server <smtp-server>\n"
               "..."
               ""
               "Optional: Then `M-x encrypt-epa-file` to generate the required ~/.authinfo.gpg and remove ~/.authinfo."
               "Whatever you choose, reference the file you use in your emacs configuration:"
               "(setq mail-pack-credentials-file (expand-file-name \"~/.authinfo\"))"
               "As a pre-requisite, you need to install the notmuch packages."
               "For example, on debian-based system, `sudo aptitude install -y notmuch`..."))
            (mail-pack-log mail-pack-credentials-file mail-pack-credentials-file mail-pack-credentials-file)))))

(defvar mail-pack-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c e l") 'mail-pack-load-pack)
    (define-key map (kbd "C-c e m") 'notmuch)
    (define-key map (kbd "C-c e s") 'mail-pack-set-main-account)
    (define-key map (kbd "C-c e d") 'mail-pack-display-current-account)
    (define-key map (kbd "C-c e u") 'notmuch-poll)
    (define-key map (kbd "C-c e c") 'mail-pack-new-mail)
    (define-key map (kbd "C-c e o") 'notmuch-search-toggle-order)
    map)
  "Keymap for mail-pack mode.")

(define-minor-mode mail-pack-mode
  "Minor mode to consolidate mail-pack extensions.

\\{mail-pack-mode-map}"
  :lighter " MP"
  :keymap mail-pack-mode-map
  :global t)

(defun mail-pack-on ()
  "Turn on `mail-pack-mode'."
  (mail-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode global-mail-pack-mode mail-pack-mode mail-pack-on)

(provide 'mail-pack)
;;; mail-pack.el ends here
