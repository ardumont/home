;;; modeline-pack.el --- Modeline setup

;;; Commentary:

;;; Code:

(defun modeline-pack--log (&rest args)
  "Log the message ARGS in the mini-buffer."
  (apply #'message (format "Modeline Pack - %s" (car args)) (cdr args)))

(defun modeline-pack-clean-mode-line ()
  "Pretty symbol for the modeline."
  (interactive)
  (-map (lambda (mmode)
          (-let (((mode mode-str) mmode))
            (if (eq mode major-mode)
                (setq mode-name (s-trim mode-str))
              (when (assq mode minor-mode-alist)
                (require 'diminish)
                (diminish mode (s-trim-right mode-str))))))
        modeline-pack-clean-mode-alist))

(defun modeline-pack--abbrev-to-mode-name (abbrev-name)
  "Given an ABBREV-NAME, return the corresponding mode if it exists."
  (-filter (lambda (mmode)
             (-let (((mode s) mmode))
               (and (stringp s) (string-match-p abbrev-name s)))) minor-mode-alist))

(defun modeline-pack-abbrev-to-mode-name ()
  "Discover the full name of a modeline abbreviation."
  (interactive)
  (let ((abbrev (read-string "modeline abbreviation: ")))
    (->> abbrev
         modeline-pack--abbrev-to-mode-name
         (modeline-pack--log "%s -> %s" abbrev))))

;;; Greek letters - C-u C-\ greek ;; C-\ to revert to default
;;; ς ε ρ τ υ θ ι ο π α σ δ φ γ η ξ κ λ ζ χ ψ ω β ν μ

(provide 'modeline-pack)
;;; modeline-pack ends here
