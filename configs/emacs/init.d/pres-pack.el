;;; pres-pack.el --- presentation pack

;;; Commentary:

;;; Code:

(when (and (require 'org)
           (require 'org-reveal nil t)
           (require 'ox-beamer nil t))
  (add-to-list 'org-beamer-environments-extra
               '("picblock" "P"
                 "\\begin{picblock}%o{%h}"
                 "\\end{picblock}")))
;;;
(provide 'pres-pack)
;;; pres-pack.el ends here
