;;; prog-mode-pack.el --- Default hooks for programming mode

;; Copyright (C) 2018-2020  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar prog-mode-pack--prog-modep nil)

(defun prog-mode-pack-hook-fn ()
  "Specific function to install state. This state is used to
   determine if cleanup is needed or not."
  (make-local-variable 'prog-mode-pack--prog-modep)
  (setq prog-mode-pack--prog-modep t))

(defun prog-mode-pack-save-hook-fn ()
  "Clean up whitespaces when in prog-mode."
  (when prog-mode-pack--prog-modep
    (whitespace-cleanup)))

(use-package highlight-indent-guides
  :config
  (custom-set-variables '(highlight-indent-guides-method 'column)))

;; install prog-mode hooks
(dolist (hook-fn '(subword-mode
                   eldoc-mode
                   highlight-indent-guides-mode
                   prog-mode-pack-hook-fn))
  (add-hook 'prog-mode-hook hook-fn))

(add-hook 'before-save-hook 'prog-mode-pack-save-hook-fn)

(provide 'prog-mode-pack)
;;; buffer-pack.el ends here
