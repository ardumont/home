;;; python-pack.el --- A pack for python setup

;; Copyright (C) 2014-2022  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(cond ;; elpy
 ((eq python-path-setup 'elpy)
  ;; external
  (use-package elpy
    :config
    (elpy-enable)

    (use-package flycheck
      :config
      (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
      (add-hook 'elpy-mode-hook 'flycheck-mode))

    (define-key elpy-mode-map (kbd "M-.") 'elpy-goto-definition)))

 ;; or lsp
 ((eq python-path-setup 'lsp)
  ;; (add-hook 'python-mode (lambda ()
  ;;                          (require 'lsp-pyright)
  ;;                          (lsp-deferred)))
  (add-to-list 'language-mode-hook-pack 'python-mode-hook)
  ))

;; remaining part is the default configuration

;; define paredit-like bindings
(use-package smartparens
  :config
  (define-key smartparens-mode-map (kbd "C-M-h") 'sp-backward-kill-sexp)
  (define-key smartparens-mode-map (kbd "C-(") nil)
  (define-key smartparens-mode-map (kbd "M-(") nil)
  (define-key smartparens-mode-map (kbd "M-(") 'sp-backward-slurp-sexp)

  (define-key smartparens-mode-map (kbd "C-)") nil)
  (define-key smartparens-mode-map (kbd "M-)") nil)
  ;; sp-slurp-hybrid-sexp as a replacement?
  (define-key smartparens-mode-map (kbd "M-)") 'sp-forward-slurp-sexp)

  (define-key smartparens-mode-map (kbd "M-s") 'sp-splice-sexp)
  (define-key smartparens-mode-map (kbd "M-S") 'sp-split-sexp)
  (define-key smartparens-mode-map (kbd "C-M-f") 'sp-forward-sexp)
  (define-key smartparens-mode-map (kbd "C-M-b") 'sp-backward-sexp)
  (define-key smartparens-mode-map (kbd "C-M-d") 'sp-down-sexp)
  (define-key smartparens-mode-map (kbd "C-M-u") 'sp-up-sexp)
  (define-key smartparens-mode-map (kbd "C-M-a") 'sp-beginning-of-sexp)
  (define-key smartparens-mode-map (kbd "C-M-e") 'sp-end-of-sexp)
  (define-key smartparens-mode-map (kbd "C-M-t") 'sp-transpose-sexp)

  ;; keep standard kill-line sp-kill-hybrid-sexp is strange
  (define-key smartparens-strict-mode-map [remap kill-line] nil))

(use-package python
  :config
  (define-key python-mode-map (kbd "C-c C-d") nil)
  (define-key python-mode-map (kbd "C-c C-c") 'py-execute-statement-python3-no-switch)
  (define-key python-mode-map (kbd "C-c C-b") 'py-execute-buffer-python3-no-switch)
  (define-key python-mode-map (kbd "C-c C-l") 'py-execute-buffer-python3-no-switch)

  (custom-set-variables
   ;; python setup
   '(python-shell-interpreter "python")
   ;; '(python-shell-interpreter-args "...")
   '(python-indent-offset 4)
   '(python-indent-guess-indent-offset-verbose nil)
   '(python-shell-buffer-name "Python")
   '(python-check-command "pyflakes"))

  (add-hook 'python-mode-hook 'smartparens-strict-mode)
  (add-hook 'python-mode-hook
            (lambda ()
              (setq fill-column global-wrap-line-number)))

  (use-package blacken
    :config
    (custom-set-variables '(blacken-only-if-project-is-blackened t))
    (add-hook 'python-mode-hook 'blacken-mode))

  (defun python-pack-pdb ()
    (interactive)
    (utils-pack-insert-text-at-point "from pprint import pprint; breakpoint()\n"))

  (defun python-pack-pprint ()
    (interactive)
    (utils-pack-insert-text-at-point "from pprint import pprint\n"))

  (define-key python-mode-map (kbd "C-c M-d") 'python-pack-pdb)
  (define-key python-mode-map (kbd "C-c M-p") 'python-pack-pprint))

(provide 'python-pack)
;;; python-pack.el ends here
