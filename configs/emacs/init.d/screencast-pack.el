;;; screencast-pack.el --- Documentation tools pack          -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022  Antoine R. Dumont (@ardumont)

;; Author: Antoine R. Dumont (@ardumont) <tony@yavin4>
;; Keywords: docs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Documentation tools to ease creating mini-video demo

;;; Code:

(use-package gif-screencast
  :config
  (custom-set-variables
   '(gif-screencast-program (executable-find "scrot"))
   '(gif-screencast-convert-program (executable-find "convert"))
   '(gif-screencast-optimize-program (executable-find "gifsicle"))))

(defvar screencast-pack-mode-map nil
  "Keymap for screencast-pack mode.")
(setq screencast-pack-mode-map
      (let ((map (make-sparse-keymap)))
        ;; start screencast
        (define-key map (kbd "C-c v s") 'gif-screencast)
        ;; toggle on/off
        (define-key map (kbd "C-c v p") 'gif-screencast-toggle-pause)
        ;; halt video
        (define-key map (kbd "C-c v h") 'gif-screencast-stop)
        map))

(define-minor-mode screencast-pack-mode
  "Minor mode to consolidate Emacs' screencast-pack extensions.

\\{buffer-pack-mode-map}"
  :lighter ""
  :keymap screencast-pack-mode-map
  :t global)

(defun screencast-pack-on ()
  "Turn on `screencast-pack-mode'."
  (screencast-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode
  global-screencast-pack-mode screencast-pack-mode screencast-pack-on)

(provide 'screencast-pack)
;;; doc-pack.el ends here
