;;; theme-pack.el --- Theme setup

;;; Commentary:

;;; Code:

(use-package hl-line
  :config (global-hl-line-mode 1))

(defvar theme-pack-font "DejaVu Sans Mono" "Font used by theme-pack")
(defvar theme-pack-default-size 100 "Default font size to use")
(defvar theme-pack-medium-size 120 "Medium font size to use")
(defvar theme-pack-large-size 140 "Large font size to use")
(defvar theme-pack-outside-size 160 "Large font size to use")

(defvar theme-pack--current-size theme-pack-default-size "Current size installed.")
(defvar theme-pack--increment-size 20)

(if (string= system-name "dagobah")
    (use-package doom-modeline
      :ensure t
      :hook (after-init . doom-modeline-mode)
      :config (use-package all-the-icons
                :ensure t))
  (use-package powerline
    :config
    (setq powerline-arrow-shape 'arrow14 ;; arrow, curve or arrow14
          powerline-color1 "grey22"
          powerline-color2 "grey40"
          powerline-default-separator 'utf-8)))

(use-package menu-bar
  ;; Removes the menu-bar
  :config (menu-bar-mode -1))

(use-package tool-bar
  ;; Removes the toolbar in graphic editor
  :config (tool-bar-mode -1))

(use-package tooltip
  ;; Removes mouse hover tooltips
  :config (tooltip-mode -1))

(use-package scroll-bar
  ;; Removes the scollbar
  :config (scroll-bar-mode -1))

(use-package frame
  :config
  (blink-cursor-mode 1)
  (custom-set-variables
   '(blink-cursor-blinks 0) ;; <= 0 blinks forever, otherwise stops after `'10`'
))

(use-package whitespace
  :config
  (custom-set-variables
   '(whitespace-line-column fill-column) ;; limit line length
   '(whitespace-style '(face tabs empty trailing lines-tail))))

(set-language-environment "UTF-8")

(custom-set-variables
 ;; box, hollow, hbar, bar
 '(cursor-type 'bar))

(defun theme-pack-load-and-get-theme (required-mode theme-name)
  "Require the REQUIRED-MODE and return the THEME-NAEM symbol."
  (when (require required-mode nil t)
    theme-name))

(defun theme-pack-which-theme (dark-p)
  "Compute theme-pack name to load depending on DARK-P and THEME-PACK-THEME-NAME."
  (cond ((eq theme-pack-theme-name 'spacemacs)
         (if dark-p
             (theme-pack-load-and-get-theme 'spacemacs-dark-theme 'spacemacs-dark)
           (theme-pack-load-and-get-theme 'spacemacs-light-theme 'spacemacs-light)))
        ;; light:
        ;; 'doom-tomorrow-day
        ;; 'doom-acario-light
        ;; 'doom-ayu-light
        ;; dark:
        ;; 'doom-tomorrow-night
        ;; 'doom-acario-dark
        ;; 'doom-ayu-mirage
        ((eq theme-pack-theme-name 'doom)

         (if dark-p
             (theme-pack-load-and-get-theme 'doom-badger-theme 'doom-badger)
           (theme-pack-load-and-get-theme 'doom-ayu-light-theme 'doom-ayu-light)))))

;;;###autoload
(defun theme-pack-set-size (&optional font-size-input)
  "Depending on the hostname, will set the optional FONT-SIZE-INPUT.
ARGS With universal argument, can force the font-size to the input value."
  (interactive "P")

  (let ((font-size (if font-size-input font-size-input theme-pack--current-size)))
    (add-to-list 'default-frame-alist `(font . ,theme-pack-font))
    (set-face-attribute 'default nil :height font-size)
    (set-face-attribute 'mode-line nil :height (- font-size 20))
    (setq theme-pack--current-size font-size)))

(defvar theme-pack-default-make-frame-function after-make-frame-functions
  "Keep the original functions from after-make-frame-functions variables")

(defun theme-pack--install (fn log)
  "Execute the function FN (install theme function).
Display the LOG when done."
  (require 'cl)
  (require 'deferred)
  (lexical-let ((msg log)
                (theme-fn fn))
    (deferred:$
      (deferred:next 'theme-pack--disable-themes) ;; deactivate previous theme
      (deferred:nextc it theme-fn)                ;; apply theme
      (deferred:nextc it
        (lambda ()
          ;; remove any prior installed theme functions (frame creation routine)
          (setq after-make-frame-functions theme-pack-default-make-frame-function)
          ;; install new one to draw with the right theme each frame (x or no)
          (add-hook 'after-make-frame-functions
                    (lambda (&optional frame) (funcall theme-fn)))))
      (deferred:nextc it
        (lambda ()
          "Install some more custom faces depending on the chosen theme"
          ;; Those must be installed after the main theme is installed
          (cond ((eq theme-pack-theme-name 'spacemacs)
                 (custom-set-faces
                  '(mode-line
                    ((t (:foreground "#030303" :background "#bdbdbd" :box nil))))
                  '(mode-line-inactive
                    ((t (:foreground "#f9f9f9" :background "#666666" :box nil))))))
                ((eq theme-pack-theme-name 'doom)
                 (custom-set-faces
                  `(mode-line ((t (:background ,(doom-color 'dark-violet)))))
                  `(font-lock-comment-face ((t (:foreground ,(doom-color 'base6))))))
                 ))))
      (deferred:nextc it
        (lambda ()
          (message (format "theme-pack - %s" msg)))))))

(defun theme-pack--disable-themes ()
  "Disable current enabled themes."
  (mapc 'disable-theme custom-enabled-themes))

(defun theme-pack--load-theme (theme)
  "Disable currently enabled themes then load THEME."
  (load-theme theme 'no-confirm))

;;;###autoload
(defun theme-pack-light ()
  "For outside."
  (interactive)
  (theme-pack--install (lambda ()
                         (theme-pack-set-size theme-pack-outside-size)
                         (theme-pack--load-theme (theme-pack-which-theme nil)))
                       "Light theme installed!"))

;; (defun theme-pack-font-size (font-name)
;;   (-> font-name font-info (aref 2)))

;; (defun theme-pack-font-height (font-name)
;;   (-> font-name font-info (aref 3)))


;; (font-info theme-pack-font)
;; ["-PfEd-DejaVu Sans
;; Mono-normal-normal-normal-*-16-*-*-*-m-0-iso10646-1"
;; "DejaVu Sans
;; Mono:pixelsize=16:foundry=PfEd:weight=normal:slant=normal:width=normal
;; :spacing=100:scalable=true"
;; 16 19 0 0 0 10 15 4 10 10 ...]

(defun theme-pack-increase-font-size ()
  "Increase current font size"
  (interactive)
  (-> theme-pack--current-size
      (+ theme-pack--increment-size)
      theme-pack-set-size))

(defun theme-pack-decrease-font-size ()
  "Decrease current font size"
  (interactive)
  (-> theme-pack--current-size
      (- theme-pack--increment-size)
      theme-pack-set-size))

(defun theme-pack-reset-font-size ()
  "Reset font size to default"
  (interactive)
  (-> theme-pack-default-size
      (- theme-pack--increment-size)
      theme-pack-set-size))
;;;
(defun theme-pack-dark ()
  "Default theme for the inside."
  (interactive)
  (theme-pack--install (lambda ()
                         (theme-pack-set-size theme-pack-outside-size)
                         (theme-pack--load-theme (theme-pack-which-theme 'dark)))
                       "Dark theme installed!"))

;;;###autoload
(defun theme-pack-no-theme ()
  "Revert to no theme."
  (interactive)
  (theme-pack--install (lambda ()) "Reset theme done!"))

;; ######### define mode

(defvar theme-pack-mode-map nil "Keymap for theme-pack mode.")
(setq theme-pack-mode-map
      (let ((map (make-sparse-keymap)))
        (define-key map (kbd "C-c t d") 'theme-pack-dark)
        (define-key map (kbd "C-c t l") 'theme-pack-light)
        (define-key map (kbd "C-c t r") 'theme-pack-no-theme)
        (define-key map (kbd "C-c t s") 'theme-pack-set-size)
        (define-key map (kbd "C-c t +") 'theme-pack-increase-font-size)
        (define-key map (kbd "C-c t -") 'theme-pack-decrease-font-size)
        (define-key map (kbd "C-c t 0") 'theme-pack-reset-font-size)
        map))

(define-minor-mode theme-pack-mode
  "Minor mode to consolidate them-pack extensions.

\\{theme-pack-mode-map}"
  :lighter " TP"
  :keymap theme-pack-mode-map
  :global t)

(defun theme-pack-on ()
  "Turn on `theme-pack-mode'."
  (theme-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode global-theme-pack-mode theme-pack-mode theme-pack-on)

(provide 'theme-pack)
;;; theme-pack.el ends here
