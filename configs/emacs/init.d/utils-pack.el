;;; utils-pack.el ---                                     -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont (ardumont)

;; Author: Antoine R. Dumont (ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;;###autoload
(defun utils-pack-insert-text-at-point (text)
  "Insert TEXT in current buffer at point."
  (with-current-buffer (current-buffer)
    (insert text)))

(defun utils-pack-insert-header-at-point (text-type)
  "Print standard emacs header format so it knows what mode to trigger."
  (interactive "P")
  (utils-pack-insert-text-at-point (format "-*- mode: %s -*-" text-type)))

(defun utils-pack-load-directory (dir)
  "Load emacs-lisp files contained in DIR."
  (let ((expanded-dir (expand-file-name dir)))
    (when (file-exists-p expanded-dir)
      (let ((load-it (lambda (f)
                       (load-file (concat (file-name-as-directory dir) f)))))
        (mapc load-it (directory-files expanded-dir nil "\\.el$"))
        t))))

(defun utils-pack-reload-home-configuration ()
  "Load specific home-configured emacs."
  (interactive)
  (let ((home (getenv "HOME")))
    (or (utils-pack-load-directory (format "%s/repo/private/home/configs/emacs/" home))
        (utils-pack-load-directory (format "%s/repo/private/guix-home/emacs/" home)))))

;; ######### define mode

(defvar utils-pack-mode-map nil "Keymap for utils-pack mode.")
(setq utils-pack-mode-map
      (let ((map (make-sparse-keymap)))
        (define-key map (kbd "C-c u h") 'utils-pack-insert-header-at-point)
        (define-key map (kbd "C-c u r") 'utils-pack-reload-home-configuration)
        map))

(define-minor-mode utils-pack-mode
  "Minor mode to add utils commands in global context

\\{utils-pack-mode-map}"
  :lighter " UP"
  :keymap utils-pack-mode-map
  :global t)

(defun utils-pack-on ()
  "Turn on `utils-pack-mode'."
  (utils-pack-mode +1))

;;;###autoload
(define-globalized-minor-mode global-utils-pack-mode utils-pack-mode utils-pack-on)

(provide 'utils-pack)
;;; utils-pack.el ends here
