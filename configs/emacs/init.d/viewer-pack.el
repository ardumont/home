;;; viewer-pack.el --- Different viewers setup       -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(use-package doc-view
  :config
  (custom-set-variables '(doc-view-continuous t)))

(provide 'viewer-pack)
;;; viewer-pack ends here
