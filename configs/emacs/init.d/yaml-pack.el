;;; yaml-pack.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Antoine R. Dumont (ardumont)

;; Author: Antoine R. Dumont (ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package yaml
  :config
  (use-package highlight-indent-guides
    :config
    (add-hook 'yaml-mode-hook 'highlight-indent-guides-mode)))

(defun yaml-pack-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line"
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))

(provide 'yaml-pack)
;;; yaml-pack.el ends here
