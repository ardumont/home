;;; end.el --- dot-emacs end setup through nix

;; Copyright (C) 2019-2020  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(use-package modeline-pack
  :config
  ;; cleanup the modeline
  (defvar modeline-pack-clean-mode-alist
    `((auto-complete-mode         "")
      (org-indent-mode            "")
      (yas-minor-mode             "")
      (paredit-mode               "")
      (eldoc-mode                 "")
      (abbrev-mode                "")
      (guide-key-mode             "")
      (guru-mode                  "")
      (undo-tree-mode             "")
      (volatile-highlights-mode   "")
      (elisp-slime-nav-mode       "")
      (nrepl-mode                 "")
      (nrepl-interaction-mode     "")
      (cider-mode                 "")
      (cider-interaction          "")
      ;; Major modes
      (clojure-mode               "")
      (hi-lock-mode               "")
      (python-mode                "")
      (highlight-indentation-mode "")
      (elpy-mode                  "")
      (emacs-lisp-mode            "")
      (markdown-mode              "")
      (magit                      "")
      (haskell-mode               "")
      (tuareg-mode                "")
      (flymake-mode               "")
      (js2-mode                   "")
      (company-mode               "")
      (projectile-mode            "")
      (crux-mode                  "")
      (subword-mode               "")
      (whitespace-mode            "")
      (global-whitespace-mode     "")
      (git-gutter-mode            "")
      (which-key-mode             "")
      (smartparens-mode           "")
      (page-break-lines-mode      "")
      (nix-mode                   "")
      (aggressive-indent-mode     "")
      ;; personal pack
      (buffer-pack-mode           "")
      (popup-pack-mode            "")
      (twitter-pack-mode          "")
      (wiki-pack-mode             "")
      (mercurial-pack-mode        "")
      (shell-pack-mode            "")
      (git-pack-mode              "")
      (prelude-pack-mode          "")
      (theme-pack-mode            "")
      (dubcaps-mode               "")
      (kill-pack-mode             ""))
    "List of modes, clean mode string representation.

When you add a new element to the alist, keep in mind that you
must pass the correct minor/major mode symbol and a string you
want to use in the modeline instead of the original.")
  (modeline-pack-clean-mode-line))

;; printer
(setq printer-name "EPSON_ET_2820_Series")  ;; lpstat -p -d

(use-package theme-pack
  :config
  (global-theme-pack-mode)
  (theme-pack-set-size)
  (let ((theme-pack-fn (if (eq theme-pack-theme-nature 'dark)
                         'theme-pack-dark
                       'theme-pack-light)))
  (call-interactively theme-pack-fn)))

(use-package prelude-pack :config (global-prelude-pack-mode))
(use-package buffer-pack :config (global-buffer-pack-mode))
(use-package git-pack :config (global-git-pack-mode))
(use-package emacs-pack :config (global-emacs-pack-mode))
(use-package mercurial-pack :config (global-mercurial-pack-mode))
(use-package popup-pack :config (global-popup-pack-mode))
(use-package shell-pack :config (global-shell-pack-mode))
(use-package twitter-pack :config (global-twitter-pack-mode))
(use-package utils-pack :config (global-utils-pack-mode))
(use-package mail-pack :config (global-mail-pack-mode))
(use-package global-pack
  :config
  ;; Install lsp to for desired language mode hooks (defined in language-mode-hook-pack)
  (call-interactively 'global-pack-reload-supported-lsp-modes))

;; Load a bunch of extra ~/.emacs.d/init.d/*.el files
(utils-pack-load-directory "~/.emacs.d/extra-init.d")

(message "### .emacs init done")
