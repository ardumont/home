;;; init.el --- dot-emacs setup through nix/guix

;; Copyright (C) 2019-2022  Antoine R. Dumont (@ardumont)
;; Author: Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(message "### .emacs init loading")

(defvar default-load-path load-path
  "Default load-path at startup prior to adapt it.")

(defun utils-pack-reset-load-path ()
  (setq load-path default-load-path))

(defun add-to-load-path (dir)
  (let ((expanded-dir (expand-file-name dir)))
    (when (file-exists-p expanded-dir)
      (add-to-list 'load-path expanded-dir)
      expanded-dir)))

;; massage the load-path to make available the guix managed package to emacs
(when-let (site-lisp-dir (add-to-load-path "~/.guix-home/profile/share/emacs/site-lisp"))
  (mapc (lambda (dir) (add-to-load-path (format "%s/%s" site-lisp-dir dir)))
        (directory-files site-lisp-dir nil)))

;; HACK: for not yet guix/nix packaged emacs modules, fallback to require those from
;; potential local checkouts
(let ((module-configs '(("creds" "~/repo/public/emacs-creds")
                        ("org2jekyll" "~/repo/public/org2jekyll"))))
  (mapc
   (lambda (module-config)
     (let ((module (car module-config) )
           (module-path (cadr module-config) ))
       (unless (require (intern module) nil t)
         (when (add-to-load-path module-path)
           (require '(intern  module))))))
   module-configs))

(require 'use-package)

(defun utils-pack-load-directory (dir)
  "Load emacs-lisp files contained in DIR."
  (let ((expanded-dir (expand-file-name dir)))
    (when (file-exists-p expanded-dir)
      (let ((load-it (lambda (f)
                       (load-file (concat (file-name-as-directory dir) f)))))
        (mapc load-it (directory-files expanded-dir nil "\\.el$"))
        t))))

;; Load extra pack files
(utils-pack-load-directory "~/.emacs.d/init.d")

(use-package server
  :config
  (when (and (not (daemonp)) (not (server-running-p)))
    (server-start)))

;;; init.el ends here
