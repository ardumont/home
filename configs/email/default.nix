{ config, lib, pkgs, identity, ... }:

with lib;
let cfg = config.my.email;
    config-notmuch="$HOME/.config/notmuch/notmuchrc";
    me = identity.me;
    signature = ''${me.shortname} / ${me.fullname}

-----------------------------------------------------------------
gpg fingerprint ${me.gpg-id-long}
'';
  my-notmuch = with pkgs; writeShellScriptBin "my-notmuch" ''
#!{stdenv.shell}

set -x

# Use:
# - ... tag --remove-all '*'
# - ... new

${notmuch}/bin/notmuch --config=${config-notmuch} $@
      '';
    email-pre-sync-hook = with pkgs; writeShellScriptBin "email-pre-sync-hook" ''
#!{stdenv.shell}

set -x

NOTMUCH=${my-notmuch}/bin/my-notmuch
XARGS=${findutils}/bin/xargs

if [ `$NOTMUCH count tag:deleted` != 0 ]; then
    $NOTMUCH search --format=text0 \
         --output=files tag:deleted | \
         $XARGS -0 --no-run-if-empty rm
fi

if [ `$NOTMUCH count tag:spam` != 0 ]; then
    $NOTMUCH search --format=text0 \
        --output=files tag:spam | \
        $XARGS -0 --no-run-if-empty rm -f
fi
  '';
    my-afew = with pkgs; writeShellScriptBin "my-afew" ''
#!{stdenv.shell}

set -x

# Wrapper around afew so it uses the user configuration
# use:
# $@ --tag --all --verbose --dry-run
# $@ --tag --new --verbose
# ...

${afew}/bin/afew --notmuch-config ${config-notmuch} $@
'';

    email-post-sync-hook = with pkgs; writeShellScriptBin "email-post-sync-hook" ''
#!{stdenv.shell}

set -x

${my-notmuch}/bin/my-notmuch new
${my-afew}/bin/my-afew --tag --new
'';
in {
  options.my.email = {
    enable = mkEnableOption "Activate email install";
  };
  config = mkIf cfg.enable {
    programs = {
        offlineimap.enable = true;
        notmuch = {
          enable = true;
          new.tags = [ "new" ];
        };
        afew = {
          enable = true;
          extraConfig = readFile ./afew.filters;
      };
    };

    accounts.email =
      let real-name = me.fullname;
        gpg-config = {
          key = me.gpg-id;
          signByDefault = true;
          encryptByDefault = false;
        };
        signature-config = {
          text = signature;
          showSignature = "append";
        };
        offlineimap-config = {
          enable = true;
          extraConfig = {
            local = {
              # need this to sync labels
              # status_backend = "sqlite";
              sync_labels = true;
              sync_deletes = true;
              real_deletes = false;
            };
            remote = {
              maxconnections = 2;
              auth_mechanism = "LOGIN";
              folderfilter = "lambda d: d not in ['[Gmail]/Spam', '[Gmail]/All Mail', '[Gmail]/Drafts', '[Gmail]/Important']";
              ssl_version = "tls1_2";  # fix current offlineimap issue with ssl
            };
          };
        };
        notmuch-config = {
          enable = true;
        };
        metadata-from-email = email:
          let splitted-email = lib.splitString "@" email;
              path = lib.elemAt splitted-email 0;
              flavor-from = lib.elemAt splitted-email 1;
              flavor = if flavor-from == "gmail.com" then flavor-from else "plain";
              pass-path = if flavor == "gmail.com" then "gmail-token" else "work";
          in {
            inherit path flavor;
            pass-path = "ardumont/email/${pass-path}/${path}";
          };
      in rec {
        maildirBasePath = ".mails";
        accounts = {
          ard = let email = me.email.main;
                    meta = metadata-from-email email;
          in {
            offlineimap = offlineimap-config;
            notmuch = notmuch-config;
            primary = true;
            address = email;
            userName = email;
            maildir.path = meta.path;
            realName = real-name;
            aliases = [ me.email.work ];
            flavor = meta.flavor;
            passwordCommand = "${pkgs.pass}/bin/pass ${meta.pass-path}";
            signature = signature-config;
            gpg = gpg-config;
          };
          eniotna = let email = me.email.secondary;
                        meta = metadata-from-email email;
          in {
            offlineimap = offlineimap-config;
            notmuch = notmuch-config;
            address = email;
            userName = email;
            maildir.path = meta.path;
            realName = real-name;
            flavor = meta.flavor;
            passwordCommand = "${pkgs.pass}/bin/pass ${meta.pass-path}";
            signature = signature-config;
            gpg = gpg-config;
          };
          andumont = let email = me.email.work-secondary;
                         meta = metadata-from-email email;
          in {
            offlineimap = {
              enable = true;
              extraConfig = {
                local = {
                  sync_labels = true;
                  sync_deletes = true;
                  real_deletes = false;
                };
                remote = {
                  maxconnections = 2;
                  auth_mechanism = "LOGIN";
                  ssl_version = "tls1_2";  # fix current offlineimap issue with ssl
                };
              };
            };
            imap = {
              host = "zimbra.inria.fr";
              port = 993;
            };
            notmuch = notmuch-config;
            address = email;
            userName = email;
            maildir.path = meta.path;
            realName = real-name;
            flavor = meta.flavor;
            passwordCommand = "${pkgs.pass}/bin/pass ${meta.pass-path}";
            signature = signature-config;
            gpg = gpg-config;
          };
        };
      };

      home = {
        file.".signature".text = signature;
        # Install the email hooks within the hom environment so we can execute
        # those independently from the service
        packages = [ my-afew my-notmuch email-pre-sync-hook email-post-sync-hook ];
      };

      systemd.user.services.offlineimap = {
        Unit = {
          Description = "Offlineimap";
        };
        Service = {
          Type = "oneshot";
          ExecStartPre = "${email-pre-sync-hook}/bin/email-pre-sync-hook";
          ExecStart = "${pkgs.offlineimap}/bin/offlineimap";
          ExecStartPost = "${email-post-sync-hook}/bin/email-post-sync-hook";
          # apply the post sync hook even if some issues happened
          SuccessExitStatus = "0 1";
        };
      };

      systemd.user.timers.offlineimap = {
        Unit = {
          Description = "regular offlineimap mail";
        };
        Timer = {
          Unit = "offlineimap.service";
          AccuracySec = "10s";
          OnCalendar = "*:0/30";
        };
        Install = {
          WantedBy = [ "timers.target" ];
        };
      };
    };
  }
