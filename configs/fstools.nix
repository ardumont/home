{ lib, config, pkgs, ... }:

let cfg = config.my.fstools;
in {
  options.my.fstools = {
    enable = lib.mkEnableOption "FS tools configuration";
  };

  config = lib.mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        iotop
      ];
    };
  };
}
