{ lib, config, pkgs, identity, ... }:

let me = identity.me;
    cfg = config.my.git;
in {
  options.my.git = {
    enable = lib.mkEnableOption "Git configuration";
  };

  config = lib.mkIf cfg.enable {
    programs.git = {
      enable = true;
      userName = me.fullname;
      userEmail = me.email.main;
      signing = {
        key = me.gpg-id;
        signByDefault = true;
      };
      extraConfig = {
        init = {
          defaultBranch = "master";  # Keep the old git behavior about branch names.
        };
        core = {
          editor = "$EDITOR -nw";
          whitespace = "fix,-indent-with-non-tab,trailing-space,cr-at-eol";
          excludesfile = "~/.config/gitignore/global";
          autocrlf = "input";
        };
        web = {
          browser = "${config.my.browser.program}";
        };
        pull = {
          all = true;
          rebase = true;
          ff-only = true;
        };
        "url \"git@gitlab.softwareheritage.org:\"" = {
          pushInsteadOf = "https://gitlab.softwareheritage.org";
        };
        "url \"git@github.com:\"" = {
          pushInsteadOf = "https://github.com";
        };
        help = {
          autocorrect = 1;
        };
        credential = {
          helper = "cache --timeout 3600";
        };
        github = {
          user = me.nickname;
        };
        gitlab = {
          user = me.nickname;
        };
        gitlab."gitlab.softwareheritage.org/api/v4" = {
          user = me.nickname;
        };

        diff = {
          compactionHeuristic = true;
        };
        alias = {
          checkout-from-staging = "!f() { git fetch -nf https://forge.softwareheritage.org/source/staging.git phabricator/diff/$1:diff/$1 && git checkout diff/$1; }; f";
          rbranch = "! git for-each-ref --sort='-committerdate' --format='%20%20%(refname:short)' refs/heads/ | grep -v --max-count 20 -E '^  (delete|old)/' | tac";
        };
      };
    };

    home.file = {
      ".tigrc".text = ''
bind generic <C-P> scroll-line-up
bind generic <C-N> scroll-line-down
bind generic <Esc>v scroll-page-up
bind generic <C-V> scroll-page-down
'';

      ".gitattributes".text = ''
*.py diff=python
'';
      ".config/gitignore/global".text = ''
loaddefs.el*
backups/*
.DS_Store
auto-save-list
custom.el
*elc
thumbs/*
#.*#
\#*
.\#*
.envrc
.direnv/
'';
    };
  };
}
