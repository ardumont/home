{ lib, config, creds, pkgs, identity, ... }:

let cfg = config.my.gpg;
    my-key = identity.me.gpg-id;
    h = config.home.homeDirectory;
    gpg-backup-dir = "${h}/backup/gpg";
in {
  options.my.gpg = {
    enable = lib.mkEnableOption "GnuPG configuration";
  };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [ python310Packages.xkcdpass ];

    programs = {
      gpg = {
        enable = true;
        settings = {
          use-agent = true;
          no-comments = false;
          keyid-format = "long";
          default-key = my-key;
          charset = "utf-8";
        };
      };

      password-store = {
        enable = true;
        settings = {
          PASSWORD_STORE_DIR = "${h}/.password-store";
          PASSWORD_STORE_CLIP_TIME = "60";
        };
      };
    };

    # gpg-agent (gpg + ssh)
    services.gpg-agent = {
      enable = true;
      defaultCacheTtl = 1728000;
      defaultCacheTtlSsh = 1728000;
      maxCacheTtl = 1728000;
      maxCacheTtlSsh = 1728000;
      enableSshSupport = true;
    };

    home.file = {
      ".gnupg/sshcontrol".text = creds.sshcontrol;

      "bin/pass" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell}

# Force the editor to be a no x instance
# so that i can work remotely without x as well
export EDITOR="$EDITOR -nw"
${pkgs.pass}/bin/pass $@
'';
      };

      "bin/my-gpg-backup" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -e

mkdir -p ${gpg-backup-dir}
backup_file=${gpg-backup-dir}/gpg-backup.tar.gz

cp -v ~/.gnupg/pubring.gpg ${gpg-backup-dir}/
cp -v ~/.gnupg/secring.gpg ${gpg-backup-dir}/
cp -v ~/.gnupg/trustdb.gpg ${gpg-backup-dir}/
cp -rvf ~/.gnupg/private-keys-v1.d ${gpg-backup-dir}/

# or, instead of backing up trustdb...
# gpg --export-ownertrust > ${gpg-backup-dir}/tony-ownertrust-gpg.txt

cd ${gpg-backup-dir}
tar cvf $backup_file *
sha256sum $backup_file > $backup_file.sha256sum

'';
      };

      "bin/my-gpg-backup-restore" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -e

backup_file="${gpg-backup-dir}/gpg-backup.tar.gz"

[ ! -f $backup_file -a \
  ! -f $backup_file.sha256sum ] && \
  echo "no consistent backup found, stopping" && exit 0

sha256sum -c $backup_file.sha256sum
[ ! $? = 0 ] && echo "Mismatch hash, stopping" && exit 1

chmod 700 ~/.gnupg
tar xvf $backup_file -C ~/.gnupg/
'';
      };

      "bin/my-gpg-backup-deploy" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -xe

backup_file="${gpg-backup-dir}/gpg-backup.tar.gz"

DESTINATION=$1
[ -z "$DESTINATION" ] && DESTINATION=odroid

[ ! -f $backup_file -a \
  ! -f $backup_file.sha256sum ] && \
  echo "no consistent backup found, stopping" && exit 0

ssh $DESTINATION mkdir -p ${gpg-backup-dir}

RSYNC="${pkgs.rsync}/bin/rsync -av --stats --progress --delete"
$RSYNC $backup_file $backup_file.sha256sum \
    $DESTINATION:"${gpg-backup-dir}/"
'';
      };

      "bin/my-ssh-deploy" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell}

PASS=${pkgs.pass}/bin/pass

KEY=$1
  [ -z "$KEY" ] && KEY=corellia
SSH_HOME=${h}/.ssh/
KEYNAME=$SSH_HOME/id-rsa-$KEY

mkdir -p $SSH_HOME
$PASS ls ardumont/ssh-key/id-rsa-$KEY > $KEYNAME
$PASS ls ardumont/ssh-key/id-rsa-$KEY.pub > $KEYNAME.pub

'';
      };

      "bin/my-gpg" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -xe

case "$1" in
    backup)
        shift;
        cmd="$HOME/bin/my-gpg-backup"
        ;;
    deploy)
        shift;
        cmd="$HOME/bin/my-gpg-backup-deploy"
        ;;
    restore)
        shift;
        cmd="$HOME/bin/my-gpg-backup-restore"
          ;;
esac

$cmd $@
'';
      };

      "bin/my-ssh" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -xe

case "$1" in
    deploy)
        shift;
        cmd="$HOME/bin/my-ssh-deploy"
        ;;
esac

$cmd $@
'';
      };
    };
  };
}
