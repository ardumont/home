{ config, lib, pkgs, identity, ... }:

let cfg = config.my.guix;
    # Default packages for guix repl scripts
    script-packages-deps = "bash guix guile-sqlite3 shepherd";
in {
  options.my.guix = {
    enable = lib.mkOption {
      type = lib.types.bool;
      description = "Manage guix tools and configuration";
      default = false;
    };
  };

  config = lib.mkIf cfg.enable {
    home = {
      packages = [ pkgs.guile ];
      file = {
        ".config/guix/shell-authorized-directories".text = ''
/home/tony/repo/private/guix-home/xmonad
/home/tony/work/inria/repo/swh/swh-environment
/home/tony/work/scratch
'';
        ".shrc-guix-env".text = ''
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
export GUIX_EXTENSIONS_PATH=$HOME/repo/public/guix/guix/guix/extensions
'';
        ".config/guix/channels.scm".text = ''
(list (channel
       (name 'guix)
       (url "https://git.savannah.gnu.org/git/guix.git")
       ;; Mon Oct 30 11:03:22 AM CET 2023
       (commit "5d79012073c67a36201c4b10eee31b364c4d73dd")))
'';
        "bin/remote-guix-repl" = {
          executable = true;
          text = ''
#!/usr/bin/env -S guix shell psmisc ${script-packages-deps} -- bash

set -x

PORT=37146

fuser -n tcp $PORT || \
  guix repl --listen=tcp:$PORT --load-path=$HOME/repo/public/guix/guix

fuser -n tcp 37146 -k
'';
        };
        "bin/local-guix-repl" = {
          executable = true;
          text = ''
#!/usr/bin/env -S guix shell coreutils rlwrap ${script-packages-deps} -- bash

set -x

rlwrap guix repl --load-path=$HOME/repo/public/guix/guix
'';
        };
        "repo/public/guix/.envrc".text = ''
           cd guix && use guix
        '';
        ".config/direnv/lib/guix.sh" = {
          executable = true;
          text = ''
# ---- Guix ----
# From: https://guix.gnu.org/en/cookbook/en/guix-cookbook.html#Guix-environment-via-direnv

# Thanks <https://github.com/direnv/direnv/issues/73#issuecomment-152284914>
export_function()
{
  local name=$1
  local alias_dir=$PWD/.direnv/aliases
  mkdir -p "$alias_dir"
  PATH_add "$alias_dir"
  local target="$alias_dir/$name"
  if declare -f "$name" >/dev/null; then
    echo "#!$SHELL" > "$target"
    declare -f "$name" >> "$target" 2>/dev/null
    # Notice that we add shell variables to the function trigger.
    echo "$name \$*" >> "$target"
    chmod +x "$target"
  fi
}

use_guix()
{
    CONCURRENCY_BUILD=4
    # Fix perl warnings?
    # export LANGUAGE="en_US.utf8"
    # export LC_ALL="en_US.utf8"
    # export LC_ADDRESS="en_US.utf8"
    # export LC_MONETARY="fr_FR.utf8"
    # export LC_PAPER="fr_FR.utf8"
    # export LC_TIME="fr_FR.utf8"
    # export LANG="en_US.utf8"
    # export C="en_US.utf8"

    # Set GitHub token.
    # export GUIX_GITHUB_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    # Unset 'GUIX_PACKAGE_PATH'.
    export GUIX_PACKAGE_PATH=""

    # Recreate a garbage collector root.
    gcroots="$HOME/.config/guix/gcroots"
    mkdir -p "$gcroots"
    gcroot="$gcroots/guix"
    if [ -L "$gcroot" ]; then
        rm -v "$gcroot"
    fi

    # Default packages
    PACKAGES_DEFAULT="zsh autojump coreutils findutils less openssh rlwrap grep graphviz texinfo glibc-locales gawk watchexec"

    # Miscellaneous
    PACKAGES_MAINTENANCE="git git:send-email git-cal gnupg ncurses xdot autoconf make automake gettext tar xz sed pkg-config libtool gcc-toolchain sqlite po4a"

    # Environment
    PACKAGES_DEV="automake help2man guile guile-sqlite3 guile-colorized guile-readline guile-gcrypt guile2.2-gnutls guile-zlib guile-avahi guile-git guile-json diffutils patch"

    OPTIONAL_PACKAGES="guile-ssh guile-zstd guile-semver guile-lib libgcrypt"

    # Thanks <https://lists.gnu.org/archive/html/guix-devel/2016-09/msg00859.html>
    eval "$(guix shell -D --search-paths --root="$gcroot" --pure guix \
      $PACKAGES_DEFAULT $PACKAGES_DEV $PACKAGES_MAINTENANCE $OPTIONAL_PACKAGES \
      -- zsh)"

    # Predefine configure flags.
    bootstrap() {
        export LC_ALL=C
        # guile.m4, pkg.m4, ...
        export ACLOCAL_PATH=$HOME/.config/guix/gcroots/guix/share/aclocal/
        ./bootstrap
    }
    export_function bootstrap

    # Predefine configure flags.
    configure() {
        ./configure --localstatedir=/var --prefix=
    }
    export_function configure

    # Run make and optionally build something.
    build() {
        make -j $CONCURRENCY_BUILD
        if [ $# -gt 0 ]; then
            ./pre-inst-env guix build "$@"
        fi
    }
    export_function build

    # Predefine push Git command.
    push() {
        git push --set-upstream origin
    }
    export_function push

    watch () {
        # $1 should be the directory to watch for modifications
        watchexec -w $1 -- make -j$CONCURRENCY_BUILD
    }
    export_function watch

    # Show commands help.
    help () {
        echo "
help      This help message
bootstrap Create the build system
build     Build a package or just a project if no argument provided
check     Trigger the tests
configure Run ./configure with predefined parameters
guix      A pre-inst-env guix wrapper script
push      Push to upstream Git repository
watch     Watch command to ease building along modifications
"
    }
    export_function help

    guix () {
        ./pre-inst-env guix $@
    }
    export_function guix

    check () {
        make check
    }
    export_function check

    # Clean up the screen.
    clear
    git-cal --author='${identity.me.fullname}'

    help
}
            '';
        };
      };
    };
  };
}
