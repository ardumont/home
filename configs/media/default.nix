{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.my.media;
in {
  options.my.media = {
    enable = mkEnableOption "Media dependencies";
  };
  config = mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        # python3Packages.subliminal
        subdl
        yt-dlp
        easytag
      ];
    };
  };
}
