{ lib, config, pkgs, ...}:

let cfg = config.my.monitoring;
in {
  options.my.monitoring = {
    enable = lib.mkEnableOption "Monitoring configuration";
  };

  config = lib.mkIf cfg.enable {
    programs.htop.enable = true;

    home = {
      packages = with pkgs; [
        ncdu
        htop
        # load analysis
        dstat
        # net (analysis, ...)
        tcpdump
      ];
    };
  };
}
