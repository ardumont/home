{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs; [ iftop nethogs nmap ipcalc ];
}
