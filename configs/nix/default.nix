{ config, lib, pkgs, ... }:

let cfg = config.my.nix;
in {
  options.my.nix = {
    enable = lib.mkOption {
      type = lib.types.bool;
      description = "Manage nix tools and configuration";
      default = false;
    };
  };

  config = lib.mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        haskellPackages.niv
        cachix
        nix-prefetch-git
        nixpkgs-review
        nix-info
        nix-index
        nix-top
        nixVersions.stable
      ];
      file.".config/nix/nix.conf".source = ./nix.conf;
    };
  };
}
