{ config, lib, ... }:

let cfg = config.my.nixpkgs;
in {
  options.my.nixpkgs = {
    enable = lib.mkOption {
      type = lib.types.bool;
      description = "Manage nixpkgs tools override";
      default = true;
    };
  };

  config = lib.mkIf cfg.enable {
    home.file.".config/nixpkgs/config.nix".source = ./config.nix;
  };
}
