{ lib, config, pkgs, ... }:

let cfg = config.my.ssh;
in {
  options.my.qmk = {
    enable = lib.mkEnableOption "QMK configuration";
  };

  config = lib.mkIf cfg.enable {
    home = {
      packages = with pkgs; [ hid-listen ];
      file.".config/qmk/qmk.ini".source = ./qmk.ini;
    };
  };
}
