{ lib, config, pkgs, ... }:

# Script independent from graphical session

let cfg = config.my.scripts;
    h = config.home.homeDirectory;
    dir-work = "${h}/work";
    dir-repo = "${h}/repo";
    dir-docs = "${h}/docs";
    dir-books = "${h}/books";
    dir-public = "${dir-repo}/public";
    dir-private = "${dir-repo}/private";
    url-repo-public = "git@github.com:ardumont";
    url-repo-private = "git@ardumont.freeboxos.fr:ardumont";
    repo-home = "${url-repo-private}/home.git";
    dir-home = "${dir-private}/home";
    repo-mypkgs = "${url-repo-private}/mypkgs.git";
    dir-mypkgs = "${dir-private}/mypkgs";
    repo-home-manager = "${url-repo-public}/home-manager";
    dir-home-manager = "${dir-public}/home-manager";
    repo-nixpkgs = "${url-repo-public}/nixpkgs";
    dir-nixpkgs = "${dir-public}/nixpkgs";
    repo-org = "${url-repo-public}/org";
    dir-org = "${dir-public}/org";
    repo-private-org = "${url-repo-private}/private-org.git";
    dir-private-org = "${dir-private}/org";
    repo-dotxmonad = "${url-repo-public}/.xmonad.git";
    dir-dotxmonad = "${h}/.xmonad";
    repo-dotemacs = "${url-repo-private}/dot-emacs.git";
    dir-dotemacs = "${dir-public}/dot-emacs";
    repo-dotgnupg = "${url-repo-private}/dot-gnupg.git";
    dir-dotgnupg = "${h}/.gnupg";
    repo-dotssh = "${url-repo-private}/dot-ssh.git";
    dir-dotssh = "${h}/.ssh";
    repo-password-store = "${url-repo-private}/password-store.git";
    dir-password-store = "${h}/.password-store";
    dir-private-data = "${dir-private}/data";
    repo-dotfiles = "${url-repo-public}/dot-files";
    dir-dotfiles = "${dir-public}/dot-files";
    repo-pytools = "${url-repo-private}/ardumont-pytools";
    dir-pytools = "${dir-private}/ardumont-pytools";
    repo-nixos = "${url-repo-private}/nixos";
    dir-nixos = "${dir-private}/nixos";
    dir-guix = "${dir-private}/guix";
    dir-guix-home = "${dir-private}/guix-home";
    remote-dir-nixos = "/etc/nixos";
    remote-dir-private-data = "/etc/private-data";
    dir-home-chris = "/home/chris/repo/private/home-chris";
    dir-dot-nixpkgs = "${h}/.config/nixpkgs";
    channel-version = "20.03";
in
{
  options.my.scripts = {
    enable = lib.mkEnableOption "Scripts configuration";
  };

  config = lib.mkIf cfg.enable {
    home = {
      file = {
        ".mrconfig".text = ''
#### public
[${dir-dotfiles}]
checkout = git clone ${repo-dotfiles} ${dir-dotfiles}
update = git pull --rebase
[${dir-dotemacs}]
checkout = git clone ${repo-dotemacs} ${dir-dotemacs}
update = git pull --rebase
[${dir-home-manager}]
checkout = git clone ${repo-home-manager} ${dir-home-manager}
update = git pull --rebase
[${dir-nixpkgs}]
checkout = git clone ${repo-nixpkgs} ${dir-nixpkgs}
update = git pull --rebase
[${dir-dotxmonad}]
checkout = git clone ${repo-dotxmonad} ${dir-dotxmonad}
update = git pull --rebase
[${dir-org}]
checkout = git clone ${repo-org} ${dir-org}
update = git pull --rebase

#### private
[${dir-dotgnupg}]
checkout = git clone ${repo-dotgnupg} ${dir-dotgnupg}
update = git pull --rebase
[${dir-dotssh}]
checkout = git clone ${repo-dotssh} ${dir-dotssh}
update = git pull --rebase
[${dir-password-store}]
checkout = git clone ${repo-password-store} ${dir-password-store}
update = git pull --rebase
[${dir-home}]
checkout = git clone ${repo-home} ${dir-home}
update = git pull --rebase
[${dir-nixos}]
checkout = git clone ${repo-nixos} ${dir-nixos}
update = git pull --rebase
[${dir-mypkgs}]
checkout = git clone ${repo-mypkgs} ${dir-mypkgs}
update = git pull --rebase
[${dir-private-org}]
checkout = git clone ${repo-private-org} ${dir-private-org}
update = git pull --rebase
'';

        "bin/init-nix-channel" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

VERSION=$1
if [ -z "$VERSION" ]; then
  VERSION=${channel-version}
fi

nix-channel --add https://github.com/rycee/home-manager/archive/release-${channel-version}.tar.gz home-manager
nix-channel --add https://nixos.org/channels/nixos-${channel-version} nixpkgs

nix-channel --update home-manager
nix-channel --update nixpkgs
        '';
        };

        "bin/deploy-nix-to" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

# Deploy current development nix-related repos' state

DESTINATION=$1
if [ -z "$DESTINATION" ]; then
  DESTINATION=odroid
fi

OS=$2
if [ -z "$OS" ]; then
  OS=nixos
fi

RSYNC="${pkgs.rsync}/bin/rsync -av --stats --progress --exclude .tox --exclude .mypy_cache"

# Create those directories first (with the right default user and not root)
directories='${dir-nixos} ${dir-private-data} ${dir-pytools} ${dir-home} ${dir-password-store}'
ssh $DESTINATION mkdir -p $directories
for directory in $directories; do
  [ -d $directory ] && $RSYNC --delete "$directory"/ $DESTINATION:"$directory"/
done

if [ "$OS" = "nixos" ]; then
  [ -d $directory ] && $RSYNC --delete "${dir-nixos}"/ root@$DESTINATION:"${remote-dir-nixos}"/
  [ -d $directory ] && $RSYNC --delete "${dir-private-data}"/ root@$DESTINATION:"${remote-dir-private-data}"/
fi
        '';
        };

        "bin/deploy-guix-to" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

# Deploy current development guix-related repos' state

DESTINATION=$1
if [ -z "$DESTINATION" ]; then
  DESTINATION=talus
fi

OS=$2
if [ -z "$OS" ]; then
  OS=guix
fi

RSYNC="${pkgs.rsync}/bin/rsync -av --stats --progress --exclude .tox --exclude .mypy_cache"

# Create those directories first (with the right default user and not root)
directories='${dir-guix} ${dir-private-data} ${dir-pytools} ${dir-guix-home} ${dir-password-store}'
ssh $DESTINATION mkdir -p $directories
for directory in $directories; do
  [ -d $directory ] && $RSYNC --delete "$directory"/ $DESTINATION:"$directory"/
done

        '';
        };

        "bin/deploy-home-secrets" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

${pkgs.pass}/bin/pass ls ardumont/home/secrets.nix > ${dir-home}/secrets.nix
        '';
        };

        "bin/deploy-home-chris-to" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

# Deploy current home-manager home-chris repo

DESTINATION=$1
if [ -z "$DESTINATION" ]; then
  DESTINATION=chris@odroid
else
  DESTINATION=chris@$1
fi

RSYNC="${pkgs.rsync}/bin/rsync -av --stats --progress --exclude .tox"

# Create those directories first (with the right default user and not root)
ssh $DESTINATION mkdir -p ${dir-home-chris}
$RSYNC --delete "$HOME/repo/private/home-chris/" $DESTINATION:"${dir-home-chris}"/
        '';
        };

        "bin/my-hm-push" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cmd="${git}/bin/git push origin master"

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

for d in ${dir-home} ${dir-mypkgs}; do
  pushd $d
  $cmd
done
        '';
        };

        "bin/my-sync-repo" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $HOME
MR="${mr}/bin/mr -j4"
$MR update

pushd () {
    command pushd "$@" > /dev/null
}

# update subdirectories for dotemacs
pushd ${dir-dotemacs} && $MR update
    '';
        };

        "bin/swh-sync-repo" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -x

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

list="$SWH_ENVIRONMENT_HOME $SWH_PUPPET_ENVIRONMENT_HOME $SWH_CI_ENVIRONMENT_HOME $SWH_SYSADM_ENVIRONMENT $SWH_PASSWORD_STORE_DIR $SWH_ENVIRONMENT_HOME/../org"
for d in $list; do
  pushd $d
  ${git}/bin/git pull origin master --rebase
  # update subdirectories for dotemacs
  ${mr}/bin/mr -j4 update
done
        '';
        };

        "bin/my-hm-pull" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

for d in ${dir-home} ${dir-mypkgs}; do
  pushd $d
  ${git}/bin/git pull origin master --rebase
done
        '';
        };

        "bin/my-hm-doc" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}
echo "# First install"
echo "nix-shell -p home-manager"
echo "cd home"
echo "# build"
echo "nix build .#<host>"
echo "# install"
echo "nix run .#<host>"
echo my-hm switch
        '';
        };

        "bin/nix-gc" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -e

nix-collect-garbage -d $@
nix-store --optimize

'';
        };

        "bin/my-hm" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell}

# Wrapper around home-manager

# This will allow to execute:

# - push: push code to repository
# - pull: pull --rebase code from repository
# - deploy: run rsync commands to the node that needs a nix update
# - deploy-guix: run rsync commands to the node that needs a guix update
# - build: build the current state of the 'home' repository
# - switch: switch after building the current state of the 'home' repository

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

pushd ${dir-home}

GIT=${git}/bin/git

h=$(${hostname}/bin/hostname)

case "$h" in
  odroid|odroid.lan|odroid.vlan|myrkr|myrkr.lan|myrkr.vlan)
    os=nixos
    ;;
  *)
    os=""
  ;;
esac


case "$1" in
    doc)
      cmd="$HOME/bin/my-hm-doc"
      ;;
    pull)
      cmd="$HOME/bin/my-hm-pull"
      ;;
    push)
      cmd="$HOME/bin/my-hm-push"
      ;;
    sync)
      cmd="$HOME/bin/my-sync-repo"
      ;;
    swh-sync)
      cmd="$HOME/bin/swh-sync-repo"
      ;;
    deploy)
      pushd
      cmd="$HOME/bin/deploy-nix-to $2 $os"
      ;;
    deploy-guix)
      pushd
      cmd="$HOME/bin/deploy-guix-to $2 $os"
      ;;
    deploy-home-chris)
      pushd
      cmd="$HOME/bin/deploy-home-chris-to $2"
      ;;
    push)
      cmd="$GIT push origin master"
      ;;
    gc)
      cmd="$HOME/bin/nix-gc"
      ;;
    deploy-secrets)
      cmd="$HOME/bin/deploy-home-secrets"
      ;;
    build)
      nix build .#$h --impure -- $@
      ;;
    switch)
      nix run .#$h --impure -- $@
      ;;
esac

$cmd

SYSTEMCTL=${systemd}/bin/systemctl
$SYSTEMCTL --user daemon-reload
# $SYSTEMCTL --user import-environment

popd
         '';
        };

        "bin/f" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell}

[ $1 = "-h" ] && \
    echo "Use: $0 (<FOLDER-FROM>) <REGEXP>...
FOLDER-FROM  The folder from where to start. If not specified, run the search from the current folder.
REGEXP       The expression to look for

Example - Search for a folder or file named \"hello you\" in ~admin/bin:
f ~admin/bin hello you" && \
    exit 1

# batch processing options (-> | xargs -0 options)
[ $1 = '-print0' ] && OPTIONS="-print0" && shift || OPTIONS=""
# Search from the folder or the current folder if not specified
[ -d $1 ] && FOLDER=$1 && shift || FOLDER=.

REGEXP="*"$(echo "$*" | sed 's/[ ]/*/g')"*"

${findutils}/bin/find $FOLDER -iname "$REGEXP" $OPTIONS
        '';
        };

        "bin/rsync-media.sh" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

# --archive, -a            archive mode is -rlptgoD (no -A,-X,-U,-N,-H)
# so it's equivalent to:
# --recursive, -r          recurse into directories
# --links, -l              copy symlinks as symlinks
# --perms, -p              preserve permissions
# --times, -t              preserve modification times
# --group, -g              preserve group
# --owner, -o              preserve owner (super-user only)
# -D                       same as --devices --specials
# --devices                preserve device files (super-user only)
# --specials               preserve special files
# for media files, rlpgo sounds like enough options

${pkgs.rsync}/bin/rsync -rlpgo $@
'';
        };

        "bin/rsync-basic.sh" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe
RSYNC="${pkgs.rsync}/bin/rsync -av --stats --progress --delete --exclude=.*"

SRC=$1
DEST=$2

$RSYNC $SRC $DEST
        '';
        };

        "bin/backup-home" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

DEST=$1

for d in ${dir-work} ${dir-repo} ${dir-docs} ${dir-books}; do
  ~/bin/rsync-basic.sh $d/ "$DEST""$d"/
done
        '';
        };

        "bin/prepare-remote-home-config" = with pkgs; {
          executable = true;
          text = ''
#!${stdenv.shell} -xe

SRC=$HOME/repo/public/dot-files
DEST=$HOME/repo/private/remote-dot-files

rm -rf $DEST
mkdir -p $DEST
cp -v $SRC/.shrc-env $DEST/.shrc-env
cp -v $SRC/.shrc-path $DEST/.shrc-path
cp -v $SRC/.shrc-prompt $DEST/.shrc-prompt
cp -v $SRC/.tmux-remote.conf $DEST/.tmux.conf
cp -v $SRC/.zshrc $DEST/.zshrc

RSYNC="${rsync}/bin/rsync -av --stats --progress --delete"
$RSYNC --exclude '.git' $HOME/repo/public/oh-my-zsh/ \
    $HOME/repo/private/oh-my-zsh/

'';
        };

        "bin/parse-id-from-url" =
          let my-python3 = (pkgs.python3.withPackages (ps: [pkgs.python310Packages.click]));
          in {
            executable = true;
            text = ''
#!${my-python3.interpreter}

import click
import contextlib
import logging
import os

from typing import Optional
from urllib.parse import urlparse

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def parsing_exceptions(url):
    """Catches exception from parsing."""
    try:
        yield
    except Exception as e:
        logger.error(f'Problem during url parsing {url}. Reason: {e}')


def parse_id_last_element(url) -> Optional[str]:
    """Parse url to retrieve the object identifier (with fragment if any)

    """
    with parsing_exceptions(url):
        interesting_uri = url.path.rstrip('/').split('/')
        suffix = f"#{url.fragment}" if url.fragment else ""
        return f"{interesting_uri[-1]}{suffix}"


@click.command()
@click.argument("url")
def main(url):
    """Try and and parse identifier from url (imdb, forge.s.o, github.com, ...)

    This may fail if the url is not supported or does not follow the same pattern (id as
    last element of the url).

    """
    uri = urlparse(url)

    id_ = parse_id_last_element(uri)
    if not id_:
        logger.error(f'Unsupported url {url}')
        return None

    print(id_)

if __name__ == '__main__':
   main()
'';
          };

        "bin/isodate" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -e

date --iso-8601=second --universal $@
'';
        };

        "bin/home-activate" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

ln -nsf ${dir-home} ${dir-dot-nixpkgs}
            '';
        };

        "bin/home-deactivate" = {
          executable = true;
          text = ''
            #!${pkgs.stdenv.shell} -xe

            [ -L "${dir-dot-nixpkgs}" ] && rm "${dir-dot-nixpkgs}"

              '';
        };

        "bin/ssh-keygen" = {
          executable = true;
          text = with pkgs; ''
              #!${stdenv.shell} -xe

              ${openssh.out}/bin/ssh-keygen -f ~/.ssh/known_hosts -R $1
'';
        };
      };
    };
  };
}
