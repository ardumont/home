{ lib, config, pkgs, ...}:

let cfg = config.my.shell;
in {
  options.my.shell = {
    enable = lib.mkEnableOption "Shell configuration";
  };

  config = lib.mkIf cfg.enable {
    home = {
      file = {
        ".shrc".source = ./shrc;
        # FIXME: should probably go away and be merged here somewhere
        ".shrc-nix-env".source = ./shrc-nix-env;
        # for legacy shells
        ".bashrc".source = ./shrc;
      };
      packages = with pkgs; [ peco lshw autojump fzf rlwrap ];
    };

    programs.zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestion.enable = true;
      defaultKeymap = "emacs";
      history.size = 1000000;
      oh-my-zsh = {
        plugins = [
          "git" "git-extras" "colored-man" "rsync" "stack"
          "autojump" "sudo" "kubectl" "docker-compose"
        ];
      };
      shellAliases = {
        ls = "ls --color=always";
        ll = "ls -lAh";
        ".." = "cd ..";
        grep = "grep --color=auto";
        wget = "wget -c";
        em = "~/bin/emacs -nw";
        sem = "SUDO_EDITOR=\"$EDITOR -nw\" sudoedit";
        emc = "$EDITOR";
        gst = "git status -sb";
        gci = "git commit -v";
        gco = "git checkout";
        gcob = "git checkout -b";
        gbr = "git branch";
        gdf = "git diff --word-diff";
        gf = "git fetch -p";
        gfa = "git fetch --all -p";
        xup = "xrdb -merge ~/.Xresources";
        hm = "home-manager";
        doco = "docker-compose";
        ip = "ip -c=auto";
        today = "date +%Y%m%d-%H%M";
        sbcl = "rlwrap sbcl";
      };
      initExtra = with pkgs; ''
[ -f ~/.shrc ] && . ~/.shrc

# Load extra kube related configuration
[ -e ~/.kube/zsh ] && . ~/.kube/zsh

# Load extra completion if any
[ -e ~/.config/glab-cli/completion-zsh ] && . ~/.config/glab-cli/completion-zsh

# Write to history file immediately (not when the shell exits)
setopt INC_APPEND_HISTORY
# To prevent history from recording duplicated entries (such as ls -l entered many times
# during single shell session), you can set the hist_ignore_all_dups option:
setopt hist_ignore_all_dups
# Expire duplicate entries first when trimming history.
setopt HIST_EXPIRE_DUPS_FIRST

# default: *?_-.[]~=/&;!#$%^(){}<>
# i'm not using the same word definition as this default
# so clearing that variable
export WORDCHARS=""

# emacs (eshell) and tramp - https://www.emacswiki.org/emacs/TrampMode#toc9
# or shell within emacs, keep prompt to a minimum
if [ $TERM = "dumb" -o ! -z "$INSIDE_EMACS" ]; then
  # basic prompt when within emacs
  unsetopt prompt_cr
  unsetopt zle
  # PS1='\u@\h:\w\$ '
  # PS1='$ '
elif [ "$TERM" != "linux" ]; then

  . ${pkgs.autojump}/etc/profile.d/autojump.sh

  # from https://github.com/justjanne/powerline-go#zsh
  function powerline_precmd() {
    PS1="$(${powerline-go}/bin/powerline-go -error $? -shell zsh)"
  }

  function install_powerline_precmd() {
    for s in "$precmd_functions[@]"; do
      if [ "$s" = "powerline_precmd" ]; then
        return
      fi
    done
    precmd_functions+=(powerline_precmd)
  }

  install_powerline_precmd
fi

# workaround
unset __HM_ZSH_SESS_VARS_SOURCED; unset __HM_SESS_VARS_SOURCED
source ~/.zshenv
'';

    };
  };
}
