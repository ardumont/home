{ lib, pkgs, config, creds, ... }:

let cfg = config.my.clustershell;
    pkg-clustershell = pkgs.python310Packages.clustershell;
in {
  options.my.clustershell = {
    enable = lib.mkEnableOption "Debian configuration";
  };

  config = lib.mkIf cfg.enable {
    home = {
      packages = [ pkg-clustershell ];

      file = let h-clustershell = "${config.xdg.configHome}/clustershell";
                 groups = ''
machines: devbox server chris
server: odroid
devbox: bespin myrkr yavin4
chris: alderaan dathomir
cassandra: cassandra01 cassandra02 cassandra03 cassandra04
'';
       in {
        "${h-clustershell}/clush.conf".source = ./clush.conf;
        "${h-clustershell}/groups.conf".source = ./groups.conf;
        "${h-clustershell}/groups".text = groups + creds.swh.clush-groups;

        "bin/remove-remote-home-config" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

machines=$1
[ -z $machines ] && echo "Usage: $0 <clush-group-name>" && exit 1

CMD="${pkg-clustershell}/bin/clush -b -w $machines"
$CMD rm -rf repo/public/dot-files repo/public/oh-my-zsh;
        '';
        };

        "bin/deploy-remote-home-config" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

machines=$1
[ -z $machines ] && echo "Usage: $0 <clush-group-name>" && exit 1

heavy_skip_copy=$2  # anything will make this script skip the copy

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

pushd $HOME

CMD="${pkg-clustershell}/bin/clush -b -w $machines"

# Light copies here
$CMD mkdir -p repo/public
for d in dot-files; do
    $CMD --copy $HOME/repo/private/$d --dest repo/public
done

# heavy copies here
if [ -z "$heavy_skip_copy" ]; then
    for d in oh-my-zsh; do
        $CMD --copy $HOME/repo/private/$d --dest repo/public/$d;
        $CMD "([ -d repo/public/$d/$d ] && rm -rf repo/public/$d/$d && echo '$d/$d removed') || echo 'nothing more to do, skip'"
    done
fi

$CMD 'ln -nsf $HOME/repo/public/dot-files/.shrc-env .shrc-env'
$CMD 'ln -nsf $HOME/repo/public/dot-files/.shrc-aliases .shrc-aliases'
$CMD 'ln -nsf $HOME/repo/public/dot-files/.shrc-path .shrc-path'
$CMD 'ln -nsf $HOME/repo/public/dot-files/.shrc-prompt .shrc-prompt'
$CMD 'ln -nsf $HOME/repo/public/dot-files/.tmux-remote.conf $HOME/.tmux.conf'
$CMD 'ln -nsf $HOME/repo/public/oh-my-zsh $HOME/.oh-my-zsh'
$CMD 'ln -nsf $HOME/repo/public/dot-files/.zshrc $HOME/.zshrc'

popd
'';
        };
       };
    };
  };
}
