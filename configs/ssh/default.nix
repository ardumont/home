{ lib, config, pkgs, ... }:

let cfg = config.my.ssh;
in {
  options.my.ssh = {
    enable = lib.mkEnableOption "SSH configuration";
  };

  config = lib.mkIf cfg.enable {
    home.file.".ssh/authorized_keys".source = ./authorized_keys;

    programs.ssh =
      let h = config.home.homeDirectory;
          user = {
            default = "tony";
            swh = "ardumont";
            inria = "andumont";
            pi = "pi";
          };
          identity-file = {
            default = "${h}/.ssh/id-rsa-corellia";
            secondary = "${h}/.ssh/id-ed25519-yavin4";
          };
          local = rec {
            lan = "lan";
            vlan = "vlan";
            nodes-lan-pattern = "*.${lan}";
            nodes-vlan-pattern = "*.${vlan}";
            nodes = "alderaan dathomir myrkr yavin4 odroid-n2 odroid rpi4 rpi3 dagobah";
            freebox = "ardumont.freeboxos.fr";
            remote-nodes = "rpi3 rpi4 odroid-n2 odroid ${freebox}";
          };
          swh = rec {
            bastion = "sesi-ssh.inria.fr";
            lan = "softwareheritage.org";
            forge = "forge.${lan}";
            inria = "inria.fr";
            internal-lan = "internal.${lan}";
            staging-lan = "internal.staging.swh.network";
            admin-lan = "internal.admin.swh.network";
            azure-lan = "euwest.azure.${internal-lan}";
            proxy-node = "uffizi";
            proxy-fqdn = "${proxy-node}.${internal-lan}";
            desktop = "ardumont ardumont.${internal-lan} bespin ardumont-desktop";
            azure-nodes-pattern = "*.${azure-lan}";
            production-nodes = "louvre prado somerset moma pergamon getty saatchi thyssen belvedere esnode1 esnode2 esnode3 saam branly pushkin glyptotek kelvingrove kafka1 kafka2 kafka3 kafka4 riverside tate";
            production-nodes-pattern = "*.${lan} swh*.${inria} *.paris.${inria} *.${inria} !*.${internal-lan} !*.${azure-lan} !ardumont.${internal-lan} !ardumont-desktop.${internal-lan} !${forge}";
            staging-nodes = "db1 storage1 journal0 scheduler worker0 worker1 worker2 deposit webapp clearly-defined";
            staging-nodes-pattern = "*.${staging-lan} ${staging-nodes}";
            admin-nodes = "bardo pushkin glyptotek";
            admin-nodes-pattern =  "*.${admin-lan} ${admin-nodes}";
          };
      in {
        enable = true;
        matchBlocks = {
          "cassandra01" = {
            user = "root";
          };
          "cassandra02" = {
            user = "root";
          };
          "cassandra03" = {
            user = "root";
          };
          "cassandra04" = {
            user = "root";
          };
          "${swh.desktop}" = {
            user = user.default;
            hostname = "ardumont-desktop.${swh.internal-lan}";
            identityFile = identity-file.secondary;
          };
          "${local.nodes-lan-pattern}" = {
            user = user.default;
            port = 22;
          };
          "sesi-ssh ${swh.bastion}" = {
            hostname = swh.bastion;
            user = user.inria;
            identityFile = identity-file.default;
            identitiesOnly = true;
          };
          "${swh.production-nodes-pattern}" = {
            user = user.swh;
            # proxyCommand = "ssh -W %h:%p sesi-ssh";
            identitiesOnly = true;
          };
          "${swh.production-nodes}" = {
            user = user.swh;
            # proxyCommand = "ssh -W %h:%p ${swh.proxy-fqdn}";
          };
          "tate tate.internal.softwareheritage.org" = {
            port = 2222;
          };
          "forge ${swh.forge}" = {
            hostname = swh.forge;
            user = user.swh;
            identitiesOnly = true;
            identityFile = identity-file.secondary;
          };
          "${swh.staging-nodes-pattern} ${swh.admin-nodes-pattern}" = {
            user = user.swh;
            identitiesOnly = true;
          };
          "*.${swh.internal-lan} *.${swh.azure-lan}" = {
            user = user.swh;
            identityFile = identity-file.secondary;
            identitiesOnly = true;
          };
          "${swh.proxy-node} ${swh.proxy-fqdn}" = {
            hostname = "${swh.proxy-fqdn}";
            user = user.swh;
            identityFile = identity-file.secondary;
            identitiesOnly = true;
          };
          "kids" = {
            port = 6457;
          };
          "rpi3" = {
            port = 6458;
          };
          "rpi4" = {
            port = 6459;
          };
          "odroid-n2 ${local.freebox}" = {
            port = 6461;
          };
          "odroid ${local.freebox}" = {
            port = 6460;
          };
          "${local.remote-nodes}" = {
            hostname = "${local.freebox}";
            user = user.default;
            identityFile = identity-file.default;
            identitiesOnly = true;
          };
          "mrdumont coruscant" = {
            hostname = "mrdumont.freeboxos.fr";
            addressFamily = "inet";
            identityFile = identity-file.default;
            user = user.default;
            port = 49152;
          };
          "endor" = {
            hostname = "mrdumont.freeboxos.fr";
            identityFile = identity-file.default;
            addressFamily = "inet";
            user = user.default;
            port = 49153;
          };
          "coruscant.lan" = {
            hostname = "192.168.1.69";
            identityFile = identity-file.default;
            user = user.default;
          };
          "endor.lan" = {
            hostname = "192.168.1.170";
            identityFile = identity-file.default;
            user = user.default;
          };
          "talus.lan" = {
            hostname = "192.168.150.45";
            identityFile = identity-file.default;
            user = user.default;
          };
          nodes-vlan-pattern = {
            user = user.default;
            proxyCommand = "ssh -W %h:%p odroid";
            identitiesOnly = true;
          };
          "chris-laptop" = {
            hostname = "192.168.150.18";
            port = 22;
            user = user.default;
            identityFile = identity-file.default;
          };
          "pinephone" = {
            hostname = "192.168.150.50";
            port = 22;
            user = "root";
            identityFile = identity-file.default;
          };
        };
      };
  };
}
