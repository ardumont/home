{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.my.tmux;
in {
  options.my.tmux = {
    enable = mkEnableOption "Open specific variable tmux configuration";

    prefix-key = mkOption {
      type = types.str;
      default = "o";
      description = "Default prefix key for tmux";
    };
    alternative-prefix-key = mkOption {
      type = types.str;
      default = "q";
      description = "Alternative usually the one used on remote";
    };
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      extract_url
    ];
    programs.tmux =
      let defaultShortcut = cfg.prefix-key;
          alternativeShortcut = cfg.alternative-prefix-key;
      in {
        enable = true;

        baseIndex = 1;
        aggressiveResize = true;
        clock24 = true;
        escapeTime = 0;
        historyLimit = 100000;
        keyMode = "emacs";
        newSession = false;
        shortcut = defaultShortcut;
        terminal = "${config.home.sessionVariables.TERM}";

        plugins = with pkgs; [
          {
            plugin = tmuxPlugins.urlview;
            extraConfig = ''
              set -g @urlview-key 'u'
            '';
          }
          {
            plugin = tmuxPlugins.prefix-highlight;
            extraConfig = ''
              set -g @prefix_highlight_show_copy_mode 'on'
             '';
          }
          {
            plugin = tmuxPlugins.resurrect;
          }
          {
            plugin = tmuxPlugins.sidebar;
            extraConfig = ''
              set -g @sidebar-tree 's'
              set -g @sidebar-tree-width '60'
              set -g @sidebar-tree-command 'tree -C'
             '';
          }
          {
            plugin = tmuxPlugins.fingers;
            extraConfig = ''
              set -g @fingers-key f
             '';
          }
        ];

        extraConfig = ''
# depends on online-status and prefix-highlight plugin
set -g status-right '#{prefix_highlight} | %a %Y-%m-%d %H:%M'
# <prefix>-j allows to choose to switch between pane
bind j display-panes
# unbind the previous default ^ binding
unbind q
# Switch to the next pane
bind s select-pane -t :.+
# Doubling the prefix key activate the copy-mode
bind ${defaultShortcut} copy-mode
# Use `<prefix> y` instead of `<prefix> ]`
bind y paste
set-option -g update-environment "DISPLAY GPG_AGENT_INFO SSH_ASKPASS SSH_AUTH_SOCK SSH_CONNECTION WINDOWID XAUTHORITY"
set-option -g allow-rename off
set -g set-titles off
bind r source-file ~/.config/tmux/tmux.conf \; display-message "tmux setup reloaded..."
# split window horizontally (like %)
bind | split-window -h
# split window vertically (like ")
bind - split-window -v

bind -Tcopy-mode M-w send-keys -X copy-pipe-and-cancel "${pkgs.xsel}/bin/xsel --input --clipboard"
# Decrease
bind = resize-pane -D 4
# Increase
bind + resize-pane -U 4
# go to the last window <prefix> C-a
bind-key C-a last-window
# Toggle between C-${alternativeShortcut} and C-${defaultShortcut}
bind t set -g prefix C-${alternativeShortcut} \; unbind ${alternativeShortcut} \; bind ${alternativeShortcut} copy-mode\; display-message "prefix key: C-${alternativeShortcut}"
bind T set -g prefix C-${defaultShortcut} \; unbind ${defaultShortcut} \; bind ${defaultShortcut} copy-mode\; display-message "prefix key: C-${defaultShortcut}"

        '';
      };
    };
}
