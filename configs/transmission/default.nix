{ pkgs, config, lib, creds, ...}:

let xdg = config.xdg;
    dag = config.lib.dag;
    cli-folder = ".config/transmission-remote";
    cli-netrc = "${cli-folder}/netrc";
    cli-full-netrc = "${config.home.homeDirectory}/${cli-netrc}";
    cfg = config.my.xsession;
    host = creds.transmission.server;
    port = builtins.toString creds.transmission.port;
    username = creds.transmission.web-username;
    password = creds.transmission.web-password;
in {
  config = lib.mkIf cfg.enable {

    home = {
      activation.transmission-remote = dag.entryAfter [ "writeBoundary" ] ''
       $DRY_RUN_CMD mkdir $VERBOSE_ARG -m0700 -p ${cli-folder}
      '';

      file = {
        "${cli-netrc}".text = ''
machine ${host} port ${port} login ${username} password ${password}
'';

        "bin/transmission-remote" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}

${pkgs.transmission}/bin/transmission-remote ${host}:${port} --netrc ${cli-full-netrc} $@
'';
        };

        "bin/transmission-remote-cleanup" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}
# Remove finalized torrents

APP=~/bin/transmission-remote

$APP --list | grep '100%' | awk '{print $1}' | cut -f1 -d'*' | xargs -r -i{} $APP --torrent {} --remove
'';
        };

        "bin/transmission-remote-gc" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}
# Clean up everything

APP=~/bin/transmission-remote

$APP --list | awk '{print $1}' | cut -f1 -d'*' | xargs -r -i{} $APP --torrent {} --remove
'';
        };

        "bin/transmission-remote-check" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell}
# Check torrent local data
APP=~/bin/transmission-remote

$APP --list | awk '{print $1}' | cut -f1 -d'*' | xargs -r -i{} $APP --torrent {} --verify
          '';
        };
      };
    };
  };
}
