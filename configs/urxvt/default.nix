{ lib, config, pkgs, ... }:

let cfg = config.my.rxvt;
    cfg-xsession = config.my.xsession;
    urxvt-package = pkgs.rxvt-unicode;
in {
  options.my.rxvt = {
    enable = lib.mkEnableOption "URxvt configuration";
  };

  config = lib.mkIf (cfg.enable && cfg-xsession.enable) {
    programs.urxvt = {
      enable = true;
      package = urxvt-package;
      fonts = [
        "xft:DejaVu Sans Mono for Powerline:size=10, xft:Symbola, xft: Noto Emoji:minspace=False"
      ];
      scroll = {
        bar.enable = false;
        lines = 5000;
        scrollOnOutput = true;
      };
      # For the Ctrl+Shift bindings to work, a default binding needs
      # to be disabled (see discussion here):
      iso14755 = false;
      keybindings = {
        # ! clipboard - copy/paste selection clipboard
        "C-M-w" = "perl:clipboard:copy";
        "C-M-y" = "perl:clipboard:paste";

        # "M-C-Up" = "perl:font-size:increase";
        # "M-C-Down" = "perl:font-size:decrease";
        "M-C-S-Up" = "perl:font-size:incglobal";
        "M-S-C-Down" = "perl:font-size:decglobal";
        "M-S-C-r" = "perl:font-size:reset";

        "M-C-j" = "perl:color-themes:prev";
        "M-C-k" = "perl:color-themes:next";
        "M-C-l" = "perl:color-themes:load-state";
        "M-C-s" = "perl:color-themes:save-state";

      };

      extraConfig = with pkgs; {
        # "*termName" = "xterm-256color";
        "iso14755_52" = false;
        "perl-lib" = "${urxvt-package}/lib/urxvt/perl";
        "antialias" = false;

        # "buffered" = true;
        "cursorBlink" = true;
        "perl-ext-common" = "clipboard,font-size,color-themes";
        "url-launcher" = "${config.my.browser.program}";

        # ! the clipboard is automatically updated whenever the PRIMARY selection changes
        "clipboard.autocopy" = true;
        "clipboard.copycmd" = "${xsel}/bin/xsel --input --clipboard";
        "clipboard.pastecmd" = "${xsel}/bin/xsel --output --clipboard";

        # ! font-size plugin: increase/decrease by that much
        "font-size.step" =  4;

        "color-themes.themedir" =  "~/.themes/urxvt";
        "color-themes.state-file" = "~/.themes/urxvt/current";
        "color-themes.autosave" = 1;
        "urgentOnBell" = true;
        "visualBell" = false;

        # ! Some programs like alsamixer and xprop do not perform well
        # ! with some graphics drivers and in consequence redraw very
        # ! slowly. The option "skipBuiltinGlyphs" for ~/.Xresources or
        # ! the command line option -sbg may fix this. One possible
        # ! solution is to add the following to ~/.Xresources:
        "skipBuiltinGlyphs" = true;
        # ! to reduce the pixel separation between character
        # "letterSpace" = -1;
      };
    };
    # Add themes
    home.file = {
      ".themes/urxvt/tango".source = ./themes/tango;
      ".themes/urxvt/solarized_light".source = ./themes/solarized_light;
      ".themes/urxvt/badwolf".source = ./themes/badwolf;
      ".themes/urxvt/felix".source = ./themes/felix;
      ".themes/urxvt/green".source = ./themes/green;
      ".themes/urxvt/pnemva".source = ./themes/pnemva;
    };
  };
}
