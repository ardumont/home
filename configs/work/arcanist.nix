{ lib, pkgs, config, creds, ... }:

let dag = config.lib.dag;
    cfg = config.my.work;
in
{
  config = lib.mkIf cfg.enable {
    home = {
      packages = [ pkgs.arcanist ];

      activation.copy-arc-config =
        let cfgFile = pkgs.writeText "arcrc" ''
{
  "config": {
    "default": "https://forge.softwareheritage.org/",
    "history.immutable": true
  },
  "hosts": {
    "https://forge.softwareheritage.org/api/": {
      "token": "${creds.arcanist.swh}"
    }
  }
}
'';
        in dag.entryAfter ["writeBoundary"]
          "$DRY_RUN_CMD install -m400 -D ${cfgFile} $HOME/.arcrc";

      file."bin/arc" = {
        executable = true;
        text = ''
#!${pkgs.stdenv.shell} -xe

# Force the editor to be a no x instance
# so that i can work remotely without x as well
export EDITOR="$EDITOR -nw"
if [ -f /usr/bin/arc ]; then
  /usr/bin/arc $@
else
  ${pkgs.arcanist}/bin/arc $@
fi
'';
      };
    };
  };
}
