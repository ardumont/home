{ lib, config, pkgs, creds, ... }:

let dag = config.lib.dag;
    cfg = config.my.work;
    cfg-xsession = config.my.xsession;
    h = config.home.homeDirectory;
    dir-work-bin = "${h}/work/bin";
in {
  config = lib.mkIf cfg.enable {
    home = {
      packages =
        with pkgs; [ cassandra_4 ];

      file = {
        ".config/swh/cassandra/staging/cqlshrc".text = ''
[authentication]
credentials = ${h}/.config/swh/cassandra/staging/cqlshrc-creds
keyspace = swh

[connection]
hostname = cassandra1.internal.staging.swh.network
'';

        ".config/swh/cassandra/production/cqlshrc".text = ''
[authentication]
credentials = ${h}/.config/swh/cassandra/production/cqlshrc-creds
keyspace = swh

[connection]
hostname = cassandra01.internal.softwareheritage.org
'';

        "${dir-work-bin}/swh-cqlsh" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

CMD="${cassandra_4}/bin/cqlsh"

case "$1" in
  staging|stg)
    env=staging;
    ;;
  production|prod|prd)
    env=production
    ;;
  *)
    exit 1
    ;;
esac

set -x
$CMD --cqlshrc ${h}/.config/swh/cassandra/$env/cqlshrc

'';

        };
      };

      activation.copy-cqlsh-config = let
        cqlsh-stg = pkgs.writeText "cqlsh-staging" creds.swh.cqlsh.staging;
        cqlsh-prd = pkgs.writeText "cqlsh-production" creds.swh.cqlsh.production;

      in dag.entryAfter ["writeBoundary"] ''
$DRY_RUN_CMD install -m400 -D ${cqlsh-stg} ${h}/.config/swh/cassandra/staging/cqlshrc-creds
$DRY_RUN_CMD install -m400 -D ${cqlsh-prd} ${h}/.config/swh/cassandra/production/cqlshrc-creds
'';
    };
  };
}
