{ lib, config, pkgs, identity, ... }:

let cfg = config.my.work;
    me = identity.me;
in {
  config = lib.mkIf cfg.enable {
    home.file.".devscripts".text = ''
export QUILT_PATCHES=debian/patches
export DEBFULLNAME="${me.fullname}"
export DEBEMAIL=${me.email.work}
export DEBSIGN_KEYID=${me.gpg-id-short}

export DEBUILD_DPKG_BUILDPACKAGE_OPTS="-us -uc -I -i"
export DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"
  '';
  };

}
