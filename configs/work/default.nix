{ pkgs, config, lib, creds, identity, ... }:

let cfg = config.my.work;
in {
  options.my.work = {
    enable = lib.mkEnableOption "Install work development dependencies";
  };

  imports = map ( m: import m { inherit config pkgs lib creds identity; } ) [
    ./arcanist.nix
    ./gitlab.nix
    ./postgres.nix
    ./scripts.nix
    ./debian.nix
    ./development.nix
    ./terraform.nix
    ./slides.nix
    ./cassandra.nix
    ./vbox.nix
  ];
}
