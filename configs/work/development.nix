{ lib, config, pkgs, ... }:

let
  cfg = config.my.work;
  cfg-xsession = config.my.xsession;
in {
  config = lib.mkIf cfg.enable {
    home = {
      packages =
        with pkgs; [ pandoc ]
                   ++ lib.lists.optionals cfg-xsession.enable [ pkgs.inkscape ];
    };
  };
}
