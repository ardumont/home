{ lib, pkgs, config, creds, ... }:

let cfg = config.my.work;
    h = config.home.homeDirectory;
in
{
  config = lib.mkIf cfg.enable {
    home = {
      packages = [ pkgs.glab ];

      file = {
        ".python-gitlab.cfg".text = ''
[global]
default = swh
ssl_verify = true
timeout = 5

[staging-swh]
url = https://gitlab-staging.swh.network
private_token = helper: ~/bin/pass show ardumont/swh/gitlab/staging/api-access-token
api_version = 4

[swh]
url = https://gitlab.softwareheritage.org
private_token = helper: ~/bin/pass show ardumont/swh/gitlab/production/api-access-token
api_version = 4

'';
      ".config/glab-cli/config.yml".text = ''
git_protocol: ssh
editor: ${h}/bin/emacs
browser: ${h}/bin/qutebrowser
glamour_style: dark
check_update: false
# Whether or not to display hyperlink escapes when listing things like issues or MRs
display_hyperlinks: false
hosts:
    gitlab.com:
        api_protocol: https
        api_host: gitlab.com
        # Your GitLab access token.
        # Get an access token at https://gitlab.com/-/profile/personal_access_tokens
        token: ${creds.forge-tokens.gitlab.gitlab}
    gitlab.softwareheritage.org:
        api_protocol: https
        token: ${creds.forge-tokens.gitlab.swh}
    gitlab-staging.swh.network:
        api_protocol: https
        token: ${creds.forge-tokens.gitlab.swh-staging}
'';

    ".config/glab-cli/aliases.yml".text = ''
ci: pipeline ci
co: mr checkout
'';

    ".config/glab-cli/completion-zsh".source = ./glab-completion-zsh;

      };
    };
  };
}
