{ lib, config, pkgs, creds, ... }:

let dag = config.lib.dag;
    cfg = config.my.work;
in
{
  config = lib.mkIf cfg.enable {
    home = {
      packages = [ ];
      activation.copy-pg-config = let
        pg-service = pkgs.writeText "pg-service" creds.swh.pg-service;
        pg-pass = pkgs.writeText "pg-pass" creds.swh.pg-pass;

      in dag.entryAfter ["writeBoundary"] ''
$DRY_RUN_CMD install -m400 -D ${pg-service} $HOME/.pg_service.conf
$DRY_RUN_CMD install -m400 -D ${pg-pass} $HOME/.pgpass
'';
    };
  };
}
