{ lib, config, pkgs, identity, ... }:

# Script independent from graphical session
let h = config.home.homeDirectory;
    # relative path to home
    dir-work = "work/inria";
    dir-work-bin = "${dir-work}/bin";
    dir-work-resources = "${dir-work}/resources";
    # absolute path
    full-dir-work = "${h}/${dir-work}";
    full-dir-work-bin = "${full-dir-work}/bin";
    full-dir-work-resources = "${full-dir-work}/resources";
    # debian build area
    full-dir-debian-build-area = "${h}/debian/build-area";
    cfg = config.my.work;
    me = identity.me;
    python-version = "3.10.9";
in {
  config = lib.mkIf cfg.enable {

    home = {
      packages = with pkgs; [ ipxe dyff ripgrep ];

      file = {
        ".shrc-work".text = ''
# -*- sh -*-

SWH_COMMON=${full-dir-work}/repo/swh
export SWH_ENVIRONMENT_HOME=$SWH_COMMON/swh-environment
export SWH_PUPPET_ENVIRONMENT_HOME=$SWH_COMMON/puppet-environment
export SWH_CI_ENVIRONMENT_HOME=$SWH_COMMON/ci-environment
export SWH_SYSADM_ENVIRONMENT=$SWH_COMMON/sysadm-environment
export SWH_ANNEX_HOME=$SWH_COMMON/swh-annex
export PATH=${full-dir-work-bin}:$PATH

# completion swhpass
SWH_PASSWORD_STORE_DIR=$SWH_SYSADM_ENVIRONMENT/credentials/
function swhpass() {
    PASSWORD_STORE_DIR=$SWH_PASSWORD_STORE_DIR pass $@
}

_swhpass() {
    # trailing / is required for the password-store dir.
    PASSWORD_STORE_DIR=$SWH_PASSWORD_STORE_DIR _pass
}

shell=$(basename $SHELL)

[ $shell = 'zsh' ] && (which compdef >/dev/null) && compdef _swhpass swhpass
# end completion swhpass

release=$(lsb_release -si)

[ "$release" = "Debian" ] && \
  alias tox='export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig/ ; tox'

'';

        "${dir-work}/repo/swh/.envrc".text = ''
        layout pyenv swh-${python-version}
        unset PS1
        '';

        ".pyenv/versions/${python-version}/envs/swh-${python-version}/bin/postactivate".text = ''
# -*- sh -*-

# unfortunately, the interface cmd for the click autocompletion
# depends on the shell
# https://click.palletsprojects.com/en/7.x/bashcomplete/#activation

shell=$(basename $SHELL)
case "$shell" in
    "zsh")
        autocomplete_cmd=source_zsh
        ;;
    *)
        autocomplete_cmd=source
        ;;
esac

export SWH_SCHEDULER_URL=http://127.0.0.1:5008/
export CELERY_BROKER_URL=amqp://127.0.0.1:5072/
export COMPOSE_FILE=$SWH_ENVIRONMENT_HOME/docker/docker-compose.yml:$SWH_ENVIRONMENT_HOME/docker/docker-compose.override.yml
alias doco=docker-compose

eval "$(_SWH_COMPLETE=$autocomplete_cmd swh)"

function swh-clean {
  if [ "$1" = "env" ]; then
    dir=$SWH_ENVIRONMENT_HOME
  else
    dir="."
  fi
  for folder in __pycache__ .tox .hypothesis .pytest_cache .mypy_cache .eggs ; do
    find $dir -type d -name $folder -print | xargs rm -rf
  done
  [ -f .coverage ] && rm -f .coverage
}
      '';
        "${dir-work-bin}/swh-doco-rebuild" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

DOCKER_CMD=${docker}/bin/docker
cd $SWH_ENVIRONMENT_HOME/docker
$DOCKER_CMD build -f Dockerfile -t swh/stack $@ .
'';
        };
        "${dir-work-bin}/swh-doco" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME/docker
docker compose -f docker-compose.yml \
  -f docker-compose.override.yml $@
'';
        };
        "${dir-work-bin}/swh-doco-keycloak" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME/docker
docker compose -f docker-compose.yml \
  -f docker-compose.keycloak.yml \
  -f docker-compose.override.yml $@
'';
        };
        "${dir-work-bin}/swh-doco-search" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME/docker
docker compose -f docker-compose.yml \
  -f docker-compose.search.yml \
  -f docker-compose.override.yml $@
'';
        };
        "${dir-work-bin}/swh-doco-deposit" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME/docker
docker compose -f docker-compose.yml \
  -f docker-compose.deposit.yml \
  -f docker-compose.deposit-azure.yml \
  -f docker-compose.deposit.override.yml \
  -f docker-compose.override.yml $@
'';
        };
        "${dir-work-bin}/swh-doco-vault" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME/docker
docker compose -f docker-compose.yml \
  -f docker-compose.vault.yml \
  -f docker-compose.vault-azure.yml \
  -f docker-compose.vault.override.yml \
  -f docker-compose.override.yml $@
'';
        };
        "${dir-work-bin}/swh-pip-install" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -xe

cd $SWH_ENVIRONMENT_HOME
PATH=/usr/bin:$PATH pip install $( ./bin/pip-swh-packages --with-testing )
'';
        };
        "${dir-work-bin}/swh-debian-upload" = {
          executable = true;
          text = with pkgs; ''# -*- sh -*-
#!${stdenv.shell} -xe

CHANGEFILE=$1
DESTINATION=pergamon.internal.softwareheritage.org
DESTDIR=/srv/softwareheritage/repository

if [ ! -f $CHANGEFILE ]; then
    echo $CHANGEFILE does not exist, stop.
    exit 1
fi

# Check whether the extension is ending with .changes

cd ${full-dir-debian-build-area} 2>&1 >/dev/null
${debian-devscripts}/bin/debsign $CHANGEFILE
${debian-devscripts}/bin/dcmd rsync $CHANGEFILE $DESTINATION:$DESTDIR/incoming
ssh $DESTINATION "umask 002; reprepro -vb $DESTDIR processincoming incoming"
'';
        };
        "${dir-work-bin}/swh-debian-auto-upload" = {
          executable = true;
          text = ''# -*- sh -*-
#!${pkgs.stdenv.shell} -xe

DEBIAN_BUILD_AREA=$1
[ -z $DEBIAN_BUILD_AREA ] && DEBIAN_BUILD_AREA=..

changes=$DEBIAN_BUILD_AREA/$(dpkg-parsechangelog -SSource)_$(dpkg-parsechangelog -SVersion)_amd64.changes
${full-dir-work-bin}/swh-debian-upload "$changes"

'';
        };
        "${dir-work-bin}/swh-prepare-chroots" = {
          executable = true;
          text = ''
# -*- sh -*-

# Install the package
sudo apt-get install sbuild

# Add your user to the sbuild group, to allow him to use the sbuild commands
sudo sbuild-adduser $USER
# You have to logout and log back in

# Prepare chroots
sudo mkdir /srv/chroots
sudo mkdir /srv/chroots/var

# Optionally create a separate filesystem for /srv/chroots and move
# the sbuild/schroot data to that partition
sudo rsync -avz --delete /var/lib/schroot/ /srv/chroots/var/schroot/
sudo rm -r /var/lib/schroot
sudo ln -sf /srv/chroots/var/schroot /var/lib/schroot

sudo rsync -avz --delete /var/lib/sbuild/ /srv/chroots/var/sbuild/
sudo rm -r /var/lib/sbuild
sudo ln -sf /srv/chroots/var/sbuild /var/lib/sbuild
# end optionally

# Create unstable/sid chroot
sudo sbuild-createchroot sid /srv/chroots/sid http://deb.debian.org/debian/

# Create buster chroot
sudo sbuild-createchroot buster /srv/chroots/buster http://deb.debian.org/debian/

# Create sbuild autosign gpg key (!! needs entropy, consider installing haveged
# and not worrying about it again)
sudo sbuild-update --keygen
'';
        };
        "${dir-work-bin}/swh-debian-build-sync" = {
          executable = true;
          text = ''
# -*- sh -*-
#!${pkgs.stdenv.shell} -ex
INITIAL_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)

git fetch --all
git checkout pristine-tar
git clean -f
for directory in .tox .mypy_cache .eggs .hypothesis build dist docs; do
  [ -d $directory ] && rm -rvf $directory
done

find -type d -iname '__pycache__' -exec rm -rf {} \;

git reset --hard origin/pristine-tar
for branch in debian/upstream debian/unstable-swh debian/bullseye-swh debian/buster-swh ; do
  git checkout $branch
  git reset --hard origin/$branch
done

git checkout $INITIAL_BRANCH
'';
        };

        "bin/swh-git-rebase" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

git="${pkgs.git}/bin/git"

$git fetch --all

BRANCH_CUR=$($git branch | grep --basic-regexp "* " | awk '{print $2}')

function cleanup {
  $git switch $BRANCH_CUR
}
trap cleanup EXIT

BRANCH_PRD=$($git branch | grep --basic-regexp " production$" | head -1 | awk '{print $1}')
BRANCH_STG=$($git branch | grep --basic-regexp " staging$" | head -1 | awk '{print $1}')
BRANCH_MST=$($git branch | grep --basic-regexp " master$" | head -1 | awk '{print $1}')

$git fetch -a

if [ ! -z "$BRANCH_PRD" ]; then
  $git switch $BRANCH_PRD
  $git merge origin/$BRANCH_PRD
fi

if [ ! -z "$BRANCH_STG" ]; then
  $git switch $BRANCH_STG
  $git merge origin/$BRANCH_STG
fi

if [ ! -z "$BRANCH_MST" ]; then
  $git switch $BRANCH_MST
  $git merge origin/$BRANCH_MST
fi

$git switch "$BRANCH_CUR"

[ ! -z "$BRANCH_MST" ] && $git rebase $BRANCH_MST && exit

[ ! -z "$BRANCH_STG" ] && $git rebase $BRANCH_STG && exit

'';
        };

        "bin/swh-git-push" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -xe

git="${pkgs.git}/bin/git"

BRANCH_CUR=$($git branch | grep --basic-regexp "* " | awk '{print $2}')
WITH_CLEANUP=$1

function cleanup {
  # check whether the current branch still exist
  BRANCH_CUR=$($git branch | grep --basic-regexp "* " | awk '{print $2}')
  if [ ! -z "$BRANCH_CUR" ]; then
    $git switch $BRANCH_CUR
  else
    $git switch $BRANCH_STG
  fi
}
trap cleanup EXIT

BRANCH_PRD=$($git branch | grep --basic-regexp " production$" | head -1 | awk '{print $1}')
BRANCH_STG=$($git branch | grep --basic-regexp " staging$" | head -1 | awk '{print $1}')

if [ ! -z "$BRANCH_STG" ]; then
  $git switch $BRANCH_STG
  $git merge $BRANCH_CUR
fi

if [ ! -z "$BRANCH_PRD" ]; then
  $git switch $BRANCH_PRD
  $git merge $BRANCH_CUR
fi

[ ! -z "$BRANCH_STG" ] && git push origin $BRANCH_STG
[ ! -z "$BRANCH_PRD" ] && git push origin $BRANCH_PRD

$git switch $BRANCH_STG

if [ ! -z "$WITH_CLEANUP" ]; then
  # Clean up local dev and remote repository
  $git branch -d $BRANCH_CUR
  $git push origin :$BRANCH_CUR
fi

'';

        };

        "${dir-work-bin}/swh-debian-build-unstable" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/unstable-swh" ] || git checkout debian/unstable-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb http://incoming.debian.org/debian-buildd/ buildd-unstable main" \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ unstable main" \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-resources}/cassandra.asc".source = ./cassandra.asc;
        # almost the same script dedicated for storage because i don't seem to
        # find the right string expansions politic
        "${dir-work-bin}/swh-debian-build-unstable-storage" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/unstable-swh" ] || git checkout debian/unstable-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb http://incoming.debian.org/debian-buildd/ buildd-unstable main" \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ unstable main" \
    --extra-repository="deb https://debian.cassandra.apache.org 40x main" \
    --extra-repository-key=${full-dir-work-resources}/cassandra.asc \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-bin}/swh-debian-build-buster-storage" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/buster-swh" ] || git checkout debian/buster-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ buster-swh main" \
    --extra-repository="deb http://deb.debian.org/debian/ buster-backports main" \
    --extra-repository="deb http://www.apache.org/dist/cassandra/debian 40x main" \
    --extra-repository-key=${full-dir-work-resources}/cassandra.asc \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-resources}/elasticsearch.asc".source = ./elasticsearch.asc;
        # almost the same script dedicated for storage because i don't seem to
        # find the right string expansions politic
        "${dir-work-bin}/swh-debian-build-unstable-search" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/unstable-swh" ] || git checkout debian/unstable-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb http://incoming.debian.org/debian-buildd/ buildd-unstable main" \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ unstable main" \
    --extra-repository="deb https://artifacts.elastic.co/packages/7.x/apt stable main" \
    --extra-repository-key=${full-dir-work-resources}/elasticsearch.asc \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-bin}/swh-debian-build-bullseye" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/bullseye-swh" ] || git checkout debian/bullseye-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ bullseye-swh main" \
    --extra-repository="deb http://deb.debian.org/debian/ bullseye-backports main" \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-bin}/swh-debian-build-buster" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -ex

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)
[ $CURRENT_BRANCH = "debian/buster-swh" ] || git checkout debian/buster-swh

gbp buildpackage --git-builder=sbuild \
    --nolog --batch \
    --no-clean-source \
    --no-run-lintian \
    --arch-all \
    --source \
    --force-orig-source \
    --build-dep-resolver=aptitude \
    --extra-repository="deb [trusted=yes] https://debian.softwareheritage.org/ buster-swh main" \
    --extra-repository="deb http://deb.debian.org/debian/ buster-backports main" \
    --build-failed-commands %SBUILD_SHELL \
    $@
'';
        };

        "${dir-work-bin}/swh-debian-build-sign" = {
          executable = true;
          text = ''
# -*- sh -*-
gbp buildpackage --git-tag-only --git-sign-tag
'';
        };
        "${dir-work-bin}/swh-octo-diff" = {
          executable = true;
          text = ''
# -*- sh -*-

cd $SWH_PUPPET_ENVIRONMENT_HOME/swh-site

CURRENT_BRANCH=$(git branch | grep "*" | cut -d' ' -f2)

set -x

$SWH_PUPPET_ENVIRONMENT_HOME/bin/octocatalog-diff --to $CURRENT_BRANCH $*

set +x

cd -
'';
        };

        "${dir-work-bin}/swh-weekly-report" = {
          executable = true;
          text = ''
#!${pkgs.stdenv.shell} -e

report=$SWH_ENVIRONMENT_HOME/snippets/swh-team/swh-weekly-report

DAYS=$1
if [ -z $DAYS ]; then
  DAYS=7
fi

python $report --days $DAYS
'';
        };
        "${dir-work-bin}/swh-git-configure-user" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -e

CMD="${git}/bin/git"

$CMD config user.name "${me.fullname}"
$CMD config user.email "${me.email.work}"
'';
        };
        "${dir-work-bin}/swh-git-configure-origin" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -e

set -e

usage () {
    echo "$0 <origin-to-configure>"
    exit 1
}

origin_to_configure=$1

[ -z "$origin_to_configure" ] && usage

GIT="${git}/bin/git"
GREP="grep"

remotes=$($GIT remote -v)

grab_remote () {
    echo $remotes | $GREP $1 | head -1 | awk '{print $2}'
}

origin=$(grab_remote "origin")
upstream=$(grab_remote "upstream")

if [ -z "$upstream" ]; then
    $GIT remote rename origin upstream
    $GIT remote add origin $origin_to_configure
fi
'';
        };
       "${dir-work-bin}/kube-swh-clean-pv" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -ex

# Drop the persistent volumes
ROOT_DIR=/srv/softwareheritage-kube/dev/

sudo rm -rf $ROOT_DIR/{*-cache,elasticsearch,*-db,objects,redis,grafana,kafka,prometheus,zookeeper}
'';
        };
       "${dir-work-bin}/generate-frozen-requirements-and-image-then-release" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -e

# example: swh-loader-bzr
APP_NAME=$1
# example: loader_bzr
IMAGE_NAME=$2
TODAY=$(date '+%Y%m%d')
IMAGE_VERSION=$3
[ -z "$IMAGE_VERSION" ] && $IMAGE_VERSION="$TODAY.1"

base_dir=$SWH_SYSADM_ENVIRONMENT/swh-apps

echo -e "\nBuild docker image build-deps to generate frozen requirements."
cd $base_dir/scripts/
docker build -t build-deps .

echo -e "\nGenerate generate frozen requirements for application $APP_NAME."
cd $base_dir
docker run --rm -v $PWD:/src build-deps generate-frozen-requirements $APP_NAME
cd $base_dir/apps/$APP_NAME

REGISTRY=container-registry.softwareheritage.org/swh/infra/swh-apps
FULL_IMAGE=$REGISTRY/$IMAGE_NAME:$IMAGE_VERSION
FULL_IMAGE_LATEST=$REGISTRY/$IMAGE_NAME:latest

echo -e "\nBuild docker image with frozen requirements for application $APP_NAME"
docker build -t $FULL_IMAGE .

echo -e "\nRelease docker image $FULL_IMAGE to registry (y or n)?"
read answer

if [ "$answer" = "y" ]; then
  echo -e "\nRelease docker image $FULL_IMAGE to registry."
  docker tag $FULL_IMAGE $FULL_IMAGE_LATEST
  docker push $FULL_IMAGE
  docker push $FULL_IMAGE_LATEST
fi

  '';
       };

       "${dir-work-bin}/swh-gitlab-reference.py" =
          let my-python3 = (pkgs.python310.withPackages (ps: [pkgs.python310Packages.click]));
          in {
            executable = true;
            text = ''
#!${my-python3.interpreter}

import click

@click.command()
@click.argument("url")
def main(url):
    import re

    SEPARATOR = {
        "issues": "#",
        "merge_requests": "!",
        "commit": "@",
        "snippets": "$",
    }

    # https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-site/-/commit/391c8a4118ea135e65fbc7c1a1c21045a531afac
    # https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-site/-/merge_requests/669

    for pattern_name in SEPARATOR.keys():
        pattern = rf"^(?P<scheme>[\w:/]*)/(?P<domain>[-.a-zA-Z]+)/(?P<project>[-_\w\W\d]+)/-/{pattern_name}/"
        if pattern_name == "commit":
            pattern += r"(?P<id>[0-9a-zA-Z]+)$"
        else:
            pattern += r"(?P<id>[0-9]+)([#]{1}(?P<internal>[\W\w\d]+)+)*$"
        result = re.match(pattern, url, flags=re.VERBOSE | re.IGNORECASE)

        if result:
            result_d = result.groupdict()

            project = result_d['project']
            issue_or_mr_id = result_d['id']

            separator = SEPARATOR[pattern_name]

            if pattern_name == "commit":
                issue_or_mr_id = result_d['id'][:8]

            ref = f"{project}{separator}{issue_or_mr_id}"
            print(ref)
            return ref


if __name__ == '__main__':
    main()

      '';
          };

        "${dir-work-bin}/integrity" =
          let my-python3 = (pkgs.python310.withPackages (ps: [pkgs.python310Packages.click]));
          in {
            executable = true;
            text = ''
#!${my-python3.interpreter}

import click
import base64


def encode_b64(checksum: str):
    return base64.encodebytes(checksum.encode()).decode()


def decode_b64(integrity: str):
  checksum_algo, chksum_b64 = integrity.split("-")
  return checksum_algo, base64.decodebytes(chksum_b64.encode()).hex()


@click.group()
@click.pass_context
def group(ctx):
    pass


@group.command("encode")
@click.argument("algo")
@click.argument("checksum")
def encode_cli(algo: str, checksum: str):
    """Compute integrity hash out of an algorithm and a hash value.

    """
    print(rf"{algo}-{encode_b64(checksum)}")


@group.command("decode")
@click.argument("integrity")
def decode_cli(integrity: str):
    """Compute checksum hash out of an integrity field.

    """
    checksum_algo, chksum_b64 = decode_b64(integrity)
    click.echo(f"{checksum_algo} {chksum_b64}")


if __name__ == '__main__':
   group()
'';
          };

        "${dir-work-bin}/check-rabbit.py" =
          let my-python3 = (pkgs.python3.withPackages (ps: with pkgs.python3Packages; [click pika pyyaml]));
          in {
            executable = true;
            text = ''
#!${my-python3.interpreter}

#!/usr/bin/env python3
# Check connection to the RabbitMQ server

import click
import pika
import ssl
import yaml


@click.command()
@click.option("--server", "-s", required=True,
               help="Define RabbitMQ server")
@click.option("--virtual_host", "-v", default="/",
               help="Define virtual host")
@click.option("--ssl/--no-ssl",
              "ssl_flag",
              is_flag=True,
              default=False,
              help="Enable SSL")
@click.option("--port", type=click.INT, default=5672,
               help="Define port (default: %(default)s)")
@click.option("--username",
               default="guest",
               help="Define username")
@click.option("--password",
               default="guest",
               help="Define password")
@click.option(
    "--config-file",
    "-C",
    default=None,
    type=click.Path(exists=True, dir_okay=False),
    help="Configuration file.",
)
def main(server, virtual_host, ssl_flag, port, username, password, config_file):
    """Check connection to RabbitMQ server"""

    # set amqp credentials
    credentials = pika.PlainCredentials(username, password)

    ssl_options = (
        pika.SSLOptions(ssl.create_default_context(), server)
        if ssl_flag
        else None
    )

    parameters = pika.ConnectionParameters(
        host=server,
        port=port,
        virtual_host=virtual_host,
        credentials=credentials,
        ssl_options=ssl_options
    )

    # Establish connection and status check
    try:
        connection = pika.BlockingConnection(parameters)
        if connection.is_open:
            msg, return_code = ("ok", 0)
            connection.close()
    except Exception as error:
        msg, return_code = (f"ko - {error}", 1)
    finally:
        print(f"Server connection <{server}:{port}> with user <{username}> status: {msg}")
        exit(return_code)


if __name__ == "__main__":
    main()
      '';
        };

        "${dir-work-bin}/swhid-origin" =
          let my-python3 = (pkgs.python3.withPackages (ps: [pkgs.python310Packages.click]));
          in {
            executable = true;
            text = ''
#!${my-python3.interpreter}

import click
import hashlib


@click.command("compute")
@click.argument("origin")
def cli(origin: str):
    """Compute the origin swhid

    """
    h = hashlib.sha1()
    h.update(origin.encode('utf-8'))
    click.echo(f"swh:1:ori:{h.hexdigest()}")


if __name__ == '__main__':
   cli()
'';
          };
        "${dir-work-bin}/my-swh" = {
            executable = true;
            text = ''
swh $@ 2>&1 | grep -v 'not load subcommand'
     '';
          };
       "${dir-work-bin}/kube-swh-prepare" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -ex

# prepare the kube folder environment (except for the local registry)

sudo mkdir -p /srv/softwareheritage-kube/dev/{objects,storage-db,scheduler-db,kafka,web-db,prometheus,zookeeper/data,zookeeper/datalog,grafana,elasticsearch,redis,idx-storage-db,vault-db,vault-cache,deposit-db,deposit-cache}
sudo chown 1000:1000 /srv/softwareheritage-kube/dev/{objects,elasticsearch,vault-cache,deposit-cache}
sudo chown -R 999:999 /srv/softwareheritage-kube/dev/*-db
sudo chown 472:0 /srv/softwareheritage-kube/dev/grafana
sudo chown nobody:nogroup /srv/softwareheritage-kube/dev/prometheus

'';
        };
       "${dir-work-bin}/kube-swh-clean-registry" = {
          executable = true;
          text = with pkgs; ''
#!${stdenv.shell} -ex

KUBECTL=~/bin/kubectl
REGISTRY=$($KUBECTL get pods --no-headers -l app=registry-deployment)

$KUBECTL exec $REGISTRY \
  -- /bin/registry garbage-collect \
       -m /etc/docker/registry/config.yml

'';
        };
      };
    };
  };
}
