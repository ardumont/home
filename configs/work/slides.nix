{ lib, pkgs, config, creds, ... }:

let cfg = config.my.work;
in
{
  config = lib.mkIf cfg.enable {
    home = {
      packages = with pkgs; [
        texlive.combined.scheme-full
        rubber
      ];
    };
  };
}
