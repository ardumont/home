{ lib, pkgs, config, creds, ... }:

let dag = config.lib.dag;
    cfg = config.my.work;
in
{
  config = lib.mkIf cfg.enable {
    home = let hypervisor-name = "branly";
               hypervisor-fqdn = "${hypervisor-name}.internal.softwareheritage.org"; in
      {
        file.".config/terraform/swh/setup.sh" = {
          executable = true;
          text = ''
# Terraform configuration
export PM_USER=ardumont@pam
export PM_PASS=$(pass ls ardumont/swh/sudo | head -1)

# terraform/rancher configuration
key_entry=operations/rancher/azure/elastic-loader-lister-keys
export RANCHER_ACCESS_KEY=$(swhpass ls $key_entry | head -1 | cut -d: -f1)
export RANCHER_SECRET_KEY=$(swhpass ls $key_entry | head -1 | cut -d: -f2)

# Packer configuration
export PROXMOX_USER=$PM_USER
export PROXMOX_PASSWORD=$PM_PASS
export PROXMOX_URL=https://${hypervisor-fqdn}:8006/api2/json
export PROXMOX_NODE=${hypervisor-name}

# URL where the vm could reach the local http server started by packer
export HTTP_SERVER_URL=http://${hypervisor-fqdn}:8889

# Build environment
export TEMPLATE_IP=192.168.100.214
export TEMPLATE_NETMASK=255.255.255.0
export TEMPLATE_GW=192.168.100.1
export TEMPLATE_NS=192.168.100.29

'';
      };
    };
  };
}
