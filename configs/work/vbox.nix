{ lib, config, pkgs, ... }:

let
  cfg = config.my.work;
  cfg-xsession = config.my.xsession;
in {
  config = lib.mkIf cfg.enable {
    home = {
      packages =
        with pkgs; [ virtualboxHardened ];
    };
  };
}
