{ config, lib, pkgs, system, ... }:

with lib;
let cfg-xsession = config.my.xsession;
    cfg = config.my.xkeysnail;
    folder-xkeysnail=".config/xkeysnail";
    config-xkeysnail="${folder-xkeysnail}/config.py";
    dag = config.lib.dag;
in {
  options.my.xkeysnail = {
    enable = lib.mkEnableOption "Install xkeysnail related tools";
  };
  config = mkIf (cfg-xsession.enable && cfg.enable)  {
    # Ensure we have the folder where to install the configuration
    home.activation.xkeysnail = dag.entryAfter [ "writeBoundary" ] ''
      $DRY_RUN_CMD mkdir $VERBOSE_ARG -m0700 -p "${folder-xkeysnail}"
    '';

    home.file = {
      # install the configuration
      "${config-xkeysnail}".source = ./config.py;

      # and the script using it
      "bin/xkeysnail" = {
        executable = true;
        text = with pkgs; ''
#!${pkgs.stdenv.shell}

${pkgs.xorg.xhost}/bin/xhost +SI:localuser:root
${xkeysnail}/bin/xkeysnail $HOME/${config-xkeysnail} &
'';
      };
    };

    systemd.user.services.xkeysnail = {
      Unit = {
        Description = "xkeysnail";
      };
      Service = {
        Type = "oneshot";
        ExecStart = "${config.home.homeDirectory}/bin/xkeysnail";
        RemainAfterExit = "yes";
        ExecStop = "${pkgs.procps}/bin/pkill xkeysnail";
      };
    };
  };
}
