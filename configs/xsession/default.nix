{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.my.xsession;
in {
  options.my.xsession = {
    enable = mkEnableOption "Install x session dependencies user setup";
  };

  imports = [
    ./scripts.nix
    ./session-lock.nix
  ];

  config = mkIf cfg.enable {
    fonts.fontconfig.enable = lib.mkForce true;
    home.packages = with pkgs; [
      rofi
      unixtools.ifconfig
      xdotool simplescreenrecorder
      xosview2
      xorg.xbacklight
      # alsaUtils
      gnome.zenity
      libnotify
      dejavu_fonts powerline-fonts plantuml
      graphviz
      fontconfig
      stalonetray screenkey
      arandr
      scrot
      audacious pavucontrol
      apvlv
      feh
      dia
      mpv
      nox
      # rather large dependencies
      pinta
      tuxguitar
      sweethome3d.application
      gparted
      hplip
      cups
    ];
    home.file = {
      ".config/xmobar/xmobarrc".source = ./xmobar.hs;
      ".config/screenkey.json".source = ./screenkey.json;
      ".stumpwmrc".source = ./.stumpwmrc;
      ".stumpwm-functions.lisp".source = ./.stumpwm-functions.lisp;
      ".stumpwm-setup-reloaded.lisp".source = ./.stumpwm-setup-reloaded.lisp;
    };
    # default session will start xmonad
    xsession = {
      enable = true;
      windowManager = {
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
          extraPackages = self: [
            self.xmobar self.xmonad-contrib
          ];
          config = ./xmonad.hs;
        };
      };
      # startup initialization (user services, keyboard mapping, etc...)
      initExtra = with pkgs; ''
# On some machine, somehow, this ends up polluting shells (ubuntu
# 18.04 comes to mind)
unset LD_PRELOAD QT4_IM_MODULE QT_ACCESSIBILITY QT_IM_MODULE \
      QT_QPA_PLATFORMTHEME

# Set a default pointer.
${xorg.xsetroot}/bin/xsetroot -cursor_name left_ptr

# Turn off beeps.
${xorg.xset}/bin/xset -b

# Quicker blanking of screen.
${xorg.xset}/bin/xset dpms 120 360 800

# set the keyboard repeat rate
${xorg.xset}/bin/xset r rate 200 60

export GTK_IM_MODULE=xim

# before starting xmonad, we want a correct X db and a running urxvt daemon
[ -e ~/.Xresources ] && ${xorg.xrdb}/bin/xrdb -merge ~/.Xresources
[ -e ~/.Xdefaults ] && ${xorg.xrdb}/bin/xrdb -merge ~/.Xdefaults

# fix graphical libraries (awt, swt, etc...) in stumpwm/xmonad (exotic wm)
export _JAVA_AWT_WM_NONREPARENTING=1
${wmname}/bin/wmname "LG3D"
export AWT_TOOLKIT=MToolkit

# Layout per host divergence possible
[ -e $LAYOUT_FILE_PER_HOST ] && ${zsh}/bin/zsh $LAYOUT_FILE_PER_HOST

${pkgs.trayer}/bin/trayer --edge top \
                          --align right \
                          --SetDockType true \
                          --SetPartialStrut true \
                          --expand true \
                          --width 15 \
                          --height 21 \
                          --transparent true \
                          --tint 0x000000 &

# start needed services
systemctl --user start setxkbmap.service
# set the compose key to super/win key
setxkbmap -option compose:lwin
systemctl --user start syndaemon.service
systemctl --user start emacs.service
systemctl --user start xscreensaver.service
    '';
  };

  services ={
    redshift = {
      enable = true;
      latitude = "2.529885";
      longitude = "48.917335";
    };

    clipmenu = {
      enable = true;
      launcher = "rofi";
    };
  };

  home.file."bin/clipmenu" = {
    executable = true;
    text = with pkgs; ''
#!${stdenv.shell} -xe

export CM_LAUNCHER=rofi
${pkgs.clipmenu}/bin/clipmenu -p clipboard

'';
  };

  home.file."bin/xmonad-action" = {
    executable = true;
    text = with pkgs; ''
#!${stdenv.shell} -xe

case $1 in
  "recompile")
    action=recompile
    msg='XMonad recompiled!'
    ;;
  "restart")
    action=restart
    msg='XMonad restarted!'
    ;;
  *)
    echo "Unknown action"
    exit 1;
esac

pushd $HOME/.xmonad
$HOME/.nix-profile/bin/xmonad --$action
${libnotify}/bin/notify-send -t 1000 $msg
popd
    '';
  };

  home.file."bin/xmobar" = {
    executable = true;
    text = with pkgs; ''
#!${stdenv.shell} -xe

PIDS=`${procps}/bin/pgrep xmobar`

if [ $? -eq 0 ]; then
    for PID in $PIDS; do
        kill $PID > /dev/null &
    done
fi

${xmobar}/bin/xmobar ~/.config/xmobar/xmobarrc
    '';
  };

  # some bindings to draw emoji
  home.file.".XCompose".text = ''
# -*- coding: utf-8 -*-
include "%L"

<Multi_key> <c> <r> : "(T_T)"
<Multi_key> <s> <h> : "¯\\_(ツ)_/¯"
<Multi_key> <f> <l> : "(╯°□°）╯︵ ┻━┻"
<Multi_key> <f> <p> : "/)_-) "
<Multi_key> <r> <e> : "(◔_◔) "
<Multi_key> <t> <h> : "*thumbs up*"
<Multi_key> <n> <o> : "*nods*"
<Multi_key> <g> <r> : "*grins*"
<Multi_key> <d> <o> : "d'oh"
<Multi_key> <w> <i> : "\\o/"
<Multi_key> <r> <o> : "\\m/"
<Multi_key> <a> <n> : "(è_é)"
<Multi_key> <b> <r> : "[ ]"

# music
# <Multi_key> <s> <i> : U266B # &#9835; ♫
<Multi_key> <s> <i> : "🎶"

# urls
<Multi_key> <f> <a> : "https://gitlab.softwareheritage.org/dashboard/activity"
<Multi_key> <d> <r> : "https://forge.softwareheritage.org/diffusion"  # repositories
<Multi_key> <d> <t> : "https://forge.softwareheritage.org/tasks"
<Multi_key> <d> <p> : "https://forge.softwareheritage.org/paste"
<Multi_key> <d> <g> : "https://wiki.softwareheritage.org/wiki/Git_style_guide"
<Multi_key> <d> <d> : "https://docs.softwareheritage.org/devel/getting-started.html#getting-started"
<Multi_key> <d> <s> : "https://docs.softwareheritage.org/devel/developer-setup.html#developer-setup"
<Multi_key> <k> <p> : "https://auth.softwareheritage.org/auth/admin/SoftwareHeritage/console/#/realms/SoftwareHeritage"
<Multi_key> <k> <s> : "https://auth.softwareheritage.org/auth/admin/SoftwareHeritageStaging/console/#/realms/SoftwareHeritageStaging"

# accent
<Multi_key> <a> <`>: "à"
<Multi_key> <e> <'>: "é"
<Multi_key> <e> <`>: "è"
<Multi_key> <e> <^>: "ê"
<Multi_key> <u> <`>: "ù"
<Multi_key> <c> <,>: "ç"

  '';

  systemd.user.services.syndaemon = {
    Unit = {
      Description = "syndaemon";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "graphical-session.target" ];
    };

    Service = {
      ExecStart = "${pkgs.xorg.xf86inputsynaptics}/bin/syndaemon -t -k -i 2";
    };

    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
  };
  };
}
