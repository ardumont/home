{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.my.xsession;
in {
  config = mkIf cfg.enable {
    home.file = {
      "bin/print-screen" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell} -xe

SCREENSHOT_DIR=$HOME/Pictures/screenshots
[ ! -d $SCREENSHOT_DIR ] && mkdir -p $SCREENSHOT_DIR

case $1 in
  "mouse")
    cmd="${pkgs.scrot}/bin/scrot -s"
    ;;
  *)
    cmd="${pkgs.scrot}/bin/scrot -u"
    ;;
esac

screenshot="$SCREENSHOT_DIR/$(date +%F_%H-%M-%S).png"
$cmd $screenshot
${libnotify}/bin/notify-send -t 1000 "$(basename $screenshot) done!"
'';
      };

      # sometimes, the caps lock activates itself (don't know how yet)
      # As i can't remember the cmd to reset, wrap it in a script
      "bin/reset-capslock" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell} -xe

${xdotool}/bin/xdotool key Caps_Lock
'';
      };
      "bin/toggle-touchpad" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell} -xe

xinput=${xorg.xinput}/bin/xinput
# Find the identifier of the touchpad
TOUCHPAD_ID=$($xinput list \
             | grep -i touchpad \
             | grep -o "id=[0-9]*" \
             | cut -f 2 -d'=')

TOUCHPAD_STATUS=$($xinput --list-props $TOUCHPAD_ID \
                  | grep "Device Enabled " \
                  | cut -d':' -f 2)

# activate or deactivate?
[ $TOUCHPAD_STATUS = "0" ] && ACTION="--enable" || ACTION="--disable"

# commit the actions
$xinput $ACTION $TOUCHPAD_ID
'';
      };

      "bin/banish-mouse" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell} -xe

# Use: Banish the mouse to the bottom right corner of the screen

DIMENSION=$(${xorg.xdpyinfo}/bin/xdpyinfo | \
            ${gnugrep}/bin/grep "dimensions:" | \
            awk '{print $2}')
WIDTH=$(echo $DIMENSION | cut -d'x' -f1)
HEIGHT=$(echo $DIMENSION | cut -d'x' -f2)

${xdotool}/bin/xdotool mousemove $WIDTH $HEIGHT
'';
  };

      "bin/rxvt" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell}

terminal=${config.home.sessionVariables.TERM}
${config.programs.urxvt.package}/bin/urxvt -tn $terminal $@
    '';
      };

      "bin/mic-connect" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell}

set -xe

CMD=${bluez}/bin/bluetoothctl

# Existing connectable devices:
# OpenRun Pro by Shokz
# OneOdio A70

DEVICES=$($CMD devices | grep -i $1 | awk '{print $2}' | sort | uniq)

for device in $DEVICES; do
  $CMD connect $device &
done

      '';
      };

      "bin/mic-jack-in" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell}

set -xe

CMD=${pulseaudio}/bin/pactl
CARD_NAME=alsa_card.pci
PROFILE=output:analog-stereo+input:analog-stereo

CARD_ID=$($CMD list short cards | grep -i $CARD_NAME | awk '{print $1}')

# toggle off other sources
$CMD list short cards | grep -v $CARD_ID | awk '{print $1}' | xargs -r -t -i{} pactl set-card-profile {} off

$CMD set-card-profile $CARD_ID $PROFILE
      '';
      };


      "bin/mic-toggle-speak" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell}

set -xe

CMD=${pulseaudio}/bin/pactl
CARD_NAME=bluez
PROFILE=headset-head-unit

# Profiles:
#   off: Off (sinks: 0, sources: 0, priority: 0, available: yes)
#   a2dp-sink: High Fidelity Playback (A2DP Sink) (sinks: 1, sources: 0, priority: 16, available: yes)
#   headset-head-unit: Headset Head Unit (HSP/HFP) (sinks: 1, sources: 1, priority: 1, available: yes)
#   a2dp-sink-sbc: High Fidelity Playback (A2DP Sink, codec SBC) (sinks: 1, sources: 0, priority: 18, available: yes)
#   a2dp-sink-sbc_xq: High Fidelity Playback (A2DP Sink, codec SBC-XQ) (sinks: 1, sources: 0, priority: 17, available: yes)
#   headset-head-unit-cvsd: Headset Head Unit (HSP/HFP, codec CVSD) (sinks: 1, sources: 1, priority: 2, available: yes)
#   headset-head-unit-msbc: Headset Head Unit (HSP/HFP, codec mSBC) (sinks: 1, sources: 1, priority: 3, available: yes)

CARD_ID=$($CMD list short cards | grep -i $CARD_NAME | awk '{print $1}')

# toggle off other sources
$CMD list short cards | grep -v $CARD_ID | awk '{print $1}' | xargs -r -t -i{} pactl set-card-profile {} off

$CMD set-card-profile $CARD_ID $PROFILE
      '';
      };

      "bin/mic-toggle-listen" = {
        executable = true;
        text = with pkgs; ''
#!${stdenv.shell}

set -xe

CMD=${pulseaudio}/bin/pactl
CARD_NAME=bluez5
PROFILE=a2dp-sink

CARD_ID=$($CMD list short cards | grep -i $CARD_NAME | awk '{print $1}')

# toggle off other sources
$CMD list short cards | grep -v $CARD_ID | awk '{print $1}' | xargs -r -t -i{} pactl set-card-profile {} off

$CMD set-card-profile $CARD_ID $PROFILE
      '';
      };

      "bin/screen-reset" = with pkgs; {
        executable = true;
        text = ''
#!${stdenv.shell} -xe
systemctl --user restart setxkbmap

# Reset to the laptop's screen
${xorg.xrandr}/bin/xrandr -s 0
'';
    };

      "bin/connect-or-reset-screens" = with pkgs; {
        executable = true;
        text = ''
#!${python3}/bin/python3

from typing import Dict, Iterator, List, Tuple

from collections import defaultdict
import subprocess


execute_side_effect = list


XRANDR = "${xorg.xrandr}/bin/xrandr"


def execute_cli(cmd: List[str]) -> Iterator[str]:
    """Execute cmd and yields its result lines.

    """
    if len(cmd) > 1:
        print(" ".join(cmd))

    yield from subprocess.check_output(cmd).decode("utf-8").splitlines()


def xrandr() -> Iterator[str]:
    """Execute xrandr and yields its output lines.

    """
    yield from execute_cli([XRANDR])


def screen_id(screen_output_line: str) -> str:
    """Return the screen identifier out of an xrandr line output

    """
    return screen_output_line.split()[0]


def status_screens() -> Dict[str, List[str]]:
    """Returns the dict of connected/disconnected screens out of the xrandr output.

    """
    screens = defaultdict(list)
    for screen in xrandr():
        sid = screen_id(screen)
        if " connected " in screen:
            screens["connected"].append(sid)
        elif " disconnected " in screen:
            screens["disconnected"].append(sid)
    return screens


SCREEN_CONFIGURATIONS: Dict[Tuple[str, str], str] = {
    ("eDP-1", "HDMI-1"): {  # home, no dock
        "eDP-1": {
            "pos": "0x0",
            "mode": "1920x1080",
        },
        "HDMI-1": {
            "primary": True,
            "mode": "1920x1080",
            "left-of": "eDP-1",
        }
    },
    ("eDP-1", "DP-1-1"): {  # home|work, office-dock
        "eDP-1": {  # laptop screen
            "pos": "0x0",
            "mode": "1920x1080",
        },
        "DP-1-1": {  # office screen
            "primary": True,
            "pos": "1920x0",
            "mode": "1920x1200",
            "right-of": "eDP-1",
        }
    },
    ("eDP-1", "DP-1"): {  # home|work, laptop-dell-dock
        "eDP-1": {  # laptop screen
            "pos": "0x0",
            "mode": "1920x1080",
        },
        "DP-1": {  # home screen
            "primary": True,
            "pos": "1920x0",
            "mode": "1920x1080",
            "left-of": "eDP-1",
        }
    },
    ("eDP-1", "DP-1-2"): {  # home|work, laptop-hp-dock
        "eDP-1": {  # laptop screen
            "pos": "0x0",
            "mode": "1920x1080",
        },
        "DP-1-2": {  # home screen
            "primary": True,
            "pos": "1920x0",
            "mode": "1920x1080",
            "left-of": "eDP-1",
        }
    },
    ("eDP-1", ): {  # one screen
        "eDP-1": {
            "pos": "0x0",
            "mode": "1920x1080",
            "primary": True,
        },
    },
}


def xrandr_cli(status: Dict[str, List[str]], global_config: Dict) -> List[str]:
    """Generate xrandr cli to connect screens.

    """
    cmd = [XRANDR]

    connected_screens = tuple(status["connected"])
    config = global_config[connected_screens]
    for screen in connected_screens:
        cfg = config[screen]
        cmd.extend(["--output", screen])
        primary = cfg.get("primary")
        if primary:
            cmd.extend(["--primary"])
        cmd.extend(["--mode", cfg["mode"]])
        pos = cfg.get("pos")
        if pos:
            cmd.extend(["--pos", pos])
        left_of_screen = cfg.get("left-of")
        if left_of_screen:
            cmd.extend(["--left-of", left_of_screen])

    disconnected_screens = status["disconnected"]
    for screen in disconnected_screens:
        cmd.extend(['--output', screen, '--off'])

    return cmd


def main() -> None:
    """Plainly output connected screen ids out of the xrandr cli output.

    """
    status = status_screens()
    run_screens = xrandr_cli(status, SCREEN_CONFIGURATIONS)
    lazy_action = execute_cli(run_screens)

    execute_side_effect(lazy_action)

if __name__ == '__main__':
    main()
'';

      };
    };
  };
}
