{ pkgs, config, lib, ... }:

with lib;
let cfg = config.my.xsession;
in {
  config = mkIf cfg.enable {
    # override the service's exec start to use a wrapper one defined below
    systemd.user.services.xscreensaver.Service.ExecStart =
      lib.mkForce (toString (pkgs.writeShellScript "xscreensaver-wrapper" ''
        if [ -f /usr/bin/xscreensaver ]; then
          # on debian, the nix xscreensaver does not work it refuses to display the
          # password prompt my best guess is an issue with pam but i cannot entangle it
          # yet so right now using debian's packaged xscreensaver over the nix one
          locker=/usr/bin/xscreensaver
        else
          locker=${pkgs.xscreensaver}/bin/xscreensaver
        fi

        $locker -no-splash
      ''));

    services.xscreensaver.enable = true;

    home = {
      packages = [ pkgs.xscreensaver ];
      file = {
        "bin/xscreensaver-lock" = {
          executable = true;
          text = ''
            default_cmd=/usr/bin/xscreensaver-command
            if [ -f $default_cmd ]; then
              lock=$default_cmd
            else
              lock=${pkgs.xscreensaver}/bin/xscreensaver-command
            fi

            $lock -lock
'';
        };
        ".Xdefaults".text = ''
xscreensaver.dateFormat:
xscreensaver.passwd.body.label:
xscreensaver.passwd.heading.label:
xscreensaver.passwd.login.label:
xscreensaver.passwd.thermometer.width:  2
xscreensaver.passwd.uname:              False
xscreensaver.passwd.unlock.label:
xscreensaver.Dialog.background:         #000000
xscreensaver.Dialog.foreground:         #ffffff
xscreensaver.Dialog.Button.background:  #000000
xscreensaver.Dialog.Button.foreground:  #ffffff
xscreensaver.Dialog.text.background:    #000000
xscreensaver.Dialog.text.foreground:    #ffffff

xscreensaver.Dialog.shadowThickness:    1
xscreensaver.Dialog.topShadowColor:     #000000
xscreensaver.Dialog.bottomShadowColor:  #000000
'';

        ".xscreensaver".text = with pkgs; ''
timeout: 0:20:00
cycle: 0:20:00
lock: True
lockTimeout: 0:20:00
passwdTimeout: 0:00:30
verbose: False
timestamp: True
splash: False
splashDuration: 0:00:05
demoCommand: ${xscreensaver}/bin/xscreensaver-demo
prefsCommand: ${xscreensaver}/bin/xscreensaver-demo -prefs
nice: 10
memoryLimit: 0
fade: True
unfade: False
fadeSeconds: 0:00:03
fadeTicks: 20
captureStderr: False
ignoreUninstalledPrograms: True
font: "xft:DejaVu Sans Mono for Powerline:size=20"
dpmsEnabled: True
dpmsQuickOff: False
dpmsStandby: 0:05:00
dpmsSuspend: 0:05:00
dpmsOff: 0:00:00
grabDesktopImages: False
grabVideoFrames:  False
chooseRandomImages: False
imageDirectory:
mode: blank
selected: 78
textMode: program
textLiteral: XScreenSaver
textFile:
textProgram:
textURL:
'';
      };
    };
  };
}
