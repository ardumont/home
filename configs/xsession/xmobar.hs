Config { font = "xft:DejaVu Sans Mono for Powerline:size=10"
       , bgColor = "black"
       , fgColor = "grey"
       -- top, left-aligned, 85% of screen width
       , position = TopW L 85
       , lowerOnStart = False
       , overrideRedirect = False,
       , persistent = True
       , hideOnStart = False
       , allDesktops = False
       , commands = [
           Run DynNetwork ["-L","0","-H","32","--normal","green","--high","red"] 10
         , Run MultiCpu ["-L","15","-H","50","--normal","green","--high","red",
                         "-t","cpu: <total>%"] 10
         , Run Memory ["-t","mem: <usedratio>%"] 10
         , Run Swap ["-t","swp: <usedratio>%"] 10
         , Run Date "%d/%m/%Y %H:%M" "date" 10
         , Run Battery ["-t", "<acstatus><watts> (<left>%)",
                        "-L","25",
                        "-H","75",
                        "-h","green",
                        "-n","yellow",
                        "-l","red"] 10
         , Run Kbd [("us", "us"), ("fr", "fr")]
         , Run StdinReader
       ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{ %dynnetwork% | %memory% * %swap% | %multicpu% | %battery% | %kbd% | <fc=#ee9a00>%date%</fc>"
       }
