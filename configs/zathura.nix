{ config, lib, pkgs, ... }:

let
  cfg = config.my.xsession;
in
{
  config = lib.mkIf cfg.enable {
    home = {
      packages = [ pkgs.zathura ];

      file.".config/zathura/zathurarc".text = ''
set adjust-open width

map <C-\<> goto top
map <C-\>> goto bottom
map b navigate previous
map f navigate next
map d follow

map <C-s> search
map <C-r> search
map <C-+> zoom in
map <C--> zoom out
map <C-=> zoom in
map <C-)> adjust_window best-fit
map <C-0> adjust_window width
map <C-a> scroll full-left
map <C-b> scroll left
map <C-e> scroll full-right
map <C-f> scroll right
map <C-g> abort
map <C-n> scroll down
map <C-p> scroll up
map <C-v> scroll full-down
map <C-m> Return
map <C-i> Tab

map <A-v> scroll full-up
  '';
    };
  };
}
