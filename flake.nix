{
  description = "My home flaked configuration";

  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "master";
    };

    home-manager = {
      type = "github";
      owner = "nix-community";
      repo = "home-manager";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    pytools = {
      type = "gitlab";
      owner = "ardumont";
      repo = "pytools";
      ref = "master";
    };

    mypkgs = {
      type = "github";
      owner = "ardumont";
      repo = "mypkgs";
      ref = "master";
    };

    iamb = {
      type = "github";
      owner = "ulyssa";
      repo = "iamb";
      ref = "main";
    };
    private-data = {
      url = "/etc/private-data";
    };
  };

  outputs = { self, nixpkgs, home-manager, pytools, mypkgs, private-data, iamb, ... } @ inputs:
    let lib = nixpkgs.lib;
        creds = import private-data { inherit lib; };
        identity = { me = creds.identity.ardumont; };
        username = identity.me.shortname;
        homeDirectory = "/home/${username}";
        supportedSystems = [
          "x86_64-linux"
          "aarch64-linux"
        ];
        forEachSystem = f: lib.genAttrs supportedSystems (system: f system);
        pkgsBySystem = forEachSystem (system:
          let mypkgs-realized = (import inputs.mypkgs {
                inherit lib;
                pkgs = (import inputs.nixpkgs { inherit system; }).pkgs;
              });
          in import inputs.nixpkgs {
            inherit system;
            overlays = [ (self: super: {
              weechat-mattermost = super.weechat.override {
                configure = { ... }: {
                  scripts = [
                    mypkgs-realized.wee-most
                    mypkgs-realized.wee-matter
                  ];
                };
              };

              weechat-matrix = super.weechat.override {
                configure = { ... }: {
                  scripts = with super.weechatScripts; [
                    weechat-matrix
                  ];
                };
              };
              weechat38 = super.weechat.overrideAttrs {
                version = "3.8";

                src = super.fetchurl {
                  url = "https://weechat.org/files/src/weechat-3.8.tar.bz2";
                  hash = "sha256-objxAUGvBhTkbQl4GshDP3RsCkAW4z917L9WyaVoYj4=";
                };
              };
            })];
          }
        );
        mkHomeManagerConfiguration = name: { system, config-path }:
          lib.nameValuePair name (
            { config, ... }:
            let pkgs = pkgsBySystem."${system}";
            in {
              imports = [
                (import ./roles/default.nix )
                (import ./configs {
                  inherit config lib nixpkgs pkgs mypkgs home-manager pytools creds system identity;
                })
                (import config-path)
              ];
              # For compat with nix-shell, nix-build, etc
              home.file.".nixpkgs".source = nixpkgs;
            }
          );

        mkHomeManagerHostConfiguration = name: { system, ... }:
          lib.nameValuePair name (inputs.home-manager.lib.homeManagerConfiguration {
            pkgs = nixpkgs.legacyPackages.${system};

            modules = [
              self.internal.home-cfgs."${name}"
              {
                home = {
                  inherit username;
                  homeDirectory = "/home/${username}";
                  stateVersion = "20.09";
                };
              }
            ];
          });
        x86_64 = "x86_64-linux";
        arch64 = "aarch64-linux";
        hosts = {
          alderaan   = { system = x86_64; config-path = ./hosts/alderaan.nix; };
          bespin     = { system = x86_64; config-path = ./hosts/bespin.nix; };
          dathomir   = { system = x86_64; config-path = ./hosts/dathomir.nix; };
          myrkr      = { system = x86_64; config-path = ./hosts/myrkr.nix; };
          odroid-n2  = { system = arch64; config-path = ./hosts/odroid.nix; };
          odroid     = { system = arch64; config-path = ./hosts/odroid.nix; };
          rpi3       = { system = arch64; config-path = ./hosts/rpi3.nix; };
          rpi4       = { system = arch64; config-path = ./hosts/rpi4.nix; };
          yavin4     = { system = x86_64; config-path = ./hosts/yavin4.nix; };
          kashyyyk   = { system = x86_64; config-path = ./hosts/kashyyyk.nix; };
        };
        # make-app-configs:: Set -> Set
        make-app-configs = config:
          # local-config:: List [ Set ]
          let local-config = map (hostname:
                let system = hosts."${hostname}".system;
                    host-activation-package = (
                      mkHomeManagerHostConfiguration hostname { system = system; }
                    ).value.activationPackage;
                in {
                  # nix build .#<host>
                  "${hostname}" = host-activation-package;
                  # nix run .#<host>
                  apps."${system}"."${hostname}" = {
                    type = "app";
                    program = "${host-activation-package}/activate";
                  };
                }
              ) (lib.attrNames hosts);
              # local-config-set:: Set
              # Set of configs: {
              #   ...
              #   yavin4 = <activation-package>;
              #   apps.<system>.yavin4 = { type = app; program = ...};
              #   ...
              #   bespin = <activation-package>;
              #   apps.<system>.bespin = { type = app; program = ...};
              #   ...
              # }
              local-config-set = lib.foldl lib.attrsets.recursiveUpdate {} local-config;
          in config // local-config-set;

    in make-app-configs {
      # `nix develop`
      devShell = forEachSystem (system:
        let pkgs = pkgsBySystem."${system}";
        in pkgs.mkShell {
          name = "home";
          buildInputs = with pkgs; [ ];
        });

      # Required as used within the mkHomeManagerHostConfiguration function definition
      internal.home-cfgs = lib.mapAttrs' mkHomeManagerConfiguration hosts;
    };
}
