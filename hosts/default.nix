{ ... }:

{
  imports = [
    ./alderaan.nix
    ./bespin.nix
    ./dathomir.nix
    ./myrkr.nix
    ./odroid.nix
    ./rpi3.nix
    ./rpi4.nix
    ./yavin4.nix
  ];
}
