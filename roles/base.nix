{ lib, config, ... }:

let cfg = config.hc.roles.base;
in
{
  options.hc.roles.base.enable = lib.mkEnableOption "Home local configuration";

  config = lib.mkIf cfg.enable {
    my = {
      monitoring.enable = true;
      shell.enable = true;
      scripts.enable = true;
      ssh.enable = true;
      tmux.enable = true;
      fstools.enable = true;
      qmk.enable = true;
    };
  };
}
