{ config, ... }:

{
  imports = [
    ./base.nix
    ./local.nix
    ./remote.nix
    ./remote-no-x.nix
    ./server.nix
  ];

  hc.roles.base.enable = true;
}
