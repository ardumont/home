{ config, lib, ... }:

let cfg = config.hc.roles.local;
in {
  options.hc.roles.local.enable = lib.mkEnableOption "Home local configuration";

  config = lib.mkIf cfg.enable {
    my = {
      clustershell.enable = true;
      git.enable = true;
      gpg.enable = true;
      nix.enable = true;
      guix.enable = true;
      xsession.enable = true;
      xkeysnail.enable = false;
      rxvt.enable = true;
      email.enable = true;
      emacs.optional-dependencies.enable = true;
      common-lisp.enable = true;
      development.enable = true;
      container-tools.enable = true;
      android-tools.enable = true;
      work.enable = true;
      browser.program = "~/bin/qutebrowser";
      auth.enable = true;
      element-desktop.enable = true;
      weechat.enable = true;
      gomuks.enable = true;
      iamb.enable = true;
      media.enable = true;
      tmux = {
        prefix-key = "o";
        alternative-prefix-key = "q";
      };
    };
  };

}
