{ config, lib, pkgs, ... }:

let cfg = config.hc.roles.remote-no-x;
in {
  options.hc.roles.remote-no-x.enable = lib.mkEnableOption "Home remote no-x configuration";

  config = lib.mkIf cfg.enable {
    my = {
      browser.program = "${pkgs.w3m-nox}/bin/w3m";
      # deactivate most high volume dependencies
      xsession.enable = false;
      email.enable = false;
      emacs.optional-dependencies.enable = false;
      development.enable = true;
      android-tools.enable = false;
      work.enable = false;
      nix.enable = false;
      tmux = {
        prefix-key = "q";
        alternative-prefix-key = "o";
      };
    };
  };

}
