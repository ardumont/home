{ config, lib, ... }:

let cfg = config.hc.roles.remote;
in {
  options.hc.roles.remote.enable = lib.mkEnableOption "Home remote configuration";

  config = lib.mkIf cfg.enable {
    my = {
      xsession.enable = true;
      email.enable = false;
      emacs.optional-dependencies.enable = true;
      development.enable = true;
      android-tools.enable = false;
      work.enable = true;
      browser.program = "~/bin/qutebrowser";
      nix.enable = true;
      rxvt.enable = true;
      container-tools.enable = true;
      tmux = {
        prefix-key = "q";
        alternative-prefix-key = "o";
      };
    };
  };

}
