{ config, lib, pkgs, ... }:

let cfg = config.hc.roles.server;
in {
  options.hc.roles.server.enable = lib.mkEnableOption "Home server configuration";

  config = lib.mkIf cfg.enable {
    my = {
      browser.program = "${pkgs.w3m-nox}/bin/w3m";
      # deactivate most high volume dependencies
      xsession.enable = false;
      email.enable = false;
      emacs.optional-dependencies.enable = false;
      development.enable = false;
      android-tools.enable = false;
      work.enable = false;
      nix.enable = false;
      weechat.enable = true;
      media.enable = true;
      tmux = {
        prefix-key = "q";
        alternative-prefix-key = "o";
      };
    };
  };

}
